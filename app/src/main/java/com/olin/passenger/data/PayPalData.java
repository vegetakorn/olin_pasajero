package com.olin.passenger.data;

/**
 * Created by TOXSL\jagmel.singh on 17/6/17.
 */

public class PayPalData {
    public String id;
    public String state;
    public String createTime;
    public String short_description;
    public String amount;
    public String currencyCode;
    public String responseType;

}

package com.olin.passenger.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TOXSL\jagmel.singh on 1/6/17.
 */

public class JourneyData implements Parcelable {
    public int id;
    public String amount;
    public String location;
    public String destination;
    public int number_of_passengers;
    public int number_of_bags;
    public int number_of_hours;
    public int journey_type;
    public double location_lat;
    public double location_long;
    public double destination_lat;
    public double destination_long;
    public int state_id;
    public boolean is_hourly;
    public String start_time;
    public String end_time;
    public String create_time;
    public Vehicle vehicle;
    //2017-06-01 19:58:13


    public JourneyData(Parcel in) {
        id = in.readInt();
        amount = in.readString();
        location = in.readString();
        destination = in.readString();
        number_of_passengers = in.readInt();
        number_of_bags = in.readInt();
        number_of_hours = in.readInt();
        journey_type = in.readInt();
        location_lat = in.readDouble();
        location_long = in.readDouble();
        destination_lat = in.readDouble();
        destination_long = in.readDouble();
        state_id = in.readInt();
        is_hourly = in.readByte() != 0;
        start_time = in.readString();
        end_time = in.readString();
        create_time = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(amount);
        dest.writeString(location);
        dest.writeString(destination);
        dest.writeInt(number_of_passengers);
        dest.writeInt(number_of_bags);
        dest.writeInt(number_of_hours);
        dest.writeInt(journey_type);
        dest.writeDouble(location_lat);
        dest.writeDouble(location_long);
        dest.writeDouble(destination_lat);
        dest.writeDouble(destination_long);
        dest.writeInt(state_id);
        dest.writeByte((byte) (is_hourly ? 1 : 0));
        dest.writeString(start_time);
        dest.writeString(end_time);
        dest.writeString(create_time);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<JourneyData> CREATOR = new Creator<JourneyData>() {
        @Override
        public JourneyData createFromParcel(Parcel in) {
            return new JourneyData(in);
        }

        @Override
        public JourneyData[] newArray(int size) {
            return new JourneyData[size];
        }
    };
}


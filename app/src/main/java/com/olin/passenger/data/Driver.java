package com.olin.passenger.data;


import com.google.gson.annotations.SerializedName;

public class Driver {
    @SerializedName("id")
    public int id;

    @SerializedName("vehicle")
    public int vehicle;
    @SerializedName("model")
    public String model;
    @SerializedName("car_make")
    public String carMake;
    @SerializedName("license_no")
    public String licenseNo;
    @SerializedName("registrationNo")
    public String registrationNo;
    @SerializedName("type_id")
    public int typeId;

    @SerializedName("company")
    public Company company;
}

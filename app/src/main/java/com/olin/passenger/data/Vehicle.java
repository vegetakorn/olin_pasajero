package com.olin.passenger.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TOXSL\jagmel.singh on 2/6/17.
 */

public class Vehicle implements Parcelable{

    public  int id;
    public  String model;
    public  String car_make;
    public  String license_no;
    public  String registration_no;
    public  int type_id;

    public Vehicle(Parcel in) {
        id = in.readInt();
        model = in.readString();
        car_make = in.readString();
        license_no = in.readString();
        registration_no = in.readString();
        type_id = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(model);
        dest.writeString(car_make);
        dest.writeString(license_no);
        dest.writeString(registration_no);
        dest.writeInt(type_id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Vehicle> CREATOR = new Creator<Vehicle>() {
        @Override
        public Vehicle createFromParcel(Parcel in) {
            return new Vehicle(in);
        }

        @Override
        public Vehicle[] newArray(int size) {
            return new Vehicle[size];
        }
    };
}

package com.olin.passenger.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TOXSL\visha.sehgal on 23/1/17.
 */

public class WalletData implements Parcelable {

    public int id;
    public String amount;
    public String currency_code;
    public String currency_symbol;


    public WalletData(Parcel in) {
        id = in.readInt();
        amount = in.readString();
        currency_code = in.readString();
        currency_symbol = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(amount);
        dest.writeString(currency_code);
        dest.writeString(currency_symbol);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WalletData> CREATOR = new Creator<WalletData>() {
        @Override
        public WalletData createFromParcel(Parcel in) {
            return new WalletData(in);
        }

        @Override
        public WalletData[] newArray(int size) {
            return new WalletData[size];
        }
    };
}

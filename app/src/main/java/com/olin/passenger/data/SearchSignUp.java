package com.olin.passenger.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TOXSL\amrinder.singh on 2/11/17.
 */

public class SearchSignUp implements Parcelable {
    public String settlement;
    public String municipality;
    public String state;
    public int id;
    public int zipcode;

    public SearchSignUp(Parcel in) {
        settlement = in.readString();
        municipality = in.readString();
        state = in.readString();
        id = in.readInt();
        zipcode = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(settlement);
        dest.writeString(municipality);
        dest.writeString(state);
        dest.writeInt(id);
        dest.writeInt(zipcode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SearchSignUp> CREATOR = new Creator<SearchSignUp>() {
        @Override
        public SearchSignUp createFromParcel(Parcel in) {
            return new SearchSignUp(in);
        }

        @Override
        public SearchSignUp[] newArray(int size) {
            return new SearchSignUp[size];
        }
    };
}

package com.olin.passenger.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TOXSL\ankan.tiwari on 28/9/17.
 */

public class CouponsDetails implements Parcelable {

    public int id;
    public int coupon_id;
    public int pageCount;
    public String Description;
    public String Coupon_code;
    public int discount;
    public String startime;
    public String endtime;


    public CouponsDetails(Parcel in) {

        id = in.readInt();
        coupon_id = in.readInt();
        Description = in.readString();
        Coupon_code = in.readString();
        discount = in.readInt();
        startime = in.readString();
        endtime = in.readString();
        pageCount = in.readInt();
    }

    public static Creator<CouponsDetails> CREATOR = new Creator<CouponsDetails>() {

        @Override
        public CouponsDetails createFromParcel(Parcel in) {
            return new CouponsDetails(in);
        }

        @Override
        public CouponsDetails[] newArray(int size) {
            return new CouponsDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(id);
        dest.writeInt(coupon_id);
        dest.writeString(Description);
        dest.writeString(Coupon_code);
        dest.writeInt(discount);
        dest.writeInt(pageCount);
        dest.writeString(startime);
        dest.writeString(endtime);

    }
}

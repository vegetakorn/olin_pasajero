package com.olin.passenger.data;


/**
 * Created by Kyra on 1/11/2016.
 */
public class PlaceAutoComplete {

    private String place_id;
    private String description;
    private String icon;
    private String lat;
    private String lng;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }


    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getPlaceDesc() {
        return description;
    }

    public void setPlaceDesc(String placeDesc) {
        description = placeDesc;
    }

    public String getPlaceID() {
        return place_id;
    }

    public void setPlaceID(String placeID) {
        place_id = placeID;
    }

    public String getPlaceIcon() {
        return icon;
    }

    public void setPlaceIcon(String icon) {
        this.icon = icon;
    }
}

package com.olin.passenger.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by TOXSL\jagmel.singh on 29/6/17.
 */

public class DriverReviewData implements Parcelable {
    public String comment;
    public String created_on;
    public String first_name;
    public String last_name;
    public String image_file;
    public float rate;
    public boolean showMore;


    public DriverReviewData(Parcel in) {
        comment = in.readString();
        created_on = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        image_file = in.readString();
        rate = in.readFloat();
        showMore = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(comment);
        dest.writeString(created_on);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(image_file);
        dest.writeFloat(rate);
        dest.writeByte((byte) (showMore ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DriverReviewData> CREATOR = new Creator<DriverReviewData>() {
        @Override
        public DriverReviewData createFromParcel(Parcel in) {
            return new DriverReviewData(in);
        }

        @Override
        public DriverReviewData[] newArray(int size) {
            return new DriverReviewData[size];
        }
    };

    public String getCreatedOn(){
        String d = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date convertedCurrentDate = sdf.parse(created_on);
            String pattern = "dd/MM/yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            d = simpleDateFormat.format(convertedCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }
}

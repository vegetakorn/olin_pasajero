package com.olin.passenger.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TOXSL\visha.sehgal on 23/1/17.
 */

public class CountryData implements Parcelable{
    public int id;
    public String title;
    public String country_code;
    public String telephone_code;
    public String currency_code;
    public String currency_symbol;
    public String flag;
    public int[] arr_car_type;
    public int car_type;


    public CountryData(Parcel in) {
        id = in.readInt();
        title = in.readString();
        country_code = in.readString();
        telephone_code = in.readString();
        currency_code = in.readString();
        currency_symbol = in.readString();
        flag = in.readString();
        arr_car_type = in.createIntArray();
        car_type = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(country_code);
        dest.writeString(telephone_code);
        dest.writeString(currency_code);
        dest.writeString(currency_symbol);
        dest.writeString(flag);
        dest.writeIntArray(arr_car_type);
        dest.writeInt(car_type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CountryData> CREATOR = new Creator<CountryData>() {
        @Override
        public CountryData createFromParcel(Parcel in) {
            return new CountryData(in);
        }

        @Override
        public CountryData[] newArray(int size) {
            return new CountryData[size];
        }
    };
}

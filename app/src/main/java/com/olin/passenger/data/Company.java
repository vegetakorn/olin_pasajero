package com.olin.passenger.data;


import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Empresa
 */
public class Company  {
    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @NonNull
    @Override
    public String toString() {
        return name;
    }
}

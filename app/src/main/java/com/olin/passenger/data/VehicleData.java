package com.olin.passenger.data;

/**
 * Created by TOXSL\jagmel.singh on 29/5/17.
 */

public class VehicleData {
    public int id;
    public CountryData country;
    public String currency_code;
    public String currency_symbol;
    public int seat;
    public String description;
    public String base_price;
    public String price_min;
    public String price_mile;
    public int type_id;
}

package com.olin.passenger.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TOXSL\jagmel.singh on 20/5/17.
 */

public class ProfileData implements Parcelable {
    public int id;
    public int state_id;
    public int is_fb;
    public String first_name;
    public String last_name;
    public String email;
    public String image_file;
    public String contact_no;
    public double latitude;
    public double longitude;
    public CountryData country;
    public WalletData walletData;
    public String settlement;
    public String municipality;
    public String state;
    public String zipcode;


    public ProfileData(Parcel in) {
        id = in.readInt();
        state_id = in.readInt();
        is_fb = in.readInt();
        first_name = in.readString();
        last_name = in.readString();
        email = in.readString();
        image_file = in.readString();
        contact_no = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        country = in.readParcelable(CountryData.class.getClassLoader());
        walletData = in.readParcelable(WalletData.class.getClassLoader());
        settlement = in.readString();
        municipality = in.readString();
        state = in.readString();
        zipcode = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(state_id);
        dest.writeInt(is_fb);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(email);
        dest.writeString(image_file);
        dest.writeString(contact_no);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeParcelable(country, flags);
        dest.writeParcelable(walletData, flags);
        dest.writeString(settlement);
        dest.writeString(municipality);
        dest.writeString(state);
        dest.writeString(zipcode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProfileData> CREATOR = new Creator<ProfileData>() {
        @Override
        public ProfileData createFromParcel(Parcel in) {
            return new ProfileData(in);
        }

        @Override
        public ProfileData[] newArray(int size) {
            return new ProfileData[size];
        }
    };
}

package com.olin.passenger.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TOXSL\jagmel.singh on 3/6/17.
 */

public class RideDetails implements Parcelable {
    public int id;
    public String amount;
    public String location;
    public String destination;
    public double location_lat;
    public double location_long;
    public double destination_lat;
    public double destination_long;
    public String number_of_passengers;
    public String number_of_bags;
    public String number_of_hours;
    public boolean is_hourly;
    public int journey_type;
    public int state_id;
    public int pageCount;
    public double remaining_balance;
    public boolean coupon_applied;
    public String journey_time;
    public String start_time;
    public String end_time;
    public String create_time;
    public DriverData driverData;
    public String image_file;
    public String vehicle_image;
    public Vehicle vehicle;
    private WalletData walletData;


    public RideDetails(Parcel in) {
        id = in.readInt();
        amount = in.readString();
        location = in.readString();
        destination = in.readString();
        location_lat = in.readDouble();
        location_long = in.readDouble();
        destination_lat = in.readDouble();
        destination_long = in.readDouble();
        number_of_passengers = in.readString();
        number_of_bags = in.readString();
        number_of_hours = in.readString();
        is_hourly = in.readByte() != 0;
        journey_type = in.readInt();
        state_id = in.readInt();
        pageCount = in.readInt();
        remaining_balance = in.readDouble();
        coupon_applied = in.readByte() != 0;
        journey_time = in.readString();
        start_time = in.readString();
        end_time = in.readString();
        create_time = in.readString();
        driverData = in.readParcelable(DriverData.class.getClassLoader());
        image_file = in.readString();
        vehicle_image = in.readString();
        vehicle = in.readParcelable(Vehicle.class.getClassLoader());
        walletData = in.readParcelable(WalletData.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(amount);
        dest.writeString(location);
        dest.writeString(destination);
        dest.writeDouble(location_lat);
        dest.writeDouble(location_long);
        dest.writeDouble(destination_lat);
        dest.writeDouble(destination_long);
        dest.writeString(number_of_passengers);
        dest.writeString(number_of_bags);
        dest.writeString(number_of_hours);
        dest.writeByte((byte) (is_hourly ? 1 : 0));
        dest.writeInt(journey_type);
        dest.writeInt(state_id);
        dest.writeInt(pageCount);
        dest.writeDouble(remaining_balance);
        dest.writeByte((byte) (coupon_applied ? 1 : 0));
        dest.writeString(journey_time);
        dest.writeString(start_time);
        dest.writeString(end_time);
        dest.writeString(create_time);
        dest.writeParcelable(driverData, flags);
        dest.writeString(image_file);
        dest.writeString(vehicle_image);
        dest.writeParcelable(vehicle, flags);
        dest.writeParcelable(walletData, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RideDetails> CREATOR = new Creator<RideDetails>() {
        @Override
        public RideDetails createFromParcel(Parcel in) {
            return new RideDetails(in);
        }

        @Override
        public RideDetails[] newArray(int size) {
            return new RideDetails[size];
        }
    };
}


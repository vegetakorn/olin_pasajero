package com.olin.passenger.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.Marker;
import com.google.gson.annotations.SerializedName;

/**
 * Created by TOXSL\ankush.walia on 27/1/17.
 */

/**
 * Informacion del conductor (usuario)
 */
public class DriverData implements Parcelable {

    @SerializedName("id")
    public int id;

    @SerializedName("driver_rating")
    public double driver_rating;
    @SerializedName("first_name")
    public String first_name;
    @SerializedName("last_name")
    public String last_name;
    @SerializedName("email")
    public String email;
    @SerializedName("contact_no")
    public String contact_no;
    @SerializedName("state_id")
    public String state_id;
    public int passenger_rating;
    public double location_lat;
    public String vehicle_image;
    public CountryData countryData;
    @SerializedName("lat")
    public Double latitude;
    @SerializedName("long")
    public Double longitude;
    public Marker driverMarker;
    public int car_type;
    @SerializedName("image_file")
    public String image_file;

    @SerializedName("driver")
    public Driver driver;

    public DriverData(){
    }

    public DriverData(Parcel in) {
        id = in.readInt();
        driver_rating = in.readDouble();
        first_name = in.readString();
        last_name = in.readString();
        email = in.readString();
        contact_no = in.readString();
        state_id = in.readString();
        passenger_rating = in.readInt();
        location_lat = in.readDouble();
        vehicle_image = in.readString();
        countryData = in.readParcelable(CountryData.class.getClassLoader());
        latitude = in.readDouble();
        longitude = in.readDouble();
        car_type = in.readInt();
        image_file = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeDouble(driver_rating);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(email);
        dest.writeString(contact_no);
        dest.writeString(state_id);
        dest.writeInt(passenger_rating);
        dest.writeDouble(location_lat);
        dest.writeString(vehicle_image);
        dest.writeParcelable(countryData, flags);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeInt(car_type);
        dest.writeString(image_file);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DriverData> CREATOR = new Creator<DriverData>() {
        @Override
        public DriverData createFromParcel(Parcel in) {
            return new DriverData(in);
        }

        @Override
        public DriverData[] newArray(int size) {
            return new DriverData[size];
        }
    };
}

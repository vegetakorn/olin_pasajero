package com.olin.passenger.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.olin.passenger.R;
import com.olin.passenger.data.CountryData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by TOXSL\visha.sehgal on 23/1/17.
 */

public class CustomSpinnerAdapter extends ArrayAdapter<CountryData> {
    boolean isProfilePage;
    LayoutInflater inflater;
    Context context;
    ArrayList<CountryData> arrayList;

    public CustomSpinnerAdapter(Context context, ArrayList<CountryData> arrayList) {
        super(context, 0, arrayList);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.arrayList = arrayList;
    }

    public CustomSpinnerAdapter(Context context, ArrayList<CountryData> arrayList, boolean isProfilePage) {
        super(context, 0, arrayList);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.isProfilePage = isProfilePage;
        this.arrayList = arrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return initView(position, convertView, parent, false);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return dropView(position, convertView, parent, true);
    }

    private View dropView(int position, View convertView, ViewGroup parent, boolean isDropdown) {
        convertView = inflater.inflate(R.layout.spinner_adapter, parent, false);
        TextView countryNameTV = (TextView) convertView.findViewById(R.id.countryNameTV);
        TextView countryCodeTV = (TextView) convertView.findViewById(R.id.countryCodeTV);
        ImageView flagIV = (ImageView) convertView.findViewById(R.id.flagIV);
        countryNameTV.setText(getItem(position).title);
        countryCodeTV.setText(getItem(position).telephone_code);
        Picasso.with(context).load(getItem(position).flag).into(flagIV);
        return convertView;
    }

    private View initView(int position, View convertView, ViewGroup parent, boolean isDropdown) {
        convertView = inflater.inflate(R.layout.adapter_custom_spinner_signup, parent, false);
        TextView tvText2 = (TextView) convertView.findViewById(R.id.nameTV);
        ImageView flagIV = (ImageView) convertView.findViewById(R.id.flagIV);
        CountryData data = arrayList.get(position);
        if (isProfilePage) {
            tvText2.setTextColor(ContextCompat.getColor(context, R.color.Black));
        }
        else
            tvText2.setTextColor(ContextCompat.getColor(context, R.color.White));
        tvText2.setText(data.telephone_code);
        Picasso.with(context).load(data.flag).into(flagIV);
        return convertView;
    }
}
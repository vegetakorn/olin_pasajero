package com.olin.passenger.adapter;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.olin.passenger.R;
import com.olin.passenger.activity.BaseActivity;
import com.olin.passenger.data.CouponsDetails;
import com.olin.passenger.fragment.CouponCodeFragment;

import java.util.ArrayList;


/**
 * Created by TOXSL\ankan.tiwari on 28/9/17.
 */

public class CouponAdapter extends ArrayAdapter<CouponsDetails> {

    private BaseActivity context;
    private ArrayList<CouponsDetails> couponsDetails = new ArrayList<>();
    private CouponCodeFragment fragment;
    private boolean is_applied;

    public CouponAdapter(BaseActivity context, ArrayList<CouponsDetails> couponDetailList, CouponCodeFragment fragment, boolean is_applied) {
        super(context, 0);
        this.context = context;
        this.couponsDetails = couponDetailList;
        this.fragment = fragment;
        this.is_applied = is_applied;

    }

    @Override
    public int getCount() {
        return couponsDetails.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.adaptor_couponslist, parent, false);
            holder.journeyDetailLL = (CardView) convertView.findViewById(R.id.journeyDetailLL);
            holder.couponcodeTV = (TextView) convertView.findViewById(R.id.couponcodeTV);
            holder.appliedTV = (TextView) convertView.findViewById(R.id.appliedTV);
            holder.coupondescriptionTV = (TextView) convertView.findViewById(R.id.coupondescriptionTV);
            holder.discountTV = (TextView) convertView.findViewById(R.id.discountTV);
            holder.stimeTV = (TextView) convertView.findViewById(R.id.sdateTV);
            holder.etimeTV = (TextView) convertView.findViewById(R.id.edateTV);
            holder.couponcodeTV.setText(couponsDetails.get(position).Coupon_code);
            holder.coupondescriptionTV.setText(couponsDetails.get(position).Description);
            holder.stimeTV.setText(couponsDetails.get(position).startime);
            holder.etimeTV.setText(couponsDetails.get(position).endtime);
            holder.discountTV.setText(String.valueOf(couponsDetails.get(position).discount + "%"));
            if (!is_applied) {
                holder.appliedTV.setText(context.getString(R.string.coupon_applied));
            } else {
                holder.appliedTV.setText("");
            }

            holder.journeyDetailLL.setOnClickListener(new OnClick(position, holder) {
                @Override
                public void onClick(View v) {

                    fragment.hitApplyCouponApi(couponsDetails.get(position).id, holder.appliedTV);
                }
            });

            convertView.setTag(holder);
        } else {
            convertView.getTag();
        }
        return convertView;
    }

    static class ViewHolder {
        private TextView couponcodeTV, coupondescriptionTV, discountTV, stimeTV, etimeTV, appliedTV;
        private CardView journeyDetailLL;
    }

    private class OnClick implements View.OnClickListener {
        int position;
        ViewHolder holder;

        OnClick(int position, ViewHolder holder) {
            this.position = position;
            this.holder = holder;
        }


        @Override
        public void onClick(View v) {

        }
    }


}

package com.olin.passenger.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Vibrator;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.olin.passenger.R;
import com.olin.passenger.activity.BaseActivity;
import com.olin.passenger.data.RideDetails;
import com.olin.passenger.fragment.MyBookingsFragment;
import com.olin.passenger.utils.Const;

import java.util.ArrayList;

/**
 * Created by TOXSL\visha.sehgal on 24/1/17.
 */

public class BookingsAdapter extends ArrayAdapter<RideDetails> {
    private BaseActivity context;
    private ArrayList<RideDetails> rideDetailList = new ArrayList<>();
    private MyBookingsFragment fragment;
    private Vibrator vibrator;
    private String[] states = new String[]{"NA", "New", "Accepted", "Rejected", "Canceled", "Driver Arrived", "Started", "Completed", "Paid"};
    private Dialog dialog;

    public BookingsAdapter(BaseActivity context, ArrayList<RideDetails> rideDetailList, MyBookingsFragment fragment) {
        super(context, 0);
        this.context = context;
        this.rideDetailList = rideDetailList;
        this.fragment = fragment;
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    public int getCount() {
        return rideDetailList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_booking_history, parent, false);
            holder.journeyDetailLL = (CardView) convertView.findViewById(R.id.journeyDetailLL);
            holder.driverNameTV = (TextView) convertView.findViewById(R.id.driverNameTV);
            holder.pickUpLocTV = (TextView) convertView.findViewById(R.id.pickUpLocTV);
            holder.destinationTV = (TextView) convertView.findViewById(R.id.destinationTV);
            holder.rideStatusTV = (TextView) convertView.findViewById(R.id.rideStatusTV);
            holder.dateTimeTV = (TextView) convertView.findViewById(R.id.dateTimeTV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        RideDetails details = rideDetailList.get(position);
        if (details.driverData != null && !details.driverData.first_name.isEmpty()) {
            holder.driverNameTV.setText(details.driverData.first_name.concat(" " + details.driverData.last_name));
            if (details.state_id == Const.STATE_CANCELLED)
                holder.dateTimeTV.setText(!details.create_time.equalsIgnoreCase("") ? context.convertUTCToLocal(details.create_time, "dd MMM,yyyy hh:mm a") : "NA");
            else
                holder.dateTimeTV.setText(!details.start_time.equalsIgnoreCase("") ? context.convertUTCToLocal(details.start_time,  "dd MMM,yyyy hh:mm a") : "NA");
        } else if (details.state_id == Const.STATE_NEW) {
            holder.driverNameTV.setText(R.string.not_available);
            holder.dateTimeTV.setText(!details.journey_time.equalsIgnoreCase("") ? context.convertUTCToLocal(details.journey_time, "dd MMM,yyyy hh:mm a") : "NA");
        } else {
            holder.driverNameTV.setText(R.string.not_available);
            holder.dateTimeTV.setText(!details.create_time.equalsIgnoreCase("") ? context.convertUTCToLocal(details.create_time, "dd MMM,yyyy hh:mm a") : "NA");
        }
        holder.pickUpLocTV.setText(details.location);
        holder.destinationTV.setText(details.destination);

        holder.rideStatusTV.setText(states[details.state_id]);
        if (position + 1 == rideDetailList.size()) {
            fragment.hitRideHistoryApi();
        }
        holder.journeyDetailLL.setTag(position);
        holder.journeyDetailLL.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                openComplainDialog((int) v.getTag());
                if (vibrator.hasVibrator())
                    vibrator.vibrate(50);
                return true;
            }
        });

        return convertView;
    }

    private void openComplainDialog(int pos) {

        RideDetails rideDetail = rideDetailList.get(pos);
          dialog = new Dialog(context, R.style.Theme_AppCompat_Dialog);
        dialog.setContentView(R.layout.dialog_complaint);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button submitBT = (Button) dialog.findViewById(R.id.submitBT);
        Button cancelBT = (Button) dialog.findViewById(R.id.cancelBT);
        EditText complainET = (EditText) dialog.findViewById(R.id.complainET);


        submitBT.setTag(R.string.complain, complainET);
        submitBT.setTag(R.string.dialog, dialog);
        submitBT.setTag(R.string.ride_detail, rideDetail);
        cancelBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        submitBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText complaintET = (EditText) v.getTag(R.string.complain);
                Dialog d = (Dialog) v.getTag(R.string.dialog);
                RideDetails rideDetails = (RideDetails) v.getTag(R.string.ride_detail);
                if (validations(complaintET)) {
                    String complain = complaintET.getText().toString().trim();
                    fragment.sendComplain(rideDetails.id, complain);
                    d.dismiss();
                }
            }

            private boolean validations(EditText phoneET) {
                boolean isvalid = false;
                if (phoneET.getText().toString().trim().equals(""))
                    context.showToast("Please enter your complain !");
                else isvalid = true;
                return isvalid;
            }
        });

        if (dialog.isShowing()) {
            dialog.dismiss();
            dialog.show();
        } else
            dialog.show();
    }

    private class ViewHolder {
        private TextView rideStatusTV, driverNameTV, pickUpLocTV, destinationTV, dateTimeTV;
        private CardView journeyDetailLL;
    }

}
package com.olin.passenger.adapter;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.olin.passenger.R;
import com.olin.passenger.activity.BaseActivity;
import com.olin.passenger.data.SearchSignUp;
import com.olin.passenger.fragment.LoginFragment;
import com.olin.passenger.fragment.ProfileFragment;

import java.util.ArrayList;

/**
 * Created by TOXSL\amrinder.singh on 3/11/17.
 */

public class PostalCodeAdapter extends ArrayAdapter<SearchSignUp> {
    private BaseActivity baseActivity;
    private ArrayList<SearchSignUp> searchSignUps = new ArrayList<>();
    private Fragment fragment;

    public PostalCodeAdapter(BaseActivity baseActivity, ArrayList<SearchSignUp> searchSignUps, Fragment signupFragment) {
        super(baseActivity, 0);
        this.baseActivity = baseActivity;
        this.searchSignUps = searchSignUps;
        this.fragment = signupFragment;
    }

    @Override
    public int getCount() {
        return searchSignUps.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_postal_item, parent, false);
        }

        TextView postalTV = (TextView) convertView.findViewById(R.id.postalTV);
        TextView settleTV = (TextView) convertView.findViewById(R.id.settleTV);
        TextView muncipalTV = (TextView) convertView.findViewById(R.id.muncipalTV);
        TextView stateTV = (TextView) convertView.findViewById(R.id.stateTV);
        postalTV.setText("" + searchSignUps.get(position).zipcode);
        settleTV.setText("" + searchSignUps.get(position).settlement);
        muncipalTV.setText("" + searchSignUps.get(position).municipality);
        stateTV.setText("" + searchSignUps.get(position).state);


        postalTV.setPadding(20, 7, 0, 7);
        if (searchSignUps.size() >= 20 && position == searchSignUps.size() - 1) {
            if (fragment instanceof ProfileFragment) {
                ((ProfileFragment) fragment).hitSearchApi();
            } else if (fragment instanceof LoginFragment) {
                ((LoginFragment) fragment).hitSearchApi();
            }

        }
        return convertView;
    }
}



package com.olin.passenger.adapter;

import androidx.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.pkmmte.view.CircularImageView;
import com.olin.passenger.R;
import com.olin.passenger.activity.BaseActivity;
import com.olin.passenger.data.DriverReviewData;
import com.olin.passenger.fragment.DriverReviewFragment;

import java.util.ArrayList;

public class DriverReviewAdapter extends ArrayAdapter<DriverReviewData> {
    private BaseActivity activity;
    private String TAG = "DriverReviewAdapter";
    ArrayList<DriverReviewData> reviewDatas = new ArrayList<>();
    DriverReviewFragment fragment;
    private boolean isShownMore;

    public DriverReviewAdapter(BaseActivity context, ArrayList<DriverReviewData> reviewDatas, DriverReviewFragment fragment) {
        super(context, 0, reviewDatas);
        this.activity = context;
        this.reviewDatas = reviewDatas;
        this.fragment = fragment;

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = activity.inflater.inflate(R.layout.adapter_reviews, parent, false);
            holder.profileIV = (CircularImageView) convertView.findViewById(R.id.profileIV);
            holder.ratingRB = (RatingBar) convertView.findViewById(R.id.ratingRB);
            holder.nameTV = (TextView) convertView.findViewById(R.id.nameTV);
            holder.reviewTV = (TextView) convertView.findViewById(R.id.reviewTV);
            holder.timeTV = (TextView) convertView.findViewById(R.id.timeTV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        DriverReviewData reviewData = reviewDatas.get(position);
        holder.nameTV.setText(reviewData.first_name.concat(" " + reviewData.last_name));
        holder.reviewTV.setText(reviewData.comment);

        holder.timeTV.setText(reviewData.getCreatedOn());
        holder.ratingRB.setRating(reviewData.rate);
        try {
            if (reviewData.image_file != null && !reviewData.image_file.isEmpty()) {
                try {
                    activity.picasso.load(reviewData.image_file).placeholder(R.mipmap.default_avatar).into(holder.profileIV);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            } else
                activity.picasso.load(R.mipmap.default_avatar).into(holder.profileIV);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (position + 1 == reviewDatas.size()) {
            fragment.getReviews();
        }


        return convertView;
    }

    public class ViewHolder {
        TextView reviewTV, timeTV, nameTV, showMoreTV;
        CircularImageView profileIV;
        RatingBar ratingRB;

    }

    @Override
    public int getCount() {
        return reviewDatas.size();
    }
}

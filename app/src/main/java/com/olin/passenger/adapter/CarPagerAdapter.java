package com.olin.passenger.adapter;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.olin.passenger.R;
import com.olin.passenger.activity.BaseActivity;
import com.olin.passenger.data.VehicleData;
import com.olin.passenger.fragment.HomeFragment;
import com.olin.passenger.utils.Util;

import java.util.ArrayList;

/**
 * Created by TOXSL\jagmel.singh on 22/5/17.
 */

public class CarPagerAdapter extends RecyclerView.Adapter<CarPagerAdapter.MyViewHolder> {
    private BaseActivity baseActivity;
    ArrayList<VehicleData> list;
    private HomeFragment fragment;
    String[] name;
    int[] pics;
    private int checkedPosition = -1;
    private CarPagerAdapter.OnClickListener onClickListener;

    public CarPagerAdapter(HomeFragment fragment, BaseActivity baseActivity, String[] name, int[] pics, ArrayList<VehicleData> list,CarPagerAdapter.OnClickListener onClickListener) {
        this.baseActivity = baseActivity;
        this.fragment = fragment;
        this.list = list;
        this.name = name;
        this.pics = pics;
        this.onClickListener = onClickListener;
    }

    public interface OnClickListener {
        void onClick(int position,View var1);
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvCarTime;
        public ImageView ivCarType;
        public TextView carTypeTV;
        private LinearLayout llPlRoot;
        public MyViewHolder(View v) {
            super(v);
            llPlRoot = v.findViewById(R.id.llPlRoot);
            tvCarTime = v.findViewById(R.id.tvCarTime);
            ivCarType = v.findViewById(R.id.ivCarType);
            carTypeTV = v.findViewById(R.id.carTypeTV);
        }

        void bind(final VehicleData data) {
            if (checkedPosition == -1) {
                unselected(llPlRoot);
            } else {
                if (checkedPosition == getAdapterPosition()) {
                    selected(llPlRoot);
                } else {
                    unselected(llPlRoot);
                }
            }
            ivCarType.setImageResource(pics[data.type_id]);
            ivCarType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragment.showCarDetailDialog(getAdapterPosition());
                }
            });
            carTypeTV.setText(data.description);
            //carTypeTV.setCompoundDrawablesRelativeWithIntrinsicBounds(baseActivity.getResources().getDrawable(pics[list.get(position).type_id]), null, null, null);
            /*
            carTypeTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            */

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selected(llPlRoot);
                    if (checkedPosition != getAdapterPosition()) {
                        notifyItemChanged(checkedPosition);
                        checkedPosition = getAdapterPosition();
                        onClickListener.onClick(checkedPosition,view);
                    }
                }
            });
        }
    }

    private void selected(LinearLayout llPlRoot){
        llPlRoot.setBackground(llPlRoot.getContext().getResources().getDrawable(R.drawable.button_blue));
        TextView carTypeTV = llPlRoot.findViewById(R.id.carTypeTV);
        TextView tvCarTime = llPlRoot.findViewById(R.id.tvCarTime);
        carTypeTV.setTextColor(Util.getColor(R.color.White,llPlRoot.getContext()));
        tvCarTime.setTextColor(Util.getColor(R.color.White,llPlRoot.getContext()));
    }
    private void unselected(LinearLayout llPlRoot){
        llPlRoot.setBackground(llPlRoot.getContext().getResources().getDrawable(R.drawable.bg_item));
        TextView carTypeTV = llPlRoot.findViewById(R.id.carTypeTV);
        TextView tvCarTime = llPlRoot.findViewById(R.id.tvCarTime);
        carTypeTV.setTextColor(Util.getColor(R.color.Black,llPlRoot.getContext()));
        tvCarTime.setTextColor(Util.getColor(R.color.Black,llPlRoot.getContext()));
        tvCarTime.setText(null);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CarPagerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pager_layout, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.bind(list.get(position));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return list.size();
    }

    public int getCurrentItem(){
        return checkedPosition;
    }

}

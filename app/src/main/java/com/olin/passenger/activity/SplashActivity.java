package com.olin.passenger.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.google.firebase.iid.FirebaseInstanceId;
import com.olin.passenger.R;
import com.olin.passenger.data.ProfileData;
import com.olin.passenger.data.RideDetails;
import com.olin.passenger.utils.Const;
import com.olin.passenger.utils.Util;
import com.toxsl.volley.Request;
import com.toxsl.volley.VolleyError;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;


public class SplashActivity extends BaseActivity {

    private static final int PERM_REQ_ACCESS_FINE_LOCATION = 9091;
    private static final int PERM_REQ_ACCESS_COARSE_LOCATION = 9092;

    private static final String TAG = "SplashActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(Util.isDebug() == false){
            AppCenter.start(getApplication(), "60ca62e2-92aa-44f8-9226-cb7fd9d392c7",
                    Analytics.class, Crashes.class);
        }

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        keyHash();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (checkPlayServices())
                    check();

            }
        }, 2000);
    }

    protected void allParent() {
        check();
        super.allParent();

    }


    private void check() {
        boolean perm1 = checkPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, PERM_REQ_ACCESS_FINE_LOCATION, new PermCallback() {
            @Override
            public void permGranted(int resultCode) {
                checkToken();
            }

            @Override
            public void permDenied(int resultCode) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                         check();

                    }
                }, 2000);
            }
        });
        //Util.log(TAG,"check.perm1 "+perm1);
        /*
        boolean perm2 = checkPermissions(new String[]{}, PERM_REQ_ACCESS_COARSE_LOCATION, new PermCallback() {
            @Override
            public void permGranted(int resultCode) {
                check();
            }

            @Override
            public void permDenied(int resultCode) {
                check();
            }
        });
        */
        if(perm1){
            checkToken();
        }
    }

    private void checkToken(){
        RequestParams params = new RequestParams();
        String token = FirebaseInstanceId.getInstance().getToken();
        if (token != null)
            params.put("AuthSession[device_token]", token);
        else
            params.put("AuthSession[device_token]", getUniqueDeviceId());
        syncManager.sendToServer("api/user/check", params, this);
        //Util.log(TAG,"checkToken");
    }

    @Override
    public void onSyncStart() {

    }

    @Override
    public void onSyncFailure(VolleyError error, Request mRequest) {
        super.onSyncFailure(error, mRequest);
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (action.equals("check")&&controller.equals("user")) {
            if (status) {
                JSONObject object = null;
                Intent intent=new Intent(this, MainActivity.class);
                try {
                    object = jsonObject.getJSONObject("detail");

                    ProfileData profileData=parseProfileData(object);
                    saveProfileDataInPrefStore(profileData);
                    if (object.has("addresses")) {
                        JSONObject Addressobject = object.getJSONObject("addresses");
                        if (Addressobject.has("Home"))
                            store.saveString(Const.HOME_ADDRESS, Addressobject.getJSONObject("Home").getString("title"));
                        if (Addressobject.has("Work"))
                            store.saveString(Const.WORK_ADDRESS, Addressobject.getJSONObject("Work").getString("title"));
                    } else {
                        store.saveString(Const.HOME_ADDRESS, "");
                        store.saveString(Const.WORK_ADDRESS, "");
                    }

                    if (object.has("last_ride")) {
                        RideDetails rideDetails = parseRideDetails(object.getJSONObject("last_ride"));
                        if (rideDetails != null) {
                            intent.putExtra("rideDetail", rideDetails);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                startActivity(intent);
                finish();
            } else {
                startActivity(new Intent(this, LoginSignUpActivity.class));
                finish();
            }
        }
    }
}

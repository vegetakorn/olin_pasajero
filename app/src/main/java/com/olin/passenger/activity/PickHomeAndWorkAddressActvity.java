package com.olin.passenger.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.olin.passenger.R;
import com.olin.passenger.adapter.AutoCompleteAdapter;
import com.olin.passenger.data.PlaceAutoComplete;
import com.olin.passenger.data.PlacePredictions;
import com.olin.passenger.utils.GoogleApisHandle;
import com.olin.passenger.utils.VolleyJSONRequest;
import com.olin.passenger.utils.VolleySingleton;
import com.toxsl.volley.Request;
import com.toxsl.volley.Response;
import com.toxsl.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class PickHomeAndWorkAddressActvity extends AppCompatActivity implements Response.Listener<String>, Response.ErrorListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    double latitude;
    double longitude;
    double hwlat, hwlng;
    private ListView mAutoCompleteList;
    private EditText Address;
    private String GETPLACESHIT = "places_hit";
    private PlacePredictions predictions;
    private Location mLastLocation;
    private AutoCompleteAdapter mAutoCompleteAdapter;
    private int CUSTOM_AUTOCOMPLETE_REQUEST_CODE = 20;
    private static final int MY_PERMISSIONS_REQUEST_LOC = 30;
    private ImageView searchBtn;
    private FragmentManager fragmentManager;
    private String preFilledText;
    private Handler handler;
    private VolleyJSONRequest request;
    private GoogleApiClient mGoogleApiClient;
    private static VolleySingleton volleyQueueInstance;

    private TextView toolbar_titleTV;
    private Toolbar toolbar;
    private TextView currentLocationTV;
    private TextView errorMessageTV;
    private ImageView backArrow;
    private TextView saveTV;
    private InputMethodManager inputMethodManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_home_and_work_address_actvity);
        inputMethodManager = (InputMethodManager) this
                .getSystemService(BaseActivity.INPUT_METHOD_SERVICE);
        hideSoftKeyboard();
        instantiateVolleyQueue();
        toolbar_titleTV = (TextView) findViewById(R.id.toolbar_titleTV);
        saveTV = (TextView) findViewById(R.id.saveTV);
        if (getIntent().hasExtra("Search Text")) {
            preFilledText = getIntent().getStringExtra("Search Text");
        }
        toolbar_titleTV.setText(getIntent().getStringExtra("title"));
        backArrow = (ImageView) findViewById(R.id.backArrow);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        saveTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Address.getText().toString().trim().equals("")) {
                    hideSoftKeyboard();
                    Intent intent = new Intent();
                    intent.putExtra("Location Address", Address.getText().toString());
                    intent.putExtra("lat", hwlat);
                    intent.putExtra("lng", hwlng);
                    setResult(CUSTOM_AUTOCOMPLETE_REQUEST_CODE, intent);
                    finish();
                } else {
                    Toast.makeText(PickHomeAndWorkAddressActvity.this, "Firstly select the address", Toast.LENGTH_SHORT).show();
                }
            }
        });
        fragmentManager = getSupportFragmentManager();
        Address = (EditText) findViewById(R.id.adressText);
        mAutoCompleteList = (ListView) findViewById(R.id.searchResultLV);
        searchBtn = (ImageView) findViewById(R.id.search);
        errorMessageTV = (TextView) findViewById(R.id.errorMessageTV);
        //get permission for Android M
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            fetchLocation();
        } else {

            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOC);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            } else {
                fetchLocation();
            }
        }


        //Add a text change listener to implement autocomplete functionality
        Address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // optimised way is to start searching for laction after user has typed minimum 3 chars
                if (Address.getText().length() > 3) {


                    searchBtn.setVisibility(View.GONE);

                    Runnable run = new Runnable() {


                        @Override
                        public void run() {

                            // cancel all the previous requests in the queue to optimise your network calls during autocomplete search
                            volleyQueueInstance.cancelRequestInQueue(GETPLACESHIT);

                            //build Get url of Place Autocomplete and hit the url to fetch result.
                            request = new VolleyJSONRequest(Request.Method.GET, getPlaceAutoCompleteUrl(Address.getText().toString()), null, null, PickHomeAndWorkAddressActvity.this, PickHomeAndWorkAddressActvity.this);

                            //Give a tag to your request so that you can use this tag to cancle request later.
                            request.setTag(GETPLACESHIT);

                            volleyQueueInstance.addToRequestQueue(request);

                        }

                    };

                    // only canceling the network calls will not help, you need to remove all callbacks as well
                    // otherwise the pending callbacks and messages will again invoke the handler and will send the request
                    if (handler != null) {
                        handler.removeCallbacksAndMessages(null);
                    } else {
                        handler = new Handler();
                    }
                    handler.postDelayed(run, 1000);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

        Address.setText(preFilledText);
        Address.setSelection(Address.getText().length());

        mAutoCompleteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hwlng = 0.0;
                hwlng = 0.0;
                PlaceAutoComplete placeAutocomplete = (PlaceAutoComplete) parent.getItemAtPosition(position);
                Address.setText(placeAutocomplete.getPlaceDesc());
                hwlat = Double.parseDouble(placeAutocomplete.getLat());
                hwlng = Double.parseDouble(placeAutocomplete.getLng());

            }
        });

    }

    public boolean hideSoftKeyboard() {
        try {
            if (getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (hideSoftKeyboard())
                    finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void instantiateVolleyQueue() {
        volleyQueueInstance = VolleySingleton.getInstance(getApplicationContext());
    }


    public String getPlaceAutoCompleteUrl(String input) {
        StringBuilder urlString = new StringBuilder();

        urlString.append("https://maps.googleapis.com/maps/api/place/textsearch/json");
        urlString.append("?input=");

        try {
            urlString.append(URLEncoder.encode(input, "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        urlString.append("&types=address");
        urlString.append("&location=");
        urlString.append(latitude + "," + longitude); // append lat long of current location to show nearby results.
        urlString.append("&radius=500&language=en");
        urlString.append("&key=" + GoogleApisHandle.API_KEY);

        Log.d("FINAL URL:::   ", urlString.toString());
        return urlString.toString();
    }

    @Override
    public void onErrorResponse(VolleyError error, Request request) {
        Log.d("---", "");
    }

    ArrayList<PlaceAutoComplete> placeAutocompletes;

    @Override
    public void onResponse(String response) {
        Log.d("PLACES RESULT:::", response);
        placeAutocompletes = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(response);
            if (object.has("error_message")) {
                errorMessageTV.setVisibility(View.VISIBLE);
                errorMessageTV.setText(object.optString("error_message"));
                mAutoCompleteList.setVisibility(View.GONE);
            } else if (object.has("results")) {
                errorMessageTV.setVisibility(View.GONE);
                mAutoCompleteList.setVisibility(View.VISIBLE);
                JSONArray jsonArray = object.getJSONArray("results");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object1 = jsonArray.getJSONObject(i);
                    PlaceAutoComplete placeAutoComplete = new PlaceAutoComplete();
                    if (object1.has("vicinity")) {
                        if (object1.getString("name").equalsIgnoreCase(object1.getString("vicinity")))
                            placeAutoComplete.setPlaceDesc(object1.getString("name"));
                        else
                            placeAutoComplete.setPlaceDesc(object1.getString("name") + "," + object1.getString("vicinity"));
                    } else {

                        placeAutoComplete.setPlaceDesc(object1.getString("formatted_address"));
                    }
                    JSONObject object2 = object1.getJSONObject("geometry").getJSONObject("location");
                    placeAutoComplete.setLat(object2.getString("lat"));
                    placeAutoComplete.setLng(object2.getString("lng"));
                    placeAutoComplete.setPlaceID(object1.getString("place_id"));
                    placeAutoComplete.setPlaceIcon(object1.getString("icon"));
                    placeAutocompletes.add(placeAutoComplete);
                }
            } else {
                JSONArray jsonArray = object.getJSONArray("predictions");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object1 = jsonArray.getJSONObject(i);
                    PlaceAutoComplete placeAutoComplete = new PlaceAutoComplete();
                    placeAutoComplete.setPlaceDesc(object1.getString("description"));
                    placeAutoComplete.setPlaceID(object1.getString("id"));
                    placeAutoComplete.setPlaceIcon("");
                    placeAutocompletes.add(placeAutoComplete);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mAutoCompleteAdapter = null;
        mAutoCompleteAdapter = new AutoCompleteAdapter(this, placeAutocompletes, PickHomeAndWorkAddressActvity.this);
        mAutoCompleteList.setAdapter(mAutoCompleteAdapter);
    }

    public void hideSoftKeyboard(PickLocationActivity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity
                    .getSystemService(BaseActivity.INPUT_METHOD_SERVICE);
            if (activity.getCurrentFocus() != null)
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        try {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            if (mLastLocation != null) {
                latitude = mLastLocation.getLatitude();
                longitude = mLastLocation.getLongitude();
            }

        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void fetchLocation() {
        //Build google API client to use fused location
        buildGoogleApiClient();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOC: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission granted!
                    LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
                    manager.sendBroadcast(new Intent("Perm Granted"));
                    fetchLocation();

                } else {
                    // permission denied!

                    Toast.makeText(this, "Please grant permission for using this app!", Toast.LENGTH_LONG).show();
                }
                return;
            }


        }
    }
}


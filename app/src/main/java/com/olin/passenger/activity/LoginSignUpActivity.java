package com.olin.passenger.activity;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.olin.passenger.R;
import com.olin.passenger.fragment.LoginFragment;

public class LoginSignUpActivity extends BaseActivity {

    public Toolbar toolbar;
    public TextView toolbar_titleTV;
    private ImageView toolbar_imageIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_sign_up);
        setToolbar();
        gotoLoginFragment();
    }

    private void gotoLoginFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new LoginFragment()).commitAllowingStateLoss();
    }

    public void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_titleTV = (TextView) findViewById(R.id.toolbar_titleTV);
        toolbar_imageIV = (ImageView) findViewById(R.id.toolbar_imageIV);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        toolbar.setBackgroundColor(getResources().getColor(R.color.bg_color));
        toolbar_titleTV.setTextColor(getResources().getColor(R.color.White));
        toolbar.setVisibility(View.VISIBLE);
        toolbar_titleTV.setVisibility(View.VISIBLE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Drawable upArrow = getResources().getDrawable(R.mipmap.ic_back_black);
        upArrow.setColorFilter(getResources().getColor(R.color.White), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment instanceof LoginFragment)
            back();
        else
            getSupportFragmentManager().popBackStack();
    }

    public void setToolbarTitle(String title) {
        toolbar_titleTV.setVisibility(View.VISIBLE);
        toolbar_titleTV.setText(title);
    }
}

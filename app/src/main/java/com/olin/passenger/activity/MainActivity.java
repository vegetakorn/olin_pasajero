package com.olin.passenger.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pkmmte.view.CircularImageView;
import com.olin.passenger.R;
import com.olin.passenger.adapter.DrawerListAdapter;
import com.olin.passenger.data.ProfileData;
import com.olin.passenger.data.RideDetails;
import com.olin.passenger.data.WalletData;
import com.olin.passenger.fragment.ChangePasswordFragment;
import com.olin.passenger.fragment.CouponCodeFragment;
import com.olin.passenger.fragment.DriverProfileDetailsFragment;
import com.olin.passenger.fragment.DriverRateFragment;
import com.olin.passenger.fragment.HomeFragment;
import com.olin.passenger.fragment.MyBookingsFragment;
import com.olin.passenger.fragment.PaymentFragment;
import com.olin.passenger.fragment.ProfileFragment;
import com.olin.passenger.fragment.RideRequestFragment;
import com.olin.passenger.fragment.RideStartFragment;
import com.olin.passenger.fragment.SettingsFragment;
import com.olin.passenger.fragment.WalletFragment;
import com.olin.passenger.utils.Const;
import com.olin.passenger.utils.Util;

import org.json.JSONException;
import org.json.JSONObject;

import me.tangke.slidemenu.SlideMenu;

/**
 * Created by neeraj.narwal on 5/7/16.
 */
public class MainActivity extends BaseActivity {

    private final String TAG = "MainActivity";
    public SlideMenu slideMenu;
    public CircularImageView profileCIV;
    private HomeFragment homeFragment;

    AdapterView.OnItemClickListener seekerDrawerListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Fragment frag = null;
            switch (position) {
                case 0:
                    frag = new MyBookingsFragment();
                    break;
                case 1:
                    //frag = new WalletFragment();
                    frag = new SettingsFragment();
                    break;
                case 2:
                    //frag = new SettingsFragment();
                    logout();
                    break;
                case 3:
                    //frag = new CouponCodeFragment();
                    break;
                case 4:
                    //logout();
                    break;
            }
            slideMenu.close(true);
            if (frag != null)
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, frag)
                        .addToBackStack(null)
                        .commit();
        }
    };
    private AlertDialog dialog;
    private BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Const.DISPLAY_MESSAGE_ACTION)) {
                Bundle pushData = intent.getBundleExtra("map");

                String action = (String) pushData.get("action");
                String controller = (String) pushData.get("controller");
                log(action + " " + controller);
                if (action != null && controller != null) {
                    if (controller.equals("ride") && action.equalsIgnoreCase("cancel")) {
                        goToHomeFragment();
                    } else if (controller.equals("ride") && action.equalsIgnoreCase("accept")) {
                        try {
                            gotoStartRideFrag(Integer.parseInt(pushData.getString("id")));
                            cancelNotification(Const.ALL_NOTI);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (controller.equals("ride") && action.equalsIgnoreCase("arrived")) {
                        try {
                            gotoStartRideFrag(Integer.parseInt(pushData.getString("id")));
                            showMessage(R.string.driver_has_arrived);
                            cancelNotification(Const.ALL_NOTI);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (controller.equals("ride") && action.equalsIgnoreCase("started")) {
                        try {
                            gotoStartRideFrag(Integer.parseInt(pushData.getString("id")));
                            showMessage(R.string.journey_has_begun);
                            cancelNotification(Const.ALL_NOTI);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (controller.equals("ride") && action.equalsIgnoreCase("completed")) {
                        try {
                            gotoPaymentFrag(Integer.parseInt(pushData.getString("id")));
                            showMessage(R.string.trip_is_complete);
                            cancelNotification(Const.ALL_NOTI);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (action.equalsIgnoreCase("paid")) {
                        try {
                            goToHomeFragment();
                            cancelNotification(Const.ALL_NOTI);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (controller.equals("coupon") && action.equalsIgnoreCase("add")) {
                        gotoCouponFragment();
                        cancelNotification(Const.ALL_NOTI);
                    }


                }
            }
        }
    };
    private TextView walletTV;
    private long times_in_millies = 60 * 1000 * 5;  // 5 minutes timer
    private CountDownTimer cTimer;
    private long remianing_times_in_millies;
    private FrameLayout container;
    private Toolbar toolbar;
    private ListView drawerLV;
    private TextView toolbar_titleTV, viewProfileTV;
    private ImageView toolbar_imageIV;
    private TextView userNameTV;
    private ImageView backArrow;
    private LinearLayout profileLL;

    private void gotoCouponFragment() {

        Fragment fragment = new CouponCodeFragment();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }

    public void gotoStartRideFrag(int id) {
        Util.log(TAG,"gotoStartRideFrag "+id);
        RideStartFragment fragment = new RideStartFragment();
        fragment.setHomeFragment(homeFragment);
        Bundle bundle = new Bundle();
        bundle.putInt("ride_id", id);
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commitAllowingStateLoss();
    }

    public void gotoPaymentFrag(int id) {
        Fragment fragment = new PaymentFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("ride_id", id);
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        if (getIntent().hasExtra("rideDetail")) {
            RideDetails rideDetail = getIntent().getParcelableExtra("rideDetail");
            gotoCurrentState(rideDetail);
        } else if (getIntent().hasExtra("map")) {
            Bundle bundle = new Bundle();
            bundle = getIntent().getBundleExtra("map");

            try {
                if (bundle.getString("controller").equals("coupon") && bundle.getString("action").equals("add")) {
                    gotoCouponFragment();
                } else {
                    getJourneyDetailSingleShot(bundle.getString("id"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            gotoCurrentState(null);
        }
        hideSoftKeyboard();
    }

    @Override
    protected void onResume() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Const.DISPLAY_MESSAGE_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHandleMessageReceiver, intentFilter);

        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mHandleMessageReceiver != null)
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mHandleMessageReceiver);

    }

    public void gotoCurrentState(RideDetails journeyDetail) {
        if (journeyDetail != null) {
            switch (journeyDetail.state_id) {
                case Const.STATE_COUPON:
                    gotoCouponFragment();
                    break;
                case Const.STATE_NEW:
                    if (journeyDetail.journey_type == Const.RIDE_NOW) {
                        gotoRideRequestFragment(journeyDetail.id);
                    } else {
                        goToHomeFragment();
                    }
                    cancelNotification(Const.ALL_NOTI);
                    break;
                case Const.STATE_ACCEPTED:
                    gotoStartRideFrag(journeyDetail.id);
                    cancelNotification(Const.ALL_NOTI);
                    break;
                case Const.STATE_DRIVER_ARRIVED:
                    gotoStartRideFrag(journeyDetail.id);
                    cancelNotification(Const.ALL_NOTI);
                    break;
                case Const.STATE_STARTED:
                    gotoStartRideFrag(journeyDetail.id);
                    cancelNotification(Const.ALL_NOTI);
                    break;
                case Const.STATE_COMPLETED:
                    gotoPaymentFrag(journeyDetail.id);
                    cancelNotification(Const.ALL_NOTI);
                    break;
                case Const.STATE_CANCELLED:

                    goToHomeFragment();
                    break;
                case Const.STATE_PAID:
                    goToHomeFragment();
                    break;
                default:
                    goToHomeFragment();
                    break;
            }
        } else {
            goToHomeFragment();
        }
    }

    private void gotoRideRequestFragment(int id) {
        Fragment fragment = new RideRequestFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("ride_id", id);
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commitAllowingStateLoss();
    }

    private void getJourneyDetailSingleShot(String id) {
        syncManager.sendToServer(Const.API_RIDE_DETAIL + "?id=" + id, null, this);

    }

    private void init() {
        container = (FrameLayout) findViewById(R.id.container);
        slideMenu = (SlideMenu) findViewById(R.id.slideMenu);

        profileCIV = (CircularImageView) findViewById(R.id.profileCIV);

        userNameTV = (TextView) findViewById(R.id.userNameTV);
        profileLL = (LinearLayout) findViewById(R.id.profileLL);
        walletTV = (TextView) findViewById(R.id.walletTV);
        userNameTV.setOnClickListener(this);
        profileLL.setOnClickListener(this);
        slideMenu.setEdgeSlideEnable(false);
        slideMenu.setEdgetSlideWidth(0);
        slideMenu.setPrimaryShadowWidth(0);
        slideMenu.setOnSlideStateChangeListener(new SlideMenu.OnSlideStateChangeListener() {
            @Override
            public void onSlideStateChange(int slideState) {
                if (SlideMenu.STATE_OPEN_LEFT == slideState) {
                    getWalletDetails(getProfileDataFromPrefStore().id);
                }
            }

            @Override
            public void onSlideOffsetChange(float offsetPercent) {

            }
        });


        setToolbar();
        updateDrawer();
        goToHomeFragment();
    }

    public void getWalletDetails(int id) {
        syncManager.sendToServer(Const.API_USER_WALLET_GET + "?id=" + id, null, this);
    }

    public void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_titleTV = (TextView) findViewById(R.id.toolbar_titleTV);
        toolbar_imageIV = (ImageView) findViewById(R.id.toolbar_imageIV);
        backArrow = (ImageView) findViewById(R.id.backArrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_drawer);
        getSupportActionBar().setTitle("");
        backArrow.setOnClickListener(this);
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
                if (fragment instanceof HomeFragment)
                    slideMenu.open(false, true);
                else {
                    if (fragment instanceof ProfileFragment
                            || fragment instanceof DriverProfileDetailsFragment
                            || fragment instanceof MyBookingsFragment
                            || fragment instanceof WalletFragment
                            || fragment instanceof CouponCodeFragment
                            || fragment instanceof SettingsFragment
                            || fragment instanceof ChangePasswordFragment
                            ) {
                        onBackPressed();
                    }
                }
            }
        });
    }

    public void setActionBarTitle(String title, boolean isBackArrow) {

        if (title.equals("")) {
            toolbar_titleTV.setVisibility(View.GONE);
            toolbar.setVisibility(View.GONE);
        } else {
            toolbar.setVisibility(View.VISIBLE);
            toolbar_titleTV.setVisibility(View.VISIBLE);
            toolbar_titleTV.setText(title);
        }

        if (isBackArrow) {
            getSupportActionBar().setHomeAsUpIndicator(null);
            backArrow.setVisibility(View.VISIBLE);
        } else {
            getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_drawer);
            backArrow.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.profileLL:
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new ProfileFragment()).addToBackStack(null).commitAllowingStateLoss();
                slideMenu.close(true);
                break;
            case R.id.backArrow:
                getSupportFragmentManager().popBackStack();
                break;

        }
    }

    public void goToHomeFragment() {
        Util.log(TAG,"goToHomeFragment");
        if(homeFragment == null){
            homeFragment = new HomeFragment();
        }
        if(getSupportFragmentManager().getFragments().contains(homeFragment)){
            getSupportFragmentManager().getFragments().remove(homeFragment);
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.container, homeFragment).addToBackStack(null).commitAllowingStateLoss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("SetTextI18n")
    public void updateDrawer() {
        drawerLV = (ListView) findViewById(R.id.drawerLV);
        String[] navigationMenu = getResources().getStringArray(R.array.drawer_item_string);
        //Integer integer[] = {R.mipmap.ic_drawer_journey, R.mipmap.ic_drawer_wallet, R.mipmap.ic_drawer_settings, R.mipmap.ic_coupan, R.mipmap.ic_drawer_logout};
        Integer integer[] = {R.mipmap.ic_drawer_journey, R.mipmap.ic_drawer_settings,R.mipmap.ic_drawer_logout};
        DrawerListAdapter listAdapter = new DrawerListAdapter(this, navigationMenu, integer, this);
        drawerLV.setAdapter(listAdapter);
        drawerLV.setOnItemClickListener(seekerDrawerListener);
        ProfileData profileData = getProfileDataFromPrefStore();
        WalletData walletData = getWalletDataFromPrefStore();
        if (profileData.image_file != null && !profileData.image_file.isEmpty())
            try {
                picasso.load(profileData.image_file).error(R.mipmap.default_avatar).into(profileCIV);
            } catch (Exception e) {
                e.printStackTrace();
            }
        else {
            picasso.load(R.mipmap.default_avatar).error(R.mipmap.default_avatar).into(profileCIV);
        }

        if (walletData != null) {
            walletTV.setText(getString(R.string.wallet).concat(getString(R.string.appx_price) + (" " + walletData.amount)));
        }
        userNameTV.setText(profileData.first_name + " " + profileData.last_name);
    }

    public void updateWallet(String sym, String amount) {
        walletTV.setText(getString(R.string.wallet).concat(sym.concat(" " + amount)));
    }

    private void logout() {
        cancelNotification(Const.ALL_NOTI);
        syncManager.sendToServer("api/user/logout", null, this);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (action.equals("logout")) {
            if (status) {
                syncManager.setLoginStatus(null);
                startActivity(new Intent(this, LoginSignUpActivity.class));
                finish();
            }
        } else if (controller.equals("ride") && action.equals("detail")) {
            if (status) {
                JSONObject detail = jsonObject.optJSONObject("detail");
                RideDetails rideDetails = parseRideDetails(detail);
                gotoCurrentState(rideDetails);
            } else {
                log(jsonObject.optString("error"));
            }
        } else if (controller.equals("user-wallet") && action.equals("get")) {
            if (status) {
                walletTV.setText(getString(R.string.wallet) + jsonObject.optJSONObject("list").optString("amount"));
            } else {
                try {
                    showToast(jsonObject.getString("error"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment instanceof HomeFragment || fragment instanceof RideStartFragment)
            fragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment instanceof HomeFragment) {
            back();

        } else if (fragment instanceof ProfileFragment
                || fragment instanceof DriverProfileDetailsFragment
                || fragment instanceof MyBookingsFragment
                || fragment instanceof CouponCodeFragment
                || fragment instanceof WalletFragment
                || fragment instanceof SettingsFragment
                || fragment instanceof ChangePasswordFragment
                ) {
            getSupportFragmentManager().popBackStack();
            goToHomeFragment();
        } else if (fragment instanceof RideRequestFragment
                || fragment instanceof RideStartFragment
                || fragment instanceof PaymentFragment
                || fragment instanceof DriverRateFragment
                ) {
        }
    }

    void startTimer() {
        cTimer = new CountDownTimer(times_in_millies, 1000) {
            public void onTick(long millisUntilFinished) {
                remianing_times_in_millies = millisUntilFinished;
                int seconds = (int) (millisUntilFinished / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;

            }

            public void onFinish() {

                cTimer = null;
            }
        };
        cTimer.start();
    }

    private void showMessage(int resId){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.attention))
                .setCancelable(true)
                .setPositiveButton(getString(R.string.to_accept), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setMessage(getString(resId));
        builder.create().show();
    }


    void cancelTimer() {
        if (cTimer != null)
            cTimer.cancel();
    }
}

package com.olin.passenger.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.StrictMode;
import android.os.SystemClock;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.olin.passenger.BuildConfig;
import com.olin.passenger.R;
import com.olin.passenger.data.CountryData;
import com.olin.passenger.data.DriverData;
import com.olin.passenger.data.ProfileData;
import com.olin.passenger.data.RideDetails;
import com.olin.passenger.data.Vehicle;
import com.olin.passenger.data.WalletData;
import com.olin.passenger.service.LocationUpdateService;
import com.olin.passenger.utils.Const;
import com.olin.passenger.utils.GoogleApisHandle;
import com.olin.passenger.utils.ImageUtils;
import com.olin.passenger.utils.LatLngInterpolator;
import com.olin.passenger.utils.NetworkUtil;
import com.olin.passenger.utils.PrefStore;
import com.olin.passenger.utils.Util;
import com.toxsl.volley.AppExpiredError;
import com.toxsl.volley.AppInMaintenance;
import com.toxsl.volley.AuthFailureError;
import com.toxsl.volley.NetworkError;
import com.toxsl.volley.ParseError;
import com.toxsl.volley.Request;
import com.toxsl.volley.ServerError;
import com.toxsl.volley.TimeoutError;
import com.toxsl.volley.VolleyError;
import com.toxsl.volley.toolbox.RequestParams;
import com.toxsl.volley.toolbox.SyncEventListner;
import com.toxsl.volley.toolbox.SyncManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by tx-ubuntu-207 on 2/7/15.
 */
public class BaseActivity extends AppCompatActivity implements SyncEventListner, View.OnClickListener {

    private static String TAG = "BaseActivity";
    public static GoogleApisHandle googleApiHandle;
    public static LocationUpdateService locationUpdateService;
    public LayoutInflater inflater;
    public SyncManager syncManager;
    public PrefStore store;
    public PermCallback permCallback;
    public Picasso picasso;
    public boolean isEdit;
    private Toast toast;
    private Dialog progressDialog;
    private TextView txtMsgTV;
    private int reqCode;
    private NetworksBroadcast networksBroadcast;
    private AlertDialog networkAlertDialog;
    private String networkStatus;
    private InputMethodManager inputMethodManager;
    private android.app.AlertDialog.Builder failureDailog;
    private android.app.AlertDialog errorAlertDialog;;
    private AlertDialog.Builder networkDialog;
    private boolean exit;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(Const.DISPLAY_MESSAGE_ACTION)) {
                Bundle data = intent.getBundleExtra("map");

            } else if (intent.getAction().equalsIgnoreCase(Const.FCM_TOKEN_REFRESH)) {
                Bundle fcmBundle = intent.getExtras();
                updateTokenInServer(fcmBundle.getString(Const.FCM_TOKEN_REFRESH));
            }
        }
    };

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        syncManager = SyncManager.getInstance(this, BuildConfig.DEBUG);
        (BaseActivity.this).overridePendingTransition(R.anim.slide_in,
                R.anim.slide_out);
        inputMethodManager = (InputMethodManager) this
                .getSystemService(BaseActivity.INPUT_METHOD_SERVICE);
        store = new PrefStore(this);
        googleApiHandle = GoogleApisHandle.getInstance(this);
        locationUpdateService = LocationUpdateService.getInstance(this);
        FirebaseApp.initializeApp(this);
        registerFCM();
        initializeNetworkBroadcast();
        createPicassoDownloader();
        strictModeThread();
        checkLanguage();
        toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        transitionSlideInHorizontal();
        progressDialog();
        failureDailog = new android.app.AlertDialog.Builder(this);
        networkDialog = new AlertDialog.Builder(this);

        //ExceptionHandler.register(this);
    }

    public void checkLanguage() {
        String language;
        String languageToLoad = "es";
        try {
            if (store.getString("language") == null || store.getString("language").equalsIgnoreCase(""))
                language = "Spanish";
            else
                language = store.getString("language");

            if (language.equalsIgnoreCase("Spanish")) {
                languageToLoad = "es";

            } else if (language.equalsIgnoreCase("English")) {
                languageToLoad = "en";
            }

            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());

            syncManager.setBaseUrl(Const.SERVER_REMOTE_URL, getString(R.string.app_name), languageToLoad);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createPicassoDownloader() {
        OkHttpClient okHttpClient = new OkHttpClient();
        picasso = new Picasso.Builder(this)
                .downloader(new OkHttpDownloader(okHttpClient))
                .build();
        picasso.with(this);
    }

    private void initializeNetworkBroadcast() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        networksBroadcast = new NetworksBroadcast();
        registerReceiver(networksBroadcast, intentFilter);
    }

    private void registerFCM() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Const.DISPLAY_MESSAGE_ACTION);
        intentFilter.addAction(Const.FCM_TOKEN_REFRESH);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        Intent intent = new Intent(Const.FCM_TOKEN_REFRESH);
                        intent.putExtra(Const.FCM_TOKEN_REFRESH, token);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                    }
                });
    }

    private void updateTokenInServer(String token) {
        RequestParams requestParams = new RequestParams();
        Util.log("BaseActivity", "------device_token----- " + token);
        requestParams.put("AuthSession[device_token]", token);
        syncManager.sendToServer("api/user/check", requestParams, null);
    }

    private void unregisterNetworkBroadcast() {
        try {
            if (networksBroadcast != null) {
                unregisterReceiver(networksBroadcast);
            }
        } catch (IllegalArgumentException e) {
            networksBroadcast = null;
        }
    }

    private void showNoNetworkDialog(String status) {
        networkStatus = status;
        networkDialog.setTitle(getString(R.string.netwrk_status));
        networkDialog.setMessage(status);
        networkDialog.setPositiveButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!isNetworkAvailable()) {
                    dialog.dismiss();
                    showNoNetworkDialog(networkStatus);
                }
            }
        });
        networkDialog.setCancelable(false);
        networkAlertDialog = networkDialog.create();
        if (!networkAlertDialog.isShowing())
            networkAlertDialog.show();
    }

    public String changeDateFormat(String dateString, String sourceDateFormat, String targetDateFormat) {
        if (dateString == null || dateString.isEmpty()) {
            return "";
        }
        SimpleDateFormat inputDateFromat = new SimpleDateFormat(sourceDateFormat, Locale.getDefault());
        Date date = new Date();
        try {
            date = inputDateFromat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat outputDateFormat = new SimpleDateFormat(targetDateFormat, Locale.getDefault());
        return outputDateFormat.format(date);
    }

    public String changeDateFormatFromDate(Date sourceDate, String targetDateFormat) {
        if (sourceDate == null || targetDateFormat == null || targetDateFormat.isEmpty()) {
            return "";
        }
        SimpleDateFormat outputDateFormat = new SimpleDateFormat(targetDateFormat, Locale.getDefault());
        return outputDateFormat.format(sourceDate);
    }

    public String changeDateFormatFromDateUTC(Date sourceDate, String targetDateFormat) {
        if (sourceDate == null || targetDateFormat == null || targetDateFormat.isEmpty()) {
            return "";
        }
        SimpleDateFormat outputDateFormat = new SimpleDateFormat(targetDateFormat, Locale.getDefault());
        outputDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return outputDateFormat.format(sourceDate);
    }

    public String changeDateFormatFromUTCdate(String sourceDate, String currentFormat, String targetFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(currentFormat);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date myDate = null;
        try {
            myDate = simpleDateFormat.parse(sourceDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return changeDateFormatFromDate(myDate, targetFormat);
    }

    /**
     * Conviete una fecha UTC del serivdor a la hora local del usaurio
     * @param dateFomratOutPut
     * @param datesToConvert
     * @return
     */
    public static String convertUTCToLocal(String datesToConvert,String dateFomratOutPut) {
        String fotmatFromServer = "yyyy-MM-dd hh:mm:ss";
        String dateToReturn = datesToConvert;

        SimpleDateFormat sdf = new SimpleDateFormat(fotmatFromServer);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date gmt = null;
        //Util.log(TAG,"convertUTCToLocal timezone: "+TimeZone.getDefault().getDisplayName());
        SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat(dateFomratOutPut);
        sdfOutPutToSend.setTimeZone(TimeZone.getDefault());

        try {

            gmt = sdf.parse(datesToConvert);
            dateToReturn = sdfOutPutToSend.format(gmt);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateToReturn;
    }

    protected void checkDate(String checkDate) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date serverDate = null;
        try {
            serverDate = dateFormat.parse(checkDate);
            cal.setTime(serverDate);
            Calendar currentcal = Calendar.getInstance();
            if (currentcal.after(cal)) {
                androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
                builder.setMessage("Please contact : shiv@toxsl.com");
                builder.setTitle("Demo Expired");
                builder.setCancelable(false);
                builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        exitFromApp();
                    }
                });
                androidx.appcompat.app.AlertDialog alert = builder.create();
                alert.show();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, Const.PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                log(getString(R.string.this_device_is_not_supported));
                finish();
            }
            return false;
        }
        return true;
    }

    public void callIntent(String s) {
        try {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + s.trim()));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            startActivity(intent);
        } catch (ActivityNotFoundException activityException) {
            log("Calling a Phone Number" + "Call failed" + activityException);
        }
    }

    public boolean checkPermissions(String[] perms, int requestCode, PermCallback permCallback) {
        this.permCallback = permCallback;
        this.reqCode = requestCode;
        ArrayList<String> permsArray = new ArrayList<>();
        boolean hasPerms = true;
        for (String perm : perms) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                permsArray.add(perm);
                hasPerms = false;
            }
        }
        if (!hasPerms) {
            String[] permsString = new String[permsArray.size()];
            for (int i = 0; i < permsArray.size(); i++) {
                permsString[i] = permsArray.get(i);
            }
            ActivityCompat.requestPermissions(BaseActivity.this, permsString, 99);
            return false;
        } else
            return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean permGrantedBool = false;
        switch (requestCode) {
            case 99:
                for (int grantResult : grantResults) {
                    if (grantResult == PackageManager.PERMISSION_DENIED) {
                        showToast(getString(R.string.not_sufficient_permissions)
                                + getString(R.string.app_name)
                                + getString(R.string.permissionss));
                        permGrantedBool = false;
                        break;
                    } else {
                        permGrantedBool = true;
                    }
                }
                if (permCallback != null) {
                    if (permGrantedBool) {
                        permCallback.permGranted(reqCode);
                    } else {
                        permCallback.permDenied(reqCode);
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ImageUtils.activityResult(requestCode, resultCode, data);
    }

    public void exitFromApp() {
        finish();
        finishAffinity();
    }

    public String getUniqueDeviceId() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public boolean hideSoftKeyboard() {
        try {
            if (getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null
                && activeNetworkInfo.isConnectedOrConnecting();

    }

    public boolean isValidMail(String email) {
        return email.matches("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+");
    }

    public boolean isValidPassword(String password) {
        return password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[!&^%$#@()=*/.+_-])(?=\\S+$).{8,}$");
    }

    public void keyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash >>>>:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    public void log(String string) {
        if (BuildConfig.DEBUG)
            Log.e("BaseActivity", string);
    }

    private void progressDialog() {
        progressDialog = new Dialog(this);
        View view = View.inflate(this, R.layout.progress_dialog, null);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(view);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        txtMsgTV = (TextView) view.findViewById(R.id.txtMsgTV);
        progressDialog.setCancelable(false);
    }

    public void startProgressDialog() {
        if (progressDialog != null && !progressDialog.isShowing() && !isFinishing()) {
            try {
                progressDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void stopProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                if (!isFinishing())
                    progressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onSyncStart() {
        startProgressDialog();
    }

    @Override
    public void onSyncFinish() {
        stopProgressDialog();
    }

    android.app.AlertDialog.Builder errorDialog(String errorString, final Request mRequest) {
        if (mRequest != null) {
            failureDailog.setMessage(errorString).setCancelable(false).setNegativeButton("EXIT", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            }).setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onSyncStart();
                    syncManager.getRequestQueue().add(mRequest);
                }
            });
        } else
            failureDailog.setMessage(errorString).setCancelable(false).setPositiveButton("EXIT", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        return failureDailog;
    }

    @Override
    public void onSyncFailure(VolleyError error, Request mRequest) {
        handleSyncError(error, mRequest);
    }

    private void handleSyncError(VolleyError error, Request mRequest) {
        if(errorAlertDialog != null){
            errorAlertDialog.hide();
        }
        errorAlertDialog = null;
        if (error instanceof NetworkError) {
            if (!isFinishing())
                errorAlertDialog = errorDialog(getString(R.string.request_timeout_slow_connection), mRequest).show();
        } else if (error instanceof ServerError) {
            showToast(getString(R.string.problem_connecting_to_the_server));
        } else if (error instanceof AuthFailureError) {
            showToast(getString(R.string.session_timeout_redirecting));
            syncManager.setLoginStatus(null);
            startActivity(new Intent(this, LoginSignUpActivity.class));
            finish();
        } else if (error instanceof ParseError) {
            showToast(getString(R.string.bad_request));
        } else if (error instanceof TimeoutError) {
            if (!isFinishing())
                errorAlertDialog = errorDialog(getString(R.string.request_timeout_slow_connection), mRequest).show();
        } else if (error instanceof AppExpiredError)
            checkDate(error.getMessage());
        else if (error instanceof AppInMaintenance) {
            if (!isFinishing())
                errorAlertDialog = errorDialog(error.getMessage(), null).show();
        }

    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        if (controller.equalsIgnoreCase("user-wallet") && action.equalsIgnoreCase("get")) {
            if (status) {
                parseWalletData(jsonObject);
            }

        }
    }

    public WalletData parseWalletData(JSONObject jsonObject) {
        JSONObject wallet = jsonObject.optJSONObject("list");
        WalletData walletData = new WalletData(Parcel.obtain());
        walletData.id = wallet.optInt("id");
        walletData.amount = wallet.optString("amount");
        walletData.currency_code = wallet.optString("currency_code");
        walletData.currency_symbol = wallet.optString("currency_symbol");
        saveWalletDataInPrefStore(walletData);
        return walletData;
    }

    public void showToast(String msg) {
        toast.setText(msg);
        toast.show();
    }

    private void strictModeThread() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void transitionSlideInHorizontal() {
        this.overridePendingTransition(R.anim.slide_in_right,
                R.anim.slide_out_left);
    }

    @Override
    public void onClick(View v) {

    }

    private void check() {
        RequestParams params = new RequestParams();
        String token = FirebaseInstanceId.getInstance().getToken();
        if (token != null)
            params.put("AuthSession[device_token]", token);
        else
            params.put("AuthSession[device_token]", getUniqueDeviceId());
        syncManager.sendToServer("api/user/check", params, this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterNetworkBroadcast();
    }

    public void back() {
        if (exit) {
            exitFromApp();
        } else {
            Toast.makeText(this, "Press again in order to exit TaxiSitio", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }

    public void saveProfileDataInPrefStore(ProfileData profileData) {
        Gson gson = new Gson();
        String json = gson.toJson(profileData);
        store.saveString("profile_data", json);
    }

    public ProfileData getProfileDataFromPrefStore() {
        Gson gson = new Gson();
        return gson.fromJson(store.getString("profile_data"), ProfileData.class);
    }

    public void saveWalletDataInPrefStore(WalletData profileData) {
        Gson gson = new Gson();
        String json = gson.toJson(profileData);
        store.saveString("wallet_data", json);
    }

    public WalletData getWalletDataFromPrefStore() {
        Gson gson = new Gson();
        return gson.fromJson(store.getString("wallet_data"), WalletData.class);
    }

    public ProfileData parseProfileData(JSONObject detail) throws JSONException {
        ProfileData profileData = new ProfileData(Parcel.obtain());
        profileData.id = detail.getInt("id");
        store.setInt(Const.MY_ID, profileData.id);
        profileData.first_name = detail.getString("first_name");
        profileData.last_name = detail.getString("last_name");
        profileData.email = detail.getString("email");
        profileData.is_fb = detail.getInt("is_fb");
        store.setBoolean("isFb", detail.getInt("is_fb") != 0);
        profileData.contact_no = detail.getString("contact_no");
        profileData.image_file = detail.getString("image_file");
        if (detail.has("wallet")) {
            JSONObject wallet = detail.optJSONObject("wallet");
            WalletData walletData = new WalletData(Parcel.obtain());
            walletData.id = wallet.optInt("id");
            walletData.amount = wallet.optString("amount");
            walletData.currency_code = wallet.optString("currency_code");
            walletData.currency_symbol = wallet.optString("currency_symbol");
            saveWalletDataInPrefStore(walletData);
            profileData.walletData = walletData;

        }
        if (detail.has("country")) {
            JSONObject country = detail.getJSONObject("country");
            CountryData countryData = new CountryData(Parcel.obtain());
            countryData.id = country.getInt("id");
            countryData.title = country.getString("title");
            countryData.telephone_code = country.getString("telephone_code");
            countryData.flag = country.getString("flag");
            countryData.country_code = country.getString("country_code");
            countryData.currency_symbol = country.getString("currency_symbol");

            if (country.has("car_type")) {
                JSONArray car_type = country.getJSONArray("car_type");
                int[] carTypes = new int[car_type.length()];
                for (int j = 0; j < car_type.length(); j++) {
                    carTypes[j] = (int) car_type.get(j);
                    countryData.arr_car_type = carTypes;
                }
            }
            profileData.country = countryData;
        }
        if (detail.has("sepomex") && detail.get("sepomex") instanceof JSONObject) {
            JSONObject sepomex = detail.getJSONObject("sepomex");
            profileData.settlement = sepomex.getString("settlement");
            profileData.municipality = sepomex.getString("municipality");
            profileData.state = sepomex.getString("state");
            profileData.zipcode = sepomex.getString("zipcode");

        }
        return profileData;
    }

    public void animateMarkerToGB(LatLng finalPosition, Marker oldMarker, boolean isCameraAnimate, GoogleMap googleMap) {
        LatLngInterpolator latLngInterpolator = new LatLngInterpolator.Spherical();
        LatLng startPosition = oldMarker.getPosition();
        double lat1 = startPosition.latitude;
        double lng1 = startPosition.longitude;
        double lat2 = finalPosition.latitude;
        double lng2 = finalPosition.longitude;
        long start = SystemClock.uptimeMillis();
        Interpolator interpolator = new AccelerateDecelerateInterpolator();
        float durationInMs = 9 * 1000;
        Handler handler = new Handler();
        googleApiHandle.rotateMarker(lat1, lng1, lat2, lng2, oldMarker, handler);
        handler.post(new MyRunnable(start, durationInMs, interpolator, oldMarker, latLngInterpolator, startPosition, finalPosition, isCameraAnimate, googleMap, handler) {
            long elapsed;
            float t;
            float v;

            @Override
            public void run() {                 // Calculate progress using interpolator
                elapsed = SystemClock.uptimeMillis() - start;
                t = elapsed / durationInMs;
                v = interpolator.getInterpolation(t);
                oldMarker.setPosition(latLngInterpolator.interpolate(v, startPosition, finalPosition));
                if (isCameraAnimate) {
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    builder.include(startPosition);
                    builder.include(finalPosition);
                    LatLngBounds bounds = builder.build();
                    int padding = 10;
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,
                            padding);
                    googleMap.animateCamera(cu);
                }
                //Repeat till progress is complete.
                if (t < 1) {                     // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }


    public RideDetails parseRideDetails(JSONObject detail) {
        if (detail != null) {
            RideDetails rideDetails = new RideDetails(Parcel.obtain());
            rideDetails.id = detail.optInt("id");
            if (detail.has("pageCount")) {
                rideDetails.pageCount = detail.optInt("pageCount");
            }
            store.setInt(Const.JOURNEY_ID, rideDetails.id);
            store.setInt(Const.MY_ID, rideDetails.id);
            rideDetails.amount = detail.optString("amount");
            rideDetails.location = detail.optString("location");
            rideDetails.destination = detail.optString("destination");
            rideDetails.location_lat = detail.optDouble("location_lat");
            rideDetails.location_long = detail.optDouble("location_long");
            rideDetails.destination_lat = detail.optDouble("destination_lat");
            rideDetails.destination_long = detail.optDouble("destination_long");
            rideDetails.number_of_passengers = detail.optString("number_of_passengers");
            rideDetails.number_of_bags = detail.optString("number_of_bags");
            rideDetails.coupon_applied = detail.optBoolean("coupon_applied");
            rideDetails.is_hourly = detail.optBoolean("is_hourly");
            if (detail.has("remaining_balance"))
                rideDetails.remaining_balance = detail.optDouble("remaining_balance");
            if (rideDetails.is_hourly) {
                rideDetails.number_of_hours = detail.optString("number_of_hours");
            }
            rideDetails.journey_time = detail.optString("journey_time");
            rideDetails.journey_type = detail.optInt("journey_type");
            rideDetails.start_time = detail.optString("start_time");
            rideDetails.end_time = detail.optString("end_time");
            rideDetails.create_time = detail.optString("create_time");
            rideDetails.image_file = detail.optString("image_file");
            rideDetails.state_id = detail.optInt("state_id");
            if (detail.has("driver_detail") && detail.optJSONObject("driver_detail") != null) {
                Gson gson = new Gson();
                JSONObject driver_detail = detail.optJSONObject("driver_detail");
                DriverData driverDetail = gson.fromJson(detail.optString("driver_detail"),DriverData.class);
                //driverDetail.id = driver_detail.optInt("id");
                //driverDetail.first_name = driver_detail.optString("first_name");
                //driverDetail.last_name = driver_detail.optString("last_name");
                //driverDetail.email = driver_detail.optString("email");
                //driverDetail.contact_no = driver_detail.optString("contact_no");
                //driverDetail.state_id = driver_detail.optString("state_id");
                //driverDetail.driver_rating = driver_detail.optDouble("driver_rating");
                //driverDetail.image_file = driver_detail.optString("image_file");
                //driverDetail.latitude = driver_detail.optDouble("lat");
                //driverDetail.longitude = driver_detail.optDouble("long");

                /*
                if(driver_detail.has("company")){
                    Util.log(TAG,"deserializando company "+driver_detail.optString("company")
                    );
                    driverDetail.company = gson.fromJson(driver_detail.optString("company"),Company.class);
                }
                */

                if (driver_detail.has("country")) {
                    CountryData countryData = new CountryData(Parcel.obtain());
                    JSONObject country = driver_detail.optJSONObject("country");
                    countryData.id = country.optInt("id");
                    countryData.title = country.optString("title");
                    countryData.telephone_code = country.optString("telephone_code");
                    countryData.flag = country.optString("flag");
                    countryData.country_code = country.optString("country_code");
                    countryData.currency_symbol = country.optString("currency_symbol");
                    if (country.has("car_type")) {
                        JSONArray car_type = country.optJSONArray("car_type");
                        int[] carTypes = new int[car_type.length()];
                        for (int j = 0; j < car_type.length(); j++) {
                            carTypes[j] = (int) car_type.opt(j);
                            countryData.arr_car_type = carTypes;
                        }
                    }
                    driverDetail.countryData = countryData;
                }
                if (driver_detail.has("driver")) {
                    JSONObject driver = driver_detail.optJSONObject("driver");
                    Vehicle vehicle = new Vehicle(Parcel.obtain());
                    vehicle.id = Integer.valueOf(driver.optString("vehicle"));
                    vehicle.model = driver.optString("model");
                    vehicle.car_make = driver.optString("car_make");
                    vehicle.license_no = driver.optString("license_no");
                    vehicle.registration_no = driver.optString("registration_no");
                    rideDetails.vehicle = vehicle;
                }
                if (driver_detail.has("imageProof")) {
                    driverDetail.vehicle_image = driver_detail.optJSONObject("imageProof").optString("vehicle_image");
                }

                rideDetails.driverData = driverDetail;
            }
            store.setInt(Const.STATE_ID, rideDetails.state_id);
            return rideDetails;
        }
        return null;
    }

    public void cancelNotification(int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) this.getSystemService(ns);
        if (notifyId == Const.ALL_NOTI)
            nMgr.cancelAll();
        else
            nMgr.cancel(notifyId);
    }

    public interface PermCallback {
        void permGranted(int resultCode);

        void permDenied(int resultCode);
    }

    public class NetworksBroadcast extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = NetworkUtil.getConnectivityStatusString(context);
            if (status == null && networkAlertDialog != null) {
                networkStatus = null;
                networkAlertDialog.dismiss();
                allParent();
            } else if (status != null && networkStatus == null)
                showNoNetworkDialog(status);
        }
    }

    protected void allParent() {
    }

    class MyRunnable implements Runnable {
        Interpolator interpolator;
        Marker oldMarker;
        LatLngInterpolator latLngInterpolator;
        LatLng startPosition;
        LatLng finalPosition;
        boolean isCameraAnimate;
        GoogleMap googleMap;
        Handler handler;
        float durationInMs;
        long start;

        public MyRunnable(long start, float durationInMs, Interpolator interpolator, Marker oldMarker, LatLngInterpolator latLngInterpolator, LatLng startPosition, LatLng finalPosition, boolean isCameraAnimate, GoogleMap googleMap, Handler handler) {
            this.start = start;
            this.durationInMs = durationInMs;
            this.interpolator = interpolator;
            this.oldMarker = oldMarker;
            this.latLngInterpolator = latLngInterpolator;
            this.startPosition = startPosition;
            this.finalPosition = finalPosition;
            this.isCameraAnimate = isCameraAnimate;
            this.googleMap = googleMap;
            this.handler = handler;
        }

        @Override
        public void run() {

        }

    }

}

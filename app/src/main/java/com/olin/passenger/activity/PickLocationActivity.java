package com.olin.passenger.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.olin.passenger.BuildConfig;
import com.olin.passenger.R;
import com.olin.passenger.adapter.AutoCompleteAdapter;
import com.olin.passenger.data.PlaceAutoComplete;
import com.olin.passenger.data.PlacePredictions;
import com.olin.passenger.utils.Const;
import com.olin.passenger.utils.GoogleApisHandle;
import com.olin.passenger.utils.PrefStore;
import com.olin.passenger.utils.VolleyJSONRequest;
import com.olin.passenger.utils.VolleySingleton;
import com.toxsl.volley.Request;
import com.toxsl.volley.Response;
import com.toxsl.volley.VolleyError;
import com.toxsl.volley.toolbox.RequestParams;
import com.toxsl.volley.toolbox.SyncEventListner;
import com.toxsl.volley.toolbox.SyncManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;


public class PickLocationActivity extends AppCompatActivity implements Response.Listener<String>, Response.ErrorListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    double latitude;
    double longitude;
    private ListView mAutoCompleteList;
    private EditText Address;
    private String GETPLACESHIT = "places_hit";
    private PlacePredictions predictions;
    private Location mLastLocation;
    private AutoCompleteAdapter mAutoCompleteAdapter;
    private int CUSTOM_AUTOCOMPLETE_REQUEST_CODE = 20;
    private static final int MY_PERMISSIONS_REQUEST_LOC = 30;
    private ImageView searchBtn;
    private FragmentManager fragmentManager;
    private String preFilledText;
    private Handler handler;
    private VolleyJSONRequest request;
    private GoogleApiClient mGoogleApiClient;
    private static VolleySingleton volleyQueueInstance;
    private LinearLayout recentLocationLL;
    private TextView toolbar_titleTV;
    private Toolbar toolbar;
    private TextView currentLocationTV;
    private TextView errorMessageTV;
    private ImageView backArrow;
    private TextView addWorkTV;
    private TextView addHomeTV;
    private ImageView editLocationWorkIV;
    private ImageView editLocationHomeIV;
    private TextView workTV;
    private TextView homeTV;
    private SyncManager syncManager;
    private PrefStore store;
    private LinearLayout homeLL;
    private LinearLayout workLL;
    private InputMethodManager inputMethodManager;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        syncManager = SyncManager.getInstance(this, BuildConfig.DEBUG);
        store = new PrefStore(this);
        syncManager.setBaseUrl(Const.SERVER_REMOTE_URL, getString(R.string.app_name), store.getString("language", "en"));
        instantiateVolleyQueue();
        toolbar_titleTV = (TextView) findViewById(R.id.toolbar_titleTV);
        if (getIntent().hasExtra("Search Text")) {
            preFilledText = getIntent().getStringExtra("Search Text");
        }
        inputMethodManager = (InputMethodManager) this
                .getSystemService(BaseActivity.INPUT_METHOD_SERVICE);
        toolbar_titleTV.setText(getIntent().getStringExtra("title"));
        backArrow = (ImageView) findViewById(R.id.backArrow);
        editLocationWorkIV = (ImageView) findViewById(R.id.editLocationWorkIV);
        editLocationHomeIV = (ImageView) findViewById(R.id.editLocationHomeIV);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        fragmentManager = getSupportFragmentManager();
        Address = (EditText) findViewById(R.id.adressText);
        mAutoCompleteList = (ListView) findViewById(R.id.searchResultLV);
        searchBtn = (ImageView) findViewById(R.id.search);
        workLL = (LinearLayout) findViewById(R.id.workLL);
        homeLL = (LinearLayout) findViewById(R.id.homeLL);

        workLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!addWorkTV.getText().toString().isEmpty()) {
                    Intent intent = new Intent();
                    intent.putExtra("Location Address", addWorkTV.getText().toString());
                    setResult(CUSTOM_AUTOCOMPLETE_REQUEST_CODE, intent);
                    finish();
                } else {
                    Intent i = new Intent(getApplicationContext(), PickHomeAndWorkAddressActvity.class);
                    i.putExtra("title", "Work");
                    startActivityForResult(i, Const.ADD_WORK);
                }
            }
        });

        homeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!addHomeTV.getText().toString().isEmpty()) {
                    Intent intent = new Intent();
                    intent.putExtra("Location Address", addHomeTV.getText().toString());
                    setResult(CUSTOM_AUTOCOMPLETE_REQUEST_CODE, intent);
                    finish();
                } else {
                    Intent i = new Intent(getApplicationContext(), PickHomeAndWorkAddressActvity.class);
                    i.putExtra("title", "Home");
                    startActivityForResult(i, Const.ADD_HOME);
                }
            }
        });
        addWorkTV = (TextView) findViewById(R.id.addWorkTV);
        addHomeTV = (TextView) findViewById(R.id.addHomeTV);

        workTV = (TextView) findViewById(R.id.workTV);
        homeTV = (TextView) findViewById(R.id.homeTV);
        recentLocationLL = (LinearLayout) findViewById(R.id.recentLocationLL);
        currentLocationTV = (TextView) findViewById(R.id.currentLocationTV);

        editLocationWorkIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!addWorkTV.getText().toString().isEmpty()) {
                    Intent i = new Intent(getApplicationContext(), PickHomeAndWorkAddressActvity.class);
                    i.putExtra("title", "Work");

                    startActivityForResult(i, Const.ADD_WORK);
                }
            }
        });

        editLocationHomeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!addHomeTV.getText().toString().isEmpty()) {
                    Intent i = new Intent(getApplicationContext(), PickHomeAndWorkAddressActvity.class);
                    i.putExtra("title", "Home");
                    startActivityForResult(i, Const.ADD_HOME);
                }
            }
        });

        errorMessageTV = (TextView) findViewById(R.id.errorMessageTV);
        errorMessageTV.setText("Retrieving nearby locations");
        currentLocationTV.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                BaseActivity.locationUpdateService.getFusedLocationClient().getLastLocation()
                        .addOnSuccessListener(new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location currentLocation) {
                                if (currentLocation != null) {
                                    latitude = currentLocation.getLatitude();
                                    longitude = currentLocation.getLongitude();
                                }
                                if (latitude != 0.0) {
                                    Intent intent = new Intent();
                                    GoogleApisHandle googleApiHandle = GoogleApisHandle.getInstance(PickLocationActivity.this);
                                    intent.putExtra("Location Address", googleApiHandle.decodeAddressFromLatLng(PickLocationActivity.this, new LatLng(latitude, longitude)));
                                    Bundle bundle = new Bundle();
                                    bundle.putDouble("lat", latitude);
                                    bundle.putDouble("longt", longitude);
                                    intent.putExtra("Latlng", bundle);
                                    setResult(CUSTOM_AUTOCOMPLETE_REQUEST_CODE, intent);
                                    finish();
                                }
                            }
                        });;

            }
        });
        if (!store.getString(Const.HOME_ADDRESS).equals("")) {
            addHomeTV.setVisibility(View.VISIBLE);
            addHomeTV.setText(store.getString(Const.HOME_ADDRESS));
            editLocationHomeIV.setImageResource(R.mipmap.ic_search_edit);
            homeTV.setText("Home");
        }
        if (!store.getString(Const.WORK_ADDRESS).equals("")) {
            addWorkTV.setVisibility(View.VISIBLE);
            editLocationWorkIV.setImageResource(R.mipmap.ic_search_edit);
            addWorkTV.setText(store.getString(Const.WORK_ADDRESS));
            workTV.setText("Work");

        }
        //get permission for Android M
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            fetchLocation();
        } else {

            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOC);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            } else {
                fetchLocation();
            }
        }


        //Add a text change listener to implement autocomplete functionality
        Address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // optimised way is to start searching for laction after user has typed minimum 3 chars
                if (Address.getText().length() > 3) {
                    searchBtn.setVisibility(View.GONE);
                    Runnable run = new Runnable() {


                        @Override
                        public void run() {
                            // cancel all the previous requests in the queue to optimise your network calls during autocomplete search
                            volleyQueueInstance.cancelRequestInQueue(GETPLACESHIT);
                            //build Get url of Place Autocomplete and hit the url to fetch result.
                            request = new VolleyJSONRequest(Request.Method.GET, getPlaceAutoCompleteUrl(Address.getText().toString()), null, null, PickLocationActivity.this, PickLocationActivity.this);
                            //Give a tag to your request so that you can use this tag to cancle request later.
                            request.setTag(GETPLACESHIT);
                            volleyQueueInstance.addToRequestQueue(request);
                        }

                    };


                    if (handler != null) {
                        handler.removeCallbacksAndMessages(null);
                    } else {
                        handler = new Handler();
                    }
                    handler.postDelayed(run, 1000);

                } else if (Address.getText().length() == 0 && latitude != 0.0) {
                    volleyQueueInstance.cancelRequestInQueue(GETPLACESHIT);
                    request = new VolleyJSONRequest(Request.Method.GET, getPlaceAutoCompleteUrl(""), null, null, PickLocationActivity.this, PickLocationActivity.this);
                    //Give a tag to your request so that you can use this tag to cancle request later.
                    request.setTag(GETPLACESHIT);
                    volleyQueueInstance.addToRequestQueue(request);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

        Address.setText(preFilledText);
        Address.setSelection(Address.getText().length());

        mAutoCompleteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // pass the result to the calling activity
                PlaceAutoComplete placeAutocomplete = (PlaceAutoComplete) parent.getItemAtPosition(position);
                Intent intent = new Intent();
                intent.putExtra("Location Address", placeAutocomplete.getPlaceDesc());
                intent.putExtra("PlaceId", placeAutocomplete.getPlaceID());
                intent.putExtra("lat", placeAutocomplete.getLat());
                intent.putExtra("lng", placeAutocomplete.getLng());
                setResult(CUSTOM_AUTOCOMPLETE_REQUEST_CODE, intent);
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        hideSoftKeyboard();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        hideSoftKeyboard();
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == Const.ADD_WORK) {
                if (data != null) {
                    String address = data.getStringExtra("Location Address");
                    addWorkTV.setVisibility(View.VISIBLE);
                    editLocationWorkIV.setImageResource(R.mipmap.ic_search_edit);
                    workTV.setText("Work");
                    addWorkTV.setText(address);
                    setAddressOnServer(1, address);
                }

            } else if (requestCode == Const.ADD_HOME) {
                if (data != null) {
                    String address = data.getStringExtra("Location Address");
                    addHomeTV.setText(address);
                    editLocationHomeIV.setImageResource(R.mipmap.ic_search_edit);
                    addHomeTV.setVisibility(View.VISIBLE);
                    homeTV.setText("Home");
                    setAddressOnServer(0, address);
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void setAddressOnServer(int typeId, String address) {
        JSONArray jsonArray = null;
        GoogleApisHandle googleApiHandle = GoogleApisHandle.getInstance(PickLocationActivity.this);

        RequestParams params = new RequestParams();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type_id", typeId);
            jsonObject.put("title", address);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        jsonArray = new JSONArray();
        jsonArray.put(jsonObject);

        params.put("User[address]", jsonArray);

        syncManager.sendToServer("api/user/passenger-address", params, new SyncEventListner() {
            @Override
            public void onSyncStart() {

            }

            @Override
            public void onSyncFinish() {

            }

            @Override
            public void onSyncFailure(VolleyError code, Request mRequest) {

            }

            @Override
            public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
                try {
                    JSONObject object = jsonObject.getJSONObject("detail").getJSONObject("addresses");
                    if (object.has("Home")) {
                        store.saveString(Const.HOME_ADDRESS, object.getJSONObject("Home").getString("title"));

                    }
                    if (object.has("Work")) {

                        store.saveString(Const.WORK_ADDRESS, object.getJSONObject("Work").getString("title"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public boolean hideSoftKeyboard() {
        try {
            if (getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void instantiateVolleyQueue() {
        volleyQueueInstance = VolleySingleton.getInstance(getApplicationContext());
    }


    public String getPlaceAutoCompleteUrl(String input) {
        StringBuilder urlString = new StringBuilder();
        if (input.equals("")) {
            recentLocationLL.setVisibility(View.VISIBLE);
            urlString.append("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        } else {
            recentLocationLL.setVisibility(View.GONE);
            urlString.append("https://maps.googleapis.com/maps/api/place/textsearch/json");
            urlString.append("?input=");

            try {
                urlString.append(URLEncoder.encode(input, "utf8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        urlString.append("&types=address");
        urlString.append("&location=");
        urlString.append(latitude + "," + longitude); // append lat long of current location to show nearby results.
        urlString.append("&radius=1000&language=en");
        urlString.append("&key=" + "AIzaSyD6lstTiUL7ef-ZSbxb9ir2_q8ky6bVyis");

        Log.d("FINAL URL:::   ", urlString.toString());
        return urlString.toString();
    }

    @Override
    public void onErrorResponse(VolleyError error, Request request) {
        Log.d("---", "");
    }

    ArrayList<PlaceAutoComplete> placeAutocompletes;

    @Override
    public void onResponse(String response) {
        Log.d("PLACES RESULT:::", response);
        placeAutocompletes = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(response);
            if (object.has("error_message")) {
                errorMessageTV.setVisibility(View.VISIBLE);
                errorMessageTV.setText(object.optString("error_message"));
                mAutoCompleteList.setVisibility(View.GONE);
            } else if (object.has("results")) {
                errorMessageTV.setVisibility(View.GONE);
                mAutoCompleteList.setVisibility(View.VISIBLE);
                JSONArray jsonArray = object.getJSONArray("results");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object1 = jsonArray.getJSONObject(i);
                    PlaceAutoComplete placeAutoComplete = new PlaceAutoComplete();
                    if (object1.has("vicinity")) {
                        if (object1.getString("name").equalsIgnoreCase(object1.getString("vicinity")))
                            placeAutoComplete.setPlaceDesc(object1.getString("name"));
                        else
                            placeAutoComplete.setPlaceDesc(object1.getString("name") + "," + object1.getString("vicinity"));
                    } else {

                        placeAutoComplete.setPlaceDesc(object1.getString("formatted_address"));

                    }
                    JSONObject object2 = object1.getJSONObject("geometry").getJSONObject("location");
                    placeAutoComplete.setLat(object2.getString("lat"));
                    placeAutoComplete.setLng(object2.getString("lng"));
                    placeAutoComplete.setPlaceID(object1.getString("place_id"));
                    placeAutoComplete.setPlaceIcon(object1.getString("icon"));

                    //placeAutoComplete.setLatLng(object1.get("icon"));
                    placeAutocompletes.add(placeAutoComplete);
                }
            } else {
                JSONArray jsonArray = object.getJSONArray("predictions");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object1 = jsonArray.getJSONObject(i);
                    PlaceAutoComplete placeAutoComplete = new PlaceAutoComplete();
                    placeAutoComplete.setPlaceDesc(object1.getString("description"));
                    placeAutoComplete.setPlaceID(object1.getString("id"));
                    placeAutoComplete.setPlaceIcon("");
                    placeAutocompletes.add(placeAutoComplete);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mAutoCompleteAdapter = null;
        mAutoCompleteAdapter = new AutoCompleteAdapter(this, placeAutocompletes, PickLocationActivity.this);
        mAutoCompleteList.setAdapter(mAutoCompleteAdapter);
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        try {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            if (mLastLocation != null) {
                latitude = mLastLocation.getLatitude();
                longitude = mLastLocation.getLongitude();
            }
            volleyQueueInstance.cancelRequestInQueue(GETPLACESHIT);
            request = new VolleyJSONRequest(Request.Method.GET, getPlaceAutoCompleteUrl(""), null, null, PickLocationActivity.this, PickLocationActivity.this);

            //Give a tag to your request so that you can use this tag to cancle request later.
            request.setTag(GETPLACESHIT);
            volleyQueueInstance.addToRequestQueue(request);

        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void fetchLocation() {
        //Build google API client to use fused location
        buildGoogleApiClient();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOC: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission granted!
                    LocalBroadcastManager manager=LocalBroadcastManager.getInstance(this);
                    manager.sendBroadcast(new Intent("Perm Granted"));
                    fetchLocation();

                } else {
                    // permission denied!

                    Toast.makeText(this, "Please grant permission for using this app!", Toast.LENGTH_LONG).show();
                }
            }


        }
    }
}


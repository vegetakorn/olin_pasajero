package com.olin.passenger.utils;

public class Const {

    //public static final String SERVER_REMOTE_URL = "http://192.168.53.10:8080/freelance/ludwins/taxiwepp/app/";
    //public static final String SERVER_REMOTE_URL = "http://192.168.53.13:8080/freelance/ludwins/taxiwepp/app/";
    //public static final String SERVER_REMOTE_URL = "http://172.16.60.30:8080/freelance/ludwins/taxibloque/app/";
    public static final String SERVER_REMOTE_URL = "http://74.91.126.73:8080/OLIN/";

    public static final String DISPLAY_MESSAGE_ACTION = "com.olin.passenger.DISPLAY_MESSAGE";
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1234;
    public static final String FCM_TOKEN_REFRESH = "com.olin.passenger.FCM_TOKEN_REFRESH";
    /**
     * ******************************* App buy or Purchase Key *************************************
     */
    public static final String IN_APP_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiwmkXRr/Ui6Onc3hZl8gvxe2D9DIyAELLBMryE2+iz1X6ToPAl2D+Lypytja+xc5u1s5yyfX5PzsruerkUfwm6nMjXwX2v64+syV3QjC1yQ3HuMCL4IJgu+jSymj8PQ+tBK9i8ehWm4NvYZ8KG6uCXnR2xqWQKgJEm0E96aMl7jwb7BvAvfn62/Frqlxw9n3V1f11CbgbQUbY8rOjpZvTZ+LF+Ly5NdFAM+H8BafdF7WQ0N3mmQfqmEBLamiqS/+5F5nGjSYrn2cjjBCbfDlQMxUS0fSjACQm+Ri6r5Qzi+cYyYt9Zn/i/U6Aimj/9qyoDwTV3TTMm2tv7lVn7VLnQIDAQAB";
    public static final int GALLERY_REQ = 1;
    public static final int CAMERA_REQ = 2;

    public static final int CAR = 0;
    public static final int SUV = 1;
    public static final int MINIVAN = 2;
    public static final int ROLE_PASSENGER = 1;


    /**
     * Journry States------------------------------------Start
     */
    public static final int RIDE_NOW = 1;
    public static final int RIDE_LATER = 2;
    public static final int NOT_HOURLY = 0;
    public static final int IS_HOURLY = 1;
    public static final int STATUS_OK = 200;
    public static final int STATUS_NOK = 1000;

    public static final int STATE_NEW = 1;
    public static final int STATE_COUPON = 0;
    public static final int STATE_ACCEPTED = 2;
    public static final int STATE_REJECTED = 3;
    public static final int STATE_CANCELLED = 4;
    public static final int STATE_DRIVER_ARRIVED = 5;
    public static final int STATE_STARTED = 6;
    public static final int STATE_COMPLETED = 7;
    public static final int STATE_PAID = 8;
    /**
     * Journry States---------------------------------------End
     */
    public static final int USER_IMAGE = 3;
    public static final int PICKUP = 4;
    public static final int DROPOFF = 5;
    public static final int ADD_WORK = 6;
    public static final int ADD_HOME = 7;

    public static final int ABOUT_PAGE = 0;
    public static final int TC_CUSTOMER_PAGE = 1;
    public static final int TC_DRIVER_PAGE = 2;

    public static final String HOME_ADDRESS = "home_address";
    public static final String WORK_ADDRESS = "work_address";
    public static final String API_CAR_TYPE = "api/ride/car-type";
    public static final String API_USER_LOGIN = "api/user/login";
    public static final String API_USER_PASSENGER_SIGNUP = "api/user/passenger-signup";
    public static final String API_RIDE_REQUEST = "api/ride/request";
    public static final String API_RIDE_DETAIL = "api/ride/detail";
    public static final String API_RIDE_CASH_PAY = "api/ride/cash-pay";
    public static final String API_USER_WALLET_ADD = "api/user-wallet/add";
    public static final String API_RIDE_WALLET_PAY = "api/ride/wallet-pay";
    public static final String API_USER_WALLET_GET = "api/user-wallet/get";
    public static final String API_RIDE_PAYPAL_PAY = "api/ride/paypal-pay";
    public static final String API_REVIEW_ADD = "api/review/add";
    public static final String API_REVIEW_GET = "api/review/get";
    public static final String API_COMPLAIN_ADD = "api/complain/add";//?ride_id=537";
    public static final String API_PAGE_GET = "api/page/get";//?type_id=
    public static final String API_RIDE_UPDATE = "api/ride/update";//?id=
    public static final String API_USER_CHANGE_PASSWORD = "api/user/change-password";
    public static final String API_RIDE_USER_RIDE_HISTORY = "api/ride/user-ride-history";
    public static final String API_SEARCH_ON_SIGNUP = "api/user/address-search";

    public static final String DRIVER_STATE = "driver-state";
    public static final int DRIVER_AVAILABLE = 1;
    public static final String JOURNEY_ID = "ride-id";
    public static final String STATE_ID = "state-id";
    public static final String API_RIDE_CANCELED = "api/ride/canceled?id=";
    public static final int ALL_NOTI = 1002;
    public static final String MY_ID = "my_id";
    public static final String API_FACEBOOK = "api/user/facebook-login";
    public static final String API_USER_COUNTRY = "api/user/country?id=";

    //Coupons api//

    public static final String API_COUPONS_LIST = "api/coupon/index";
    public static final String COUPON_Id = "coupon_id";
    public static final String API_APPLY_COUPON = "api/coupon/apply";

    //Lista las empresas
    public static final String API_COMPANY_INDEX = "api/company/index";
}
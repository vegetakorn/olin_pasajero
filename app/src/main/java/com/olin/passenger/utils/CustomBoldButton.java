package com.olin.passenger.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by TOXSL\ankush.walia on 30/1/17.
 */

public class CustomBoldButton extends Button {

    Context context;
    int defStyleAttr;
    private AttributeSet attrs;
    String font;

    public CustomBoldButton(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public CustomBoldButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.attrs = attrs;
        init();
    }

    public CustomBoldButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.attrs = attrs;
        this.defStyleAttr = defStyleAttr;
        init();
    }

    @Override
    public void setTypeface(Typeface tf, int style) {
        tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/lato-bold.ttf");
        super.setTypeface(tf, style);
    }


    private void init() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/lato-bold.ttf");
        this.setTypeface(font);
    }

    @Override
    public void setTypeface(Typeface tf) {
        tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/lato-bold.ttf");
        super.setTypeface(tf);
    }
}

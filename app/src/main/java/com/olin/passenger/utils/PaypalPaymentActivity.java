package com.olin.passenger.utils;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.olin.passenger.activity.BaseActivity;
import com.olin.passenger.data.PayPalData;
import com.toxsl.volley.toolbox.RequestParams;


import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;


/**
 * Created by TOXSL\chirag.tyagi on 9/3/16.
 */
public class PaypalPaymentActivity extends BaseActivity {
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    private static final String LIVE_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
    private static final String CONFIG_CLIENT_ID = "ARCT6OWtKzxJgrE05LBI7GOAPumILhAtxSEbibjFMgPPrI8AYNcVfQqVPbQ9uNr5xwG6m2htYhikIO_N";
    private static final String SECRET_KEY = "EKMHzQOwJsaxa1wSXfmy5gmfs8PxN3Pe2q9kMxc7JHE4PDYQMxzKgO0snbLc34jIm7geRMwuK4noLbNP";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(LIVE_ENVIRONMENT).clientId(CONFIG_CLIENT_ID);
    private double totalFare;
    private int booking_id;
    private static PaypalPaymentListener paypalPaymentListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
        if (getIntent() != null) {
            totalFare = getIntent().getDoubleExtra("totalFare", 0.0);
            booking_id = getIntent().getIntExtra("id", 0);

        }

        onBuyPressed();

    }

    public static void setPaypalPaymentListener(PaypalPaymentListener paypalPaymentListener1) {
        paypalPaymentListener = paypalPaymentListener1;
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    public void onBuyPressed() {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_AUTHORIZE);
        Intent intent = new Intent(PaypalPaymentActivity.this,
                PaymentActivity.class);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);


    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal(totalFare), "MXN", "Pay For super power",
                paymentIntent);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == RESULT_OK) {
                PaymentConfirmation confirm = data
                        .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {

                        PayPalData paypalData = new PayPalData();
                        JSONObject obj = confirm.toJSONObject();
                        JSONObject subject = confirm.getPayment()
                                .toJSONObject();

                        JSONObject obj2 = obj.getJSONObject("response");
                        paypalData.state = obj2.getString("state");
                        paypalData.id = obj2.getString("id");

                        paypalData.createTime = obj2.getString("create_time");

                        paypalData.short_description = subject
                                .getString("short_description");
                        paypalData.amount = subject.getString("amount");
                        paypalData.currencyCode = subject
                                .getString("currency_code");

                        paypalData.responseType = obj
                                .getString("response_type");

                        int index = paypalData.amount.lastIndexOf(".");
                        if (index != -1) {
                            try {
                                paypalData.amount = paypalData.amount.substring(0, index + 3);
                            } catch (StringIndexOutOfBoundsException e) {
                                e.printStackTrace();
                            }
                        }
                        paypalPaymentListener.paymentDone(paypalData);
                        //saveRecordOnServer(paypalData.id, paypalData.amount, paypalData.currencyCode);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == RESULT_CANCELED) {
                finish();
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            }
            finish();
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == RESULT_OK) {
                PayPalAuthorization auth = data
                        .getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject()
                                .toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        Toast.makeText(getApplicationContext(),
                                "Future Payment code received from PayPal",
                                Toast.LENGTH_LONG).show();

                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample",
                                "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == RESULT_CANCELED) {
                finish();
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs");
            }
        }
    }

    private void saveRecordOnServer(String id, String amount, String currencyCode) {
        // startActivity(new Intent(this, MainActivity.class));

        if (booking_id != 0) {
            String ridePayment = "api/booking/confirm-booking?id=" + booking_id;
            RequestParams params = new RequestParams();
            params.put("UserWallet[transaction-id]", id);
            params.put("UserWallet[amount]", amount);
            syncManager.sendToServer(ridePayment, params, this);
        } else {
            String addWallet = Const.API_USER_WALLET_ADD;
            RequestParams params = new RequestParams();
            params.put("UserWallet[transaction-id]", id);
            params.put("UserWallet[transaction-amount]", amount);
            syncManager.sendToServer(addWallet, params, this);
        }
        finish();

    }



    private void sendAuthorizationToServer(PayPalAuthorization authorization) {


    }

    public void onFuturePaymentPurchasePressed(View pressed) {
        String correlationId = PayPalConfiguration
                .getApplicationCorrelationId(this);

        Log.i("FuturePaymentExample", "Application Correlation ID: "
                + correlationId);

        Toast.makeText(getApplicationContext(),
                "App Correlation ID received from SDK", Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public interface PaypalPaymentListener {
        void paymentDone(PayPalData payPalData);
    }


}

/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olin.passenger.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.olin.passenger.R;
import com.olin.passenger.activity.MainActivity;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMessaging";
    public void displayMessage(Context context, Bundle extras) {
        Intent intent = new Intent(Const.DISPLAY_MESSAGE_ACTION);
        intent.putExtra("map", extras);
        context.sendBroadcast(intent);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e("bundle Data", "" + remoteMessage);

        Map data = remoteMessage.getData();
        Log.e("bundle Data", "" + data);
        String from = remoteMessage.getFrom();
        Log.d(TAG, "From: " + from);
        Bundle bundle = new Bundle();
        bundle.putString("action", data.get("action").toString());
        bundle.putString("controller", data.get("controller").toString());
        bundle.putString("message", data.get("message").toString());
        if (data.containsKey("ride_id"))
            bundle.putString("id", data.get("ride_id").toString());
        generateNotification(this, bundle);

        generateNotification(this, bundle);
        displayMessage(this, bundle);
    }

    public void generateNotification(Context context, Bundle extra) {
        String notificationMessage = (String) extra.get("message");
        int noti_id = 0;
        try {

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "tsd_02";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications",NotificationManager.IMPORTANCE_HIGH);
            // Configure the notification channel.
            notificationChannel.setDescription("Notificaciones");
            notificationManager.createNotificationChannel(notificationChannel);
        }

        Bitmap largeIcon = BitmapFactory.decodeResource(this.getResources(), R.mipmap.notification_1);
        NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(context,NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.mipmap.notification_1)
                .setLargeIcon(largeIcon)
                .setContentTitle(this.getResources().getString(R.string.app_name))
                .setContentText(notificationMessage).setAutoCancel(true);
        Uri NotiSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(NotiSound);
        long[] vibrate = {0, 100, 200, 300};
        mBuilder.setVibrate(vibrate);
        Intent resultIntent = new Intent(context, MainActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        resultIntent.putExtra("map", extra);
        resultIntent.putExtra("isPush", true);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        {
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(noti_id, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(noti_id, mBuilder.build());
        }

    }

    // [START on_new_token]

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    public void onNewToken(String token) {
        super.onNewToken(token);
        Util.log(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        //sendRegistrationToServer(token);

        Intent intent = new Intent(Const.FCM_TOKEN_REFRESH);
        intent.putExtra(Const.FCM_TOKEN_REFRESH, token);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
    // [END on_new_token]

}

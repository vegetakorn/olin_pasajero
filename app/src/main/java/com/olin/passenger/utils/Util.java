package com.olin.passenger.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;

import androidx.annotation.ColorInt;

import com.olin.passenger.BuildConfig;

public class Util {
    private static final String TAG = "Util";

    /**
     * Metodo para hacer debug de la app
     * @param tag
     * @param message
     */
    public static  void log(String tag,String message){
        if (BuildConfig.DEBUG){
            Log.e(tag,message);
        }
    }

    public static boolean isDebug(){
        return BuildConfig.DEBUG;
    }

    public static int dpToPx(int dp, Activity a) {
        DisplayMetrics displayMetrics = a.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    @ColorInt public static int getColor(int id, Context context){
        int color;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            color = context.getResources().getColor(id,context.getTheme());
        }else{
            color = context.getResources().getColor(id);
        }
        return color;
    }

    /**
     * Calcula el numero que le sigue en el multiplo de 5
     * @param num
     * @return
     */
    public static double calculateNextIntValue(double num){
        int multi = 5;
        int div = (int) Math.floor(num / multi);
        int add = 0;
        double mod = num % multi;
        if(mod > 0){
            add = multi;
        }else{
            add = 0;
        }
        double appxPrice = (div * multi) + add ;
        //Util.log (TAG,"Numero original: "+num+" siguiente: "+appxPrice);
        return appxPrice;
    }
}

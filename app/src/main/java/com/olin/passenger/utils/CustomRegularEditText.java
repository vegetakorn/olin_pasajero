package com.olin.passenger.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

/**
 * Created by visha.sehgal on 26/5/16.
 */
public class CustomRegularEditText extends AppCompatEditText {
    Context context;
    int defStyleAttr;
    private AttributeSet attrs;
    String font;

    public CustomRegularEditText(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public CustomRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.attrs = attrs;
        init();
    }

    public CustomRegularEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.attrs = attrs;
        this.defStyleAttr = defStyleAttr;
        init();
    }

    @Override
    public void setTypeface(Typeface tf, int style) {
        tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/lato-regular.ttf");
        super.setTypeface(tf, style);
    }


    private void init() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/lato-regular.ttf");
        this.setTypeface(font);
    }

    @Override
    public void setTypeface(Typeface tf) {
        tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/lato-regular.ttf");
        super.setTypeface(tf);
    }
}

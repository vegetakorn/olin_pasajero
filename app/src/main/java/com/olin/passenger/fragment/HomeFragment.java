package com.olin.passenger.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.olin.passenger.R;
import com.olin.passenger.activity.BaseActivity;
import com.olin.passenger.activity.MainActivity;
import com.olin.passenger.activity.PickLocationActivity;
import com.olin.passenger.adapter.CarPagerAdapter;
import com.olin.passenger.data.Company;
import com.olin.passenger.data.DriverData;
import com.olin.passenger.data.JourneyData;
import com.olin.passenger.data.VehicleData;
import com.olin.passenger.service.LocationUpdateService;
import com.olin.passenger.utils.Const;
import com.olin.passenger.utils.GoogleApisHandle;
import com.olin.passenger.utils.Util;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static android.app.Activity.RESULT_CANCELED;

/**
 * Created by TOXSL\ankush.walia on 23/1/17.
 */

public class HomeFragment extends BaseFragment implements OnMapReadyCallback, BaseActivity.PermCallback, GoogleApisHandle.OnPolyLineReceived, LocationUpdateService.OnLocationReceived, LocationListener {
    private String TAG = "HomeFragment";
    private static final int SETUP_MAP_LOCATION_PERM_REQ = 420;
    ArrayList<VehicleData> vehicleList = new ArrayList<>();
    private View view;
    private GoogleMap googleMap;
    private AlertDialog.Builder alert;
    private LatLng pickupPlace;
    private LatLng dropoffPlace;
    private Marker sourceMarker;
    private ImageView toolbar_imageIV, nextIV, backIV;
    private Location currentLocation;
    private HashMap<Integer, DriverData> driversMarkerHashMap = new HashMap<>();
    private Timer timer;
    private Handler handler = new Handler();
    private ArrayList<DriverData> driverData = new ArrayList();
    private String[] name = {"Car", "SUV", "MiniVan"};
    private int[] pics = {R.mipmap.ic_car_big, R.mipmap.ic_suv_big, R.mipmap.ic_minivan_big};
    private TextView pickUpLocationTV;
    private TextView dropOffTV, appxTV;
    private RelativeLayout dropOffRL, pickUpRL;
    private RecyclerView pagerVP;
    private int pagerPos = -1;
    private LinearLayout rideDetailLL,llCompanies;
    private Dialog dialog;
    private FrameLayout currentPinFL;
    private Marker pickupMarker;
    private Marker destinationMarker;
    private String[] arrPassanger;
    private CarPagerAdapter pagerAdapter;
    private CarPagerAdapter.OnClickListener onClickListener;
    private String dropOffAddress;
    private String pickupAddress;
    private RelativeLayout timeRL;
    private Double lat, lng, droplat, droplng;
    private TextView dateTV;
    private String noOfHours;
    private boolean isRideLater;
    private Calendar changeTime, mcurrentTime;
    private Integer hour;
    private Integer minute;
    private int carType = 0;
    private double dist = 0.0;

    private EditText etCompany;
    private String[] companiesArr;
    private Company[] companies;
    private Map<Integer, Company> companiesSelected = new HashMap<Integer, Company>();

    private LocalBroadcastManager manager;
    private PermReceiver permReceiver;
    private Date selectedDate;
    private boolean isGetDriver;
    Runnable task = new Runnable() {
        @SuppressLint("MissingPermission")
        @Override
        public void run() {
            locationUpdateService.getFusedLocationClient().getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            currentLocation = location;
                            if (currentLocation != null){
                                getDriver(carType, currentLocation,true);
                            }
                        }
                    });
        }
    };
    private Polyline polyline;
    private boolean isFirstTimePick;
    private String address, estimate_time;
    private boolean isFirstDatePick;
    private Bundle bundle;
    private int count = 0;
    private String discountedPrice;
    private double appxPrice;
    private TextView promoTV,bookNowTV,bookLaterTV;
    private double max_amount;
    private boolean singlehit;
    private double base_time;
    private SupportMapFragment map;
    private BigDecimal amount;
    private boolean isInitPosition = false;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_home_passenger, container, false);
        }
        ((MainActivity) baseActivity).setActionBarTitle("", false);
        if (googleMap == null)
            initUI();
        if (locationUpdateService != null)
            locationUpdateService.setLocationReceivedListner(this);

        baseActivity.syncManager.sendToServer(Const.API_COMPANY_INDEX+"?Company[enabled]=1&Company[country_id]="+baseActivity.getProfileDataFromPrefStore().country.id, null, this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (googleMap != null) {
            map = ((SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.mapF));
            map.onResume();
        }

        if (currentLocation != null) {
            startTimer();
            invalidateMarkers();
        }

    }

    private void initUI() {
        toolbar_imageIV = (ImageView) view.findViewById(R.id.toolbar_imageIV);
        backIV = (ImageView) view.findViewById(R.id.backIV);
        etCompany = (EditText) view.findViewById(R.id.etCompany);
        nextIV = (ImageView) view.findViewById(R.id.nextIV);
        pickUpLocationTV = (TextView) view.findViewById(R.id.pickUpLocTV);
        bookNowTV = (TextView) view.findViewById(R.id.bookNowTV);
        bookLaterTV = (TextView) view.findViewById(R.id.bookLaterTV);
        dropOffTV = (TextView) view.findViewById(R.id.dropOffTV);
        dateTV = (TextView) view.findViewById(R.id.dateTV);
        appxTV = (TextView) view.findViewById(R.id.appxTV);
        promoTV = (TextView) view.findViewById(R.id.promoTV);
        dropOffRL = (RelativeLayout) view.findViewById(R.id.dropOffRL);
        timeRL = (RelativeLayout) view.findViewById(R.id.timeRL);
        pickUpRL = (RelativeLayout) view.findViewById(R.id.pickUpRL);
        rideDetailLL = (LinearLayout) view.findViewById(R.id.rideDetailLL);
        llCompanies = (LinearLayout) view.findViewById(R.id.llCompanies);
        currentPinFL = (FrameLayout) view.findViewById(R.id.currentPinFL);

        dropOffRL.setVisibility(View.GONE);
        pickUpLocationTV.setSelected(true);
        dropOffTV.setSelected(true);
        dropOffAddress = getString(R.string.drop_off_location);
        pickupAddress = getString(R.string.pick_up_location);
        rideDetailLL.setVisibility(View.GONE);
        llCompanies.setVisibility(View.GONE);

        pagerVP = (RecyclerView) view.findViewById(R.id.pagerVP);
        pagerVP.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
        pagerVP.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.HORIZONTAL));
        manager = LocalBroadcastManager.getInstance(getActivity());
        permReceiver = new PermReceiver();
        manager.registerReceiver(permReceiver, new IntentFilter("Perm Granted"));
        onClickListener = new CarPagerAdapter.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(final int position, View var1) {
                pagerPos = position;
                setCarSeatsNo();
                showFare(vehicleList, discountedPrice, estimate_time);
                carType = position;
                locationUpdateService.getFusedLocationClient().getLastLocation()
                        .addOnSuccessListener(new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                currentLocation = location;
                                if (currentLocation != null) {
                                    invalidateMarkers();
                                    getDriver(position, currentLocation,false);
                                } else {
                                    showToast(getString(R.string.couldnt_get_current_location));
                                }
                            }
                        });
            }
        };

        dropOffRL.setOnClickListener(this);
        pickUpRL.setOnClickListener(this);
        timeRL.setOnClickListener(this);
        bookNowTV.setOnClickListener(this);
        bookLaterTV.setOnClickListener(this);
        toolbar_imageIV.setOnClickListener(this);
        nextIV.setOnClickListener(this);
        backIV.setOnClickListener(this);
        etCompany.setOnClickListener(this);

        initilizeMap();


    }

    private void initilizeMap() {
        if (googleMap == null) {
            map = ((SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.mapF));
            map.getMapAsync(this);
        } else{
            setCurrentLocation();
        }
    }

    private void setCurrentLocation() {
        if (baseActivity.checkPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, SETUP_MAP_LOCATION_PERM_REQ, this)) {
            if (ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            View locationControl = null;
            try {
                locationControl = map.getView().findViewById(Integer.parseInt("2"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen._50dp)), Math.round(getResources().getDimension(R.dimen._50dp))); // size of button in dp
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            params.setMargins(0, 0, 20, 100);
            if (locationControl != null) {
                locationControl.setLayoutParams(params);
            }

            locationUpdateService.getFusedLocationClient().getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    currentLocation = location;
                    if (currentLocation != null) {
                        setMyLOcation();
                    } else {
                        showToast(getString(R.string.couldnt_get_current_location));
                    }
                }
            });
        }
    }

    private void setMyLOcation() {
        if(googleMap != null && currentLocation != null){
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 15));


            address = googleApiHandle.decodeAddressFromLatLng(baseActivity, new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
            pickupAddress = address;
            pickupPlace = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            pickUpLocationTV.setText(address);
            Util.log(TAG,"currentLocation: "+currentLocation.getLatitude()+" : "+currentLocation.getLongitude()+" "+address);
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap map) {
        map.getUiSettings().setAllGesturesEnabled(true);
        map.getUiSettings().setTiltGesturesEnabled(false);
        map.getUiSettings().setRotateGesturesEnabled(false);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.getUiSettings().setMapToolbarEnabled(false);
        map.getUiSettings().setCompassEnabled(false);
        map.setMyLocationEnabled(true);
        map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                //Util.log(TAG,"setOnMyLocationButtonClickListener");
                setCurrentLocation();
                return false;
            }
        });
        this.googleMap = map;
        googleApiHandle.setPolyLineReceivedListener(this);
        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLng centreOfMap = googleMap.getCameraPosition().target;

            }
        });
        googleMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {
            }
        });
        //Se buscan los taxis cercanos
        task.run();
        //Se inicia la busqueda de los taxis cercanos
        startTimer();

        setCurrentLocation();
        if(!isInitPosition){
            isInitPosition = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setCurrentLocation();
                }
            }, 2000);
        }

    }

    private void getCarType(Double lat, Double longs) {
        isGetDriver = true;
        RequestParams params = new RequestParams();
        params.add("lat", String.valueOf(lat));
        params.add("long", String.valueOf(longs));
        baseActivity.syncManager.sendToServer(Const.API_CAR_TYPE, params, this);
    }

    private synchronized void getDriver(int type, Location currentLocation,boolean fromTask) {
        isGetDriver = true;
        RequestParams params = new RequestParams();
        params.put("lat", currentLocation.getLatitude());
        params.put("long", currentLocation.getLongitude());

        if(companiesSelected.size() > 0){
            int i = 0;
            Iterator<Integer> itr2 = companiesSelected.keySet().iterator();
            while (itr2.hasNext()) {
                Integer key = itr2.next();
                params.put("companies["+i+"]",companiesSelected.get(key).id);
                i++;
            }
        }
        if(fromTask == false){
            params.put("show_all_message","1");
        }
        baseActivity.syncManager.sendToServer("api/ride/get-drivers?type_id=" + type, params, this);
    }

    @Override
    public void onSyncStart() {
        if (timer == null)
            super.onSyncStart();
        else if (!isGetDriver) {
            super.onSyncStart();
        }
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("ride") && action.equals("get-drivers")) {
            //Limpiar los marcadores antes de seguir
            invalidateMarkers();
            if (status) {
                try {
                    parseDrivers(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else{
                if(pagerAdapter != null){
                    View v = pagerVP.getChildAt(pagerAdapter.getCurrentItem());
                    if(v != null){
                        TextView tvCarTime = v.findViewById(R.id.tvCarTime);
                        tvCarTime.setText("");
                    }
                }
                String error = jsonObject.optString("error");
                if(error != null && !error.isEmpty()){
                    if (view != null) {
                        Snackbar snackbar = Snackbar
                                .make(getActivity().findViewById(android.R.id.content), error, Snackbar.LENGTH_LONG)
                                .setAction(getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        task.run();
                                    }
                                });

                        snackbar.show();
                    }
                }
            }
        } else if (controller.equals("ride") && action.equals("request")) {
            if (status) {

                if (jsonObject.has("detail") && jsonObject.optJSONObject("detail") != null && jsonObject.optJSONObject("detail").optInt("journey_type") == Const.RIDE_NOW) {
                    gotoRideRequestFragment(parseJourneyData(jsonObject));
                } else {
                    resetUI();
                }
            } else {
                if (view != null) {
                    Snackbar snackbar = Snackbar
                            .make(getActivity().findViewById(android.R.id.content), getString(R.string.please_try_again), Snackbar.LENGTH_SHORT)
                            .setAction(getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    addRide();
                                }
                            });

                    snackbar.show();
                }
            }
        } else if (controller.equals("ride") && action.equals("car-type")) {
            if (status) {
                try {
                    vehicleList.clear();
                    pagerPos = 0;
                    JSONArray list = jsonObject.optJSONArray("list");
                    vehicleList = parseVehicleData(list);
                    if (jsonObject.has("max_amount"))
                        max_amount = jsonObject.getDouble("max_amount");
                    getCarTypeAndRetry(vehicleList);
                    if (jsonObject.has("discount")) {
                        discountedPrice = jsonObject.getString("discount");
                        if (vehicleList != null || vehicleList.size() > 0) {
                            showFare(vehicleList, discountedPrice, estimate_time);

                        }
                    } else {
                        if (vehicleList != null || vehicleList.size() > 0) {
                            discountedPrice = "0";
                            promoTV.setVisibility(View.GONE);
                            showFare(vehicleList, discountedPrice, estimate_time);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if (view != null) {
                    Snackbar snackbar = Snackbar
                            .make(getActivity().findViewById(android.R.id.content), getString(R.string.please_try_again), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (currentLocation != null)
                                        getCarType(currentLocation.getLatitude(), currentLocation.getLongitude());
                                }
                            });

                    snackbar.show();
                }
            }
        }else if (controller.equals("company") && action.equals("index") && status) {
            companiesSelected.clear();
            try {
                JSONArray companiesJson = jsonObject.getJSONArray("list");
                if (companiesJson != null) {
                    companies = new Company[companiesJson.length()];
                    companiesArr = new String[companiesJson.length()];
                }
                for (int i = 0; i < companiesJson.length(); i++) {
                    JSONObject companyObj = companiesJson.getJSONObject(i);
                    Company companyData = new Company();
                    companyData.id = companyObj.getInt("id");
                    companyData.name = companyObj.getString("name");
                    companies[i] = companyData;
                    companiesArr[i] = companyData.name;
                }
                if(companies == null || companies.length == 0){
                    llCompanies.setVisibility(View.GONE);
                }else{
                    llCompanies.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }



    private void resetUI() {
        initUI();
        pickUpLocationTV.setText(address != null && !address.isEmpty() ? address : pickupAddress);
        dropOffTV.setText(dropOffAddress);
        currentPinFL.setVisibility(View.VISIBLE);
        timeRL.setVisibility(View.GONE);
        dateTV.setText(getString(R.string.select_time));
        if (googleMap != null && pickupMarker != null && destinationMarker != null && polyline != null) {
            pickupMarker.remove();
            destinationMarker.remove();
            pickupMarker = null;
            destinationMarker = null;
            polyline.remove();
        }
        pickupPlace = null;
        dropoffPlace = null;
        noOfHours = "";
        invalidateMarkers();
    }

    @SuppressLint("MissingPermission")
    private void getCarTypeAndRetry(ArrayList<VehicleData> vehicleDataArrayList) {
        if (vehicleDataArrayList != null && vehicleDataArrayList.size() != 0) {
            setCarSeatsNo();
            pagerAdapter = new CarPagerAdapter(this, baseActivity, name, pics, vehicleDataArrayList,onClickListener);
            pagerVP.setAdapter(pagerAdapter);
            pagerAdapter.notifyDataSetChanged();
        } else {
            if (baseActivity.checkPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 121, baseActivity.permCallback)) {
                if (googleMap != null) {
                    locationUpdateService.getFusedLocationClient().getLastLocation()
                            .addOnSuccessListener(new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    currentLocation = location;
                                    if (currentLocation != null) {
                                        getCarType(currentLocation.getLatitude(), currentLocation.getLongitude());
                                    }
                                }
                            });

                }
            }
        }
    }

    private void gotoRideRequestFragment(JourneyData journeyData) {
        stopTimer();//Parar la busqueda cuando se vaya a otro fragment
        Fragment fragment = new RideRequestFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("journeyData", journeyData);
        fragment.setArguments(bundle);
        baseActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    private void setCarSeatsNo() {
        VehicleData data = vehicleList.get(pagerPos);
        arrPassanger = new String[data.seat];
        for (int i = 0; i < data.seat; i++) {
            arrPassanger[i] = "" + (i + 1);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (view != null)
            manager.unregisterReceiver(permReceiver);
    }

    private void invalidateMarkers() {
        for (int i = 0; i < driverData.size(); i++) {
            if (driversMarkerHashMap.get(driverData.get(i).id) != null && driversMarkerHashMap.get(driverData.get(i).id).driverMarker != null)
                driversMarkerHashMap.get(driverData.get(i).id).driverMarker.remove();
        }
        driversMarkerHashMap.clear();
        driverData.clear();
    }

    private void setDriverOnMap(DriverData data, int car_type) {
        MarkerOptions marker = new MarkerOptions();
        marker.anchor(0.5f, 0.5f);
        if (car_type == Const.CAR)
            marker.position(new LatLng(data.latitude, data.longitude)).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_car))/*.title("Name: " + data.first_name.concat(" " + data.last_name))*/;
        else if (data.car_type == Const.SUV)
            marker.position(new LatLng(data.latitude, data.longitude)).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_suv))/*.title("Name: " + data.first_name.concat(" " + data.last_name))*/;
        else
            marker.position(new LatLng(data.latitude, data.longitude)).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_minivan))/*.title("Name: " + data.first_name.concat(" " + data.last_name))*/;
        data.driverMarker = googleMap.addMarker(marker);

        driversMarkerHashMap.put(data.id, data);
    }

    private void startTimer() {
        stopTimer();
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(task);
            }
        };
        timer.schedule(timerTask, 10000, 10000);
        Util.log(TAG,"startTimer timer iniciado");
    }

    private void updateDriverMarkerPosition(DriverData data) {
        baseActivity.animateMarkerToGB(new LatLng(data.latitude, data.longitude), data.driverMarker, false, googleMap);
    }

    public void stopTimer() {
        if (timer != null) {
            timer.cancel();
            if (handler != null)
                handler.removeCallbacks(task);
            timer = null;
            Util.log(TAG,"stopTimer timer detenido");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopTimer();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.pickUpRL:
                searchLocation(Const.PICKUP);
                break;
            case R.id.dropOffRL:
                searchLocation(Const.DROPOFF);
                break;
            case R.id.toolbar_imageIV:
                ((MainActivity) baseActivity).slideMenu.open(false, true);
                break;
            case R.id.backIV:
                /*
                if (pagerPos - 1 < 0) {
                    pagerPos = vehicleList.size() - 1;
                    pagerVP.setCurrentItem(pagerPos, true);
                } else {
                    pagerPos--;
                    pagerVP.setCurrentItem(pagerPos, true);
                }
                */
                break;
            case R.id.nextIV:
                /*
                if (pagerPos + 1 >= vehicleList.size()) {
                    pagerPos = 0;
                    pagerVP.setCurrentItem(pagerPos, true);
                } else {
                    pagerPos++;
                    pagerVP.setCurrentItem(pagerPos, true);
                }
                */
                break;
            case R.id.bookNowTV:
                drawButtonBottom(bookNowTV,bookLaterTV,R.mipmap.ic_request_trip,R.mipmap.ic_schedule_your_trip);
                if (isRideLater) {
                    isGetDriver = false;
                    timeRL.setVisibility(View.GONE);

                    isRideLater = false;
                    showFare(vehicleList, discountedPrice, estimate_time);
                } else {
                    isGetDriver = false;
                    timeRL.setVisibility(View.GONE);
                    isRideLater = false;
                }
                if (dropOffRL.getVisibility() == View.VISIBLE && rideValidations() && vehicleList.size() > 0 && !isRideLater) {
                    addRide();
                }
                if (dropOffRL.getVisibility() == View.GONE) {
                    dropOffRL.setVisibility(View.VISIBLE);
                }


                break;
            case R.id.bookLaterTV:
                drawButtonBottom(bookLaterTV,bookNowTV,R.mipmap.ic_schedule_your_trip_white,R.mipmap.ic_request_trip_black);
                isRideLater = true;
                isGetDriver = false;
                showFare(vehicleList, discountedPrice, estimate_time);
                timeRL.setVisibility(View.VISIBLE);
                if (!dateTV.getText().toString().isEmpty() &&
                        !dateTV.getText().toString().equalsIgnoreCase(getString(R.string.select_time))) {
                    if (isRideLater) {
                        if (rideValidations())
                            addRide();
                    }
                } else {
                    showToast(getString(R.string.please_select_ride_time));
                }
                if (dropOffRL.getVisibility() == View.GONE) {
                    dropOffRL.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.timeRL:
                showDatePickerDialog();
                break;
            case R.id.etCompany:
                companiesShowOptionsDialog();
                break;
        }
    }

    /**
     * Coloca el fondo al boton seleccionado
     * @param selected
     * @param unselected
     * @param selectedDrawable
     * @param unselectedDrawable
     */
    private void drawButtonBottom(TextView selected, TextView unselected,int selectedDrawable,int unselectedDrawable){
        selected.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.bg_degraded));
        selected.setTextColor(ContextCompat.getColor(getContext(),R.color.White));
        selected.setCompoundDrawablesWithIntrinsicBounds(selectedDrawable, 0, 0, 0);

        unselected.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.White));
        unselected.setTextColor(ContextCompat.getColor(getContext(),R.color.Black));
        unselected.setCompoundDrawablesWithIntrinsicBounds( unselectedDrawable, 0, 0, 0);
    }

    private boolean rideValidations() {
        if (pickupAddress.isEmpty() || pickupAddress.equalsIgnoreCase(getString(R.string.pick_up_location)))
            showToast(baseActivity.getString(R.string.please_select_pickup));
        else if (dropOffAddress.isEmpty() || dropOffAddress.equalsIgnoreCase(getString(R.string.drop_off_location)))
            showToast(baseActivity.getString(R.string.please_Select_droppoff));
        else if (!pickUpLocationTV.getText().toString().isEmpty() && !dropOffTV.getText().toString().isEmpty() && pickUpLocationTV.getText().toString().equalsIgnoreCase(dropOffTV.getText().toString()))
            showToast(baseActivity.getString(R.string.invalid_address));
        else return true;
        return false;
    }


    private void showOptionsDialog(int type, String message, String[] arr) {
        AlertDialog.Builder builder = new AlertDialog.Builder(baseActivity);
        builder.setCancelable(false);
        builder.setTitle(!message.equals("") ? message : baseActivity.getString(R.string.select_));
        builder.setItems(arr, new onClicckkk(type, arr) {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (type == 3) {
                    noOfHours = (arr[which]);
                }
            }
        });
        builder.setNegativeButton(baseActivity.getString(R.string.cancel), new onClicckkk(type, arr) {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (type == 3) {
                    noOfHours = "0";
                }
            }
        });
        builder.create().show();
    }

    /**
     * Envia la reservacion a los conductores cercanos.
     */
    private void addRide() {
        RequestParams params = new RequestParams();
        params.put("Ride[location]", pickupAddress);
        params.put("Ride[destination]", dropOffAddress);
        params.put("Ride[location_lat]", pickupPlace.latitude);
        params.put("Ride[location_long]", pickupPlace.longitude);
        params.put(" Ride[amount]", amount);
        if (dropoffPlace != null) {
            params.put("Ride[destination_lat]", dropoffPlace.latitude);
            params.put("Ride[destination_long]", dropoffPlace.longitude);
        }
        params.put("Ride[number_of_passengers]",1);
        params.put("Ride[number_of_bags]", 0);
        if (noOfHours != null) {
            params.put("Ride[number_of_hours]", noOfHours.isEmpty() ? "0" : noOfHours);
        }
        params.put(" Ride[car_price_id]", vehicleList.get(pagerPos).id);
        params.put(" Ride[is_hourly]", 0);
        params.put(" Ride[is_pet]",0);
        
        if(companiesSelected.size() > 0){
            int i = 0;
            Iterator<Integer> itr2 = companiesSelected.keySet().iterator();
            while (itr2.hasNext()) {
                Integer key = itr2.next();
                params.put(" Ride[companies]["+i+"]",companiesSelected.get(key).id);
                i++;
            }
        }

        if (isRideLater) {
            if (dateTV.getText().toString().isEmpty() && dateTV.getText().toString().length() < 14)
                showToast(baseActivity.getString(R.string.please_select_time));
            else {
                params.put("Ride[journey_type]", Const.RIDE_LATER);
                params.put(" Ride[journey_time]", baseActivity.changeDateFormatFromDateUTC(selectedDate, "yyyy-MM-dd HH:mm:ss"));
                baseActivity.syncManager.sendToServer(Const.API_RIDE_REQUEST, params, this);
            }
        } else {
            params.put("Ride[journey_type]", Const.RIDE_NOW);
            params.put(" Ride[journey_time]", "");
            baseActivity.syncManager.sendToServer(Const.API_RIDE_REQUEST, params, this);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == Const.PICKUP) {
                if (data != null) {
                    pickupAddress = data.getStringExtra("Location Address");
                    bundle = new Bundle();
                    bundle = data.getBundleExtra("Latlng");
                    if (bundle != null) {
                        lat = bundle.getDouble("lat");
                        lng = bundle.getDouble("longt");

                    }
                    if (pickupAddress != null && !pickupAddress.isEmpty()) {
                        if (bundle != null)
                            pickupPlace = new LatLng(lat, lng);
                        else
                            pickupPlace = googleApiHandle.getLatLngFromAddress(pickupAddress);
                        pickUpLocationTV.setText(pickupAddress);
                        dropOffRL.setVisibility(View.VISIBLE);
                        if (pickupPlace != null) {
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pickupPlace, 15));
                            currentPinFL.setVisibility(View.GONE);
                            setPickupMarker(pickupPlace);
                        }
                        // In case reselecting pickup address so redraw polyline and moves marker accordingly
                        if (pickupPlace != null && dropoffPlace != null) {
                            if (googleMap != null) {
                                googleApiHandle.getDirectionsUrl(pickupPlace, dropoffPlace, googleMap);
                                showRideDetail();
                            }
                        } else if (pickupPlace == null) {
                            showToast(baseActivity.getString(R.string.this_address_is_currently));
                        }
                    }
                }

            } else if (requestCode == Const.DROPOFF) {
                if (pickupPlace == null && currentLocation != null) {
                    pickupPlace = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                }
                currentPinFL.setVisibility(View.GONE);
                if (data != null) {
                    dropOffAddress = data.getStringExtra("Location Address");
                    if (data.getStringExtra("lat") != null) {
                        droplat = Double.valueOf(data.getStringExtra("lat"));
                        droplng = Double.valueOf(data.getStringExtra("lng"));
                        dropoffPlace = new LatLng(droplat, droplng);
                        if (pickupPlace != null) {
                            googleApiHandle.getDirectionsUrl(pickupPlace, dropoffPlace, googleMap);
                            dropOffTV.setText(dropOffAddress);
                            showRideDetail();

                        }
                    } else {


                        if (dropOffAddress != null && !dropOffAddress.isEmpty()) {
                            dropoffPlace = googleApiHandle.getLatLngFromAddress(dropOffAddress);
                            if (pickupPlace != null && dropoffPlace != null) {
                                googleApiHandle.getDirectionsUrl(pickupPlace, dropoffPlace, googleMap);
                                dropOffTV.setText(dropOffAddress);
                                showRideDetail();

                            }
                        } else {
                            showToast(baseActivity.getString(R.string.address_is_currently_unavailable));
                        }
                    }
                }
            }


        }

    }

    @Override
    public void onPolyLineReceived(LatLng origin, LatLng destination, GoogleMap routeMap, Polyline polyline, double distance, String time, String lin) {
        if (time != null) {
            dist = distance;
            this.polyline = polyline;
            estimate_time = time.trim();
            List<LatLng> points = polyline.getPoints();
            //Util.log(TAG,"onPolyLineReceived: "+points.size());
            if(points.size() > 0){
                setPickupMarker(points.get(0));
            }else{
                setPickupMarker(origin);
            }
            if(points.size() > 0){
                setDestinationMarker(points.get(points.size() - 1), time, distance);
            }else{
                setDestinationMarker(destination, time, distance);
            }

            if (currentLocation != null)
                getCarType(currentLocation.getLatitude(), currentLocation.getLongitude());
        } else {
            showToast(baseActivity.getString(R.string.you_cant_ride_because_des_too_far));
            rideDetailLL.setVisibility(View.GONE);
        }
    }

    @SuppressLint("SetTextI18n")
    private void showFare(ArrayList<VehicleData> vehicleList, String discountedPrice, String estimate_time) {
        double base_p, min_p, mile_p;

        if (vehicleList.size() > 0) {
            String time = estimate_time.split(" ")[0];
            base_p = Double.parseDouble(vehicleList.get(pagerPos).base_price);
            base_time = Double.parseDouble(time);
            min_p = Double.parseDouble(vehicleList.get(pagerPos).price_min);
            mile_p = Double.parseDouble(vehicleList.get(pagerPos).price_mile);
            appxPrice = base_p + (base_time * min_p) + (dist * mile_p);
            appxPrice = Util.calculateNextIntValue(appxPrice);

            amount = new BigDecimal(appxPrice);
            amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP);


            if (!discountedPrice.equalsIgnoreCase("0") && !isRideLater) {
                promoTV.setVisibility(View.VISIBLE);
                double discountprice = (appxPrice / 100) * Double.valueOf(discountedPrice);
                if (max_amount > discountprice) {
                    double price = appxPrice - discountprice;
                    amount = new BigDecimal(price);
                    amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP);
                    appxTV.setText(getString(R.string.appx_price) + String.valueOf(amount));

                } else {
                    double price = appxPrice - max_amount;
                    amount = new BigDecimal(price);
                    amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP);
                    appxTV.setText(baseActivity.getString(R.string.appx_price) + String.valueOf(amount));
                }

            } else {
                promoTV.setVisibility(View.GONE);
                appxTV.setText(baseActivity.getString(R.string.appx_price) + String.valueOf(amount));
            }
        }
    }

    private void showRideDetail() {
        rideDetailLL.setVisibility(View.VISIBLE);
        if (vehicleList.size() > 0) {
            startAnimation(rideDetailLL, true);
        }
    }

    private void setPickupMarker(LatLng pickupLatLng) {
        if (pickupMarker == null)
            pickupMarker = googleMap.addMarker(new MarkerOptions()
                    .position(pickupLatLng).draggable(false)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_pin_green)));
        else {
            pickupMarker.setPosition(pickupLatLng);
        }
    }

    private void setDestinationMarker(LatLng dropoffLatLng, String time, double distance) {
        if (destinationMarker == null) {
            MarkerOptions options = new MarkerOptions()
                    .position(dropoffLatLng).draggable(false)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_pin_red));
            if (time != null && !time.isEmpty()) {
                options.title(getString(R.string.estimate_time_distance));
                options.snippet(getString(R.string.time) + time + " & " + getString(R.string.distance) + distance + baseActivity.getString(R.string.km));
            }
            destinationMarker = googleMap.addMarker(options);
            destinationMarker.showInfoWindow();

        } else {
            if (time != null && !time.isEmpty()) {
                destinationMarker.setTitle(baseActivity.getString(R.string.estimate_time_distance));
                destinationMarker.setSnippet(getString(R.string.time) + time + " & " + getString(R.string.distance) + distance + getString(R.string.km));
            }
            destinationMarker.setPosition(dropoffLatLng);
            destinationMarker.showInfoWindow();

        }
    }

    private void searchLocation(int requestCode) {
        Intent i = new Intent(getActivity(), PickLocationActivity.class);
        if (requestCode == Const.PICKUP)
            i.putExtra("title", baseActivity.getString(R.string.pick_up_location));
        else if (requestCode == Const.DROPOFF)
            i.putExtra("title", baseActivity.getString(R.string.drop_off_location));
        getActivity().startActivityForResult(i, requestCode);
    }

    public void showCarDetailDialog(int postition) {
        VehicleData data = vehicleList.get(postition);
        dialog = new Dialog(baseActivity, R.style.Theme_AppCompat_Light_Dialog);
        dialog.setContentView(R.layout.dialog_car_details);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);


        ImageView carIV = (ImageView) dialog.findViewById(R.id.carIV);
        ImageView cancelIV = (ImageView) dialog.findViewById(R.id.cancelIV);
        TextView carTypeTV = (TextView) dialog.findViewById(R.id.carTypeTV);
        TextView seatsTV = (TextView) dialog.findViewById(R.id.seatsTV);
        TextView baseFareTV = (TextView) dialog.findViewById(R.id.baseFareTV);
        TextView perMileTV = (TextView) dialog.findViewById(R.id.perMileTV);
        TextView waitingTV = (TextView) dialog.findViewById(R.id.waitingTV);
        Button doneBT = (Button) dialog.findViewById(R.id.doneBT);
        cancelIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        doneBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        carIV.setImageResource(pics[data.type_id]);
        carTypeTV.setText(data.description);
        seatsTV.setText(String.valueOf(data.seat));
        baseFareTV.setText(String.valueOf(baseActivity.getString(R.string.dollar) + " " + data.base_price));
        perMileTV.setText(String.valueOf(baseActivity.getString(R.string.dollar) + " " + data.price_mile));
        waitingTV.setText(String.valueOf(baseActivity.getString(R.string.dollar) + " " + data.price_min));


        if (dialog.isShowing()) {
            dialog.dismiss();
            dialog.show();
        } else
            dialog.show();
    }

    private ArrayList<VehicleData> parseVehicleData(JSONArray list) {
        ArrayList<VehicleData> vehicleLists = new ArrayList<>();
        for (int i = 0; i < list.length(); i++) {
            VehicleData vehicleData = new VehicleData();
            JSONObject vehicle = list.optJSONObject(i);
            vehicleData.id = vehicle.optInt("id");
            vehicleData.currency_code = vehicle.optString("currency_code");
            vehicleData.currency_symbol = vehicle.optString("currency_symbol");
            vehicleData.base_price = vehicle.optString("base_price");
            vehicleData.price_min = vehicle.optString("price_min");
            vehicleData.price_mile = vehicle.optString("price_mile");
            vehicleData.description = vehicle.optString("description");
            vehicleData.seat = vehicle.optInt("seat");
            vehicleData.type_id = vehicle.optInt("type_id");
            vehicleLists.add(vehicleData);
        }
        return vehicleLists;
    }

    private void showDatePickerDialog() {
        isFirstDatePick = true;
        mcurrentTime = Calendar.getInstance();
        changeTime = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(baseActivity, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar selectedCalendar = Calendar.getInstance();
                selectedCalendar.set(year, monthOfYear, dayOfMonth);
                if (selectedCalendar.before(mcurrentTime)) {
                    showToast(getString(R.string.past_date));
                } else {
                    changeTime = selectedCalendar;
                    pickDeliverTime();
                }
            }
        }, changeTime.get(Calendar.YEAR), changeTime.get(Calendar.MONTH), changeTime.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        if (isFirstDatePick) {
            datePickerDialog.show();
            isFirstDatePick = false;
            isFirstTimePick = true;
        }
        datePickerDialog.setTitle("");
    }

    private void setDate() {

        dateTV.setText(baseActivity.changeDateFormatFromDate(changeTime.getTime(), "dd-MMM-yyyy,hh:mm:a"));
        selectedDate = changeTime.getTime();
    }

    private void pickDeliverTime() {
        mcurrentTime.add(Calendar.SECOND, -1);
        hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(baseActivity, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                changeTime.set(Calendar.HOUR_OF_DAY, selectedHour);
                changeTime.set(Calendar.MINUTE, selectedMinute);
                if (changeTime.after(mcurrentTime))
                    setDate();
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle(getString(R.string.please_select_time));
        if (isFirstTimePick) {
            mTimePicker.show();
            isFirstTimePick = false;
        }
    }

    @SuppressLint("MissingPermission")
    private void parseDrivers(JSONObject jsonObject) throws JSONException {
        JSONArray array = jsonObject.getJSONArray("list");
        JSONObject closestdriver = jsonObject.optJSONObject("closestdriver");
        String time = null;
        if(closestdriver != null && closestdriver.has("time")){
            time = closestdriver.optString("time");
        }
        if(pagerAdapter != null){
            View v = pagerVP.getChildAt(pagerAdapter.getCurrentItem());
            if(v != null){
                TextView tvCarTime = v.findViewById(R.id.tvCarTime);
                tvCarTime.setText(time);
            }
        }

        if (driverData.size() == 0) {
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                DriverData data = new DriverData(Parcel.obtain());
                data.id = object.getInt("id");
                data.first_name = object.getString("first_name");
                data.last_name = object.getString("last_name");
                data.contact_no = object.getString("contact_no");
                try {
                    data.latitude = Double.parseDouble(object.getString("lat"));
                    data.longitude = Double.parseDouble(object.getString("long"));
                    data.image_file = object.getString("image_file");
                    JSONObject driverObj = object.getJSONObject("driver");
                    data.car_type = Integer.parseInt(driverObj.getString("vehicle"));
                    driverData.add(data);
                    setDriverOnMap(data, data.car_type);
                }catch(NumberFormatException e){
                }
            }
        } else {
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                DriverData data = new DriverData(Parcel.obtain());
                try {
                    data.id = object.getInt("id");
                    data.first_name = object.getString("first_name");
                    data.last_name = object.getString("last_name");
                    data.contact_no = object.getString("contact_no");
                    data.latitude = Double.parseDouble(object.getString("lat"));
                    data.longitude = Double.parseDouble(object.getString("long"));
                    data.image_file = object.getString("image_file");
                    JSONObject driverObj = object.getJSONObject("driver");
                    data.car_type = Integer.parseInt(driverObj.getString("vehicle"));
                    if (driversMarkerHashMap.containsKey(data.id)) {
                        data.driverMarker = driversMarkerHashMap.get(data.id).driverMarker;
                        updateDriverMarkerPosition(data);
                    } else
                        setDriverOnMap(data, data.car_type);
                }catch (NumberFormatException e){

                }
            }
            locationUpdateService.getFusedLocationClient().getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            currentLocation = location;
                        }
                    });
        }
    }

    private JourneyData parseJourneyData(JSONObject jsonObject) {
        JSONObject detail = jsonObject.optJSONObject("detail");
        JourneyData journeyData = new JourneyData(Parcel.obtain());
        journeyData.id = detail.optInt("id");
        journeyData.state_id = detail.optInt("state_id");
        journeyData.amount = detail.optString("amount");
        journeyData.location = detail.optString("location");
        journeyData.destination = detail.optString("destination");
        journeyData.start_time = detail.optString("start_time");
        journeyData.end_time = detail.optString("end_time");
        journeyData.create_time = detail.optString("create_time");
        journeyData.is_hourly = detail.optBoolean("is_hourly");
        journeyData.location_lat = detail.optDouble("location_lat");
        journeyData.location_long = detail.optDouble("location_long");
        journeyData.destination_lat = detail.optDouble("destination_lat");
        journeyData.destination_long = detail.optDouble("destination_long");
        journeyData.number_of_passengers = detail.optInt("number_of_passengers");
        journeyData.number_of_bags = detail.optInt("number_of_bags");
        journeyData.number_of_hours = detail.optInt("number_of_hours");
        journeyData.journey_type = detail.optInt("journey_type");
        return journeyData;
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        setMyLOcation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        buildAlertMessageNoGps();

    }

    private void buildAlertMessageNoGps() {
        if (isAdded()) {
            if (alert == null) {
                alert = new AlertDialog.Builder(baseActivity);
                alert.setMessage(baseActivity.getString(R.string.your_gps_seems_to_be_disable))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(@SuppressWarnings("unused") DialogInterface dialog, @SuppressWarnings("unused") int id) {
                                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                dialog.dismiss();
                                alert = null;
                            }
                        })
                        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, @SuppressWarnings("unused") int id) {
                                dialog.cancel();
                                alert = null;
                            }
                        });
                alert.show();
            }
        }
    }

    /**
     * Muestra las empresas
     */
    public void companiesShowOptionsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(baseActivity,R.style.myDialog);
        builder.setTitle(R.string.select_);
        //companiesArr = new String[companiesJson.length()];
        boolean[] checkedItems = new boolean[companiesArr.length]; //this will checked the items when user open the dialog
        for(int i=0;i<companiesArr.length;i++){
            if(companiesSelected.containsKey(i)){
                checkedItems[i] = true;
            }else{
                checkedItems[i] = false;
            }
        }
        builder.setMultiChoiceItems(companiesArr, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if(isChecked){
                    companiesSelected.put(which,companies[which]);
                }else{
                    companiesSelected.remove(which);
                }
            }
        });

        builder.setPositiveButton(R.string.done, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String selected = "";
                long i = 0;
                Iterator<Integer> itr2 = companiesSelected.keySet().iterator();
                while (itr2.hasNext()) {
                    Integer key = itr2.next();
                    selected += companiesSelected.get(key).name;
                    if(itr2.hasNext()){
                        selected += ", ";
                    }else{
                        selected += ".";
                    }
                }
                etCompany.setText(selected);
                dialog.dismiss();
                //Vuelve a buscar los conductores cercanos
                task.run();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onLocationReceived(Location location) {

        if (sourceMarker != null)
            baseActivity.animateMarkerToGB(new LatLng(location.getLatitude(), location.getLongitude()), sourceMarker, false, googleMap);

    }

    @Override
    public void onConntected(Bundle bundle) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (view != null)
            stopTimer();
    }

    @Override
    public void permGranted(int resultCode) {

        setCurrentLocation();
    }

    @Override
    public void permDenied(int resultCode) {

    }

    abstract class onClicckkk implements DialogInterface.OnClickListener {
        int type;
        String[] arr;

        onClicckkk(int type, String[] arr) {
            this.type = type;
            this.arr = arr;
        }

    }

    private class PermReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            setCurrentLocation();
        }
    }
}

package com.olin.passenger.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcel;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.pkmmte.view.CircularImageView;
import com.olin.passenger.R;
import com.olin.passenger.activity.BaseActivity;
import com.olin.passenger.activity.MainActivity;
import com.olin.passenger.adapter.CustomSpinnerAdapter;
import com.olin.passenger.adapter.PostalCodeAdapter;
import com.olin.passenger.data.CountryData;
import com.olin.passenger.data.ProfileData;
import com.olin.passenger.data.SearchSignUp;
import com.olin.passenger.utils.Const;
import com.olin.passenger.utils.ImageUtils;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * Created by TOXSL\ankush.walia on 7/2/17.
 * Vista de perfil
 */
public class ProfileFragment extends BaseFragment implements ImageUtils.ImageSelectCallback, BaseActivity.PermCallback {

    private View view;
    private CircularImageView profileCIV;
    private EditText nameET, emailET, phoneNumberET, lastNameET, postalET, colonyET, stateET, municipalityET;
    private MenuItem edit;
    private MenuItem done;
    private Spinner countrySP;
    private int country_id;
    private ImageView selectImageIV;
    private File profileFile;
    private TextView firstNameTV;
    private LinearLayout lastNmLL;
    private ProfileData profileData;
    private boolean isUpdated;
    private Button searchBT;
    private int pagecount = 0;
    private int totalPages;
    private ArrayList<SearchSignUp> searchSignUps = new ArrayList<>();
    private PostalCodeAdapter postalCodeAdapter;
    private AlertDialog dialog;
    private int postalId;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_passenger_profile, null);
        ((MainActivity) baseActivity).setActionBarTitle(getString(R.string.profile), true);

        initUI();
        getProfile();
        return view;
    }

    private void getCountryCode() {
        baseActivity.syncManager.sendToServer("api/user/country?id=", null, this);
    }

    private void initUI() {
        profileData = baseActivity.getProfileDataFromPrefStore();
        profileCIV = (CircularImageView) view.findViewById(R.id.profileCIV);
        selectImageIV = (ImageView) view.findViewById(R.id.selectImageIV);
        nameET = (EditText) view.findViewById(R.id.nameET);
        lastNameET = (EditText) view.findViewById(R.id.lastNameET);
        firstNameTV = (TextView) view.findViewById(R.id.firstNameTV);
        lastNmLL = (LinearLayout) view.findViewById(R.id.lastNmLL);
        emailET = (EditText) view.findViewById(R.id.emailET);
        postalET = (EditText) view.findViewById(R.id.postalET);
        colonyET = (EditText) view.findViewById(R.id.colonyET);
        municipalityET = (EditText) view.findViewById(R.id.municipalityET);
        stateET = (EditText) view.findViewById(R.id.stateET);
        searchBT = (Button) view.findViewById(R.id.searchBT);
        phoneNumberET = (EditText) view.findViewById(R.id.phoneNumberET);
        countrySP = (Spinner) view.findViewById(R.id.countrySP);
        selectImageIV.setOnClickListener(this);
        searchBT.setOnClickListener(this);
        countrySP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryData dataItem = (CountryData) parent.getItemAtPosition(position);
                if (!(((CountryData) countrySP.getSelectedItem()).id == 0)) {
                    country_id = dataItem.id;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        getCountryCode();
    }

    private void updateProfile() {
        RequestParams params = new RequestParams();
        params.put("User[first_name]", nameET.getText().toString());
        params.put("User[last_name]", lastNameET.getText().toString());

        if (profileFile != null) {
            try {
                params.put("User[image_file]", profileFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        params.put("User[contact_no]", phoneNumberET.getText().toString().trim());
        if (country_id != 0)
            params.put("User[country_id]", country_id);
        if (postalId != 0) {
            params.put("User[sepomex_id]", postalId);
        }
        baseActivity.syncManager.sendToServer("api/user/passenger-update", params, this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.selectImageIV:
                if (baseActivity.checkPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Const.USER_IMAGE, this)) {
                    ImageUtils.ImageSelect.Builder builder = new ImageUtils.ImageSelect.Builder(baseActivity, this, Const.USER_IMAGE);
                    builder.crop(true).start();
                }
                break;
            case R.id.searchBT:
                if (postalET.getText().length() < 3) {
                    baseActivity.showToast(getString(R.string.please_enter_atleast_three_number));
                } else {
                    pagecount = 0;
                    hitSearchApi();
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        baseActivity.isEdit = false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.profile_edit_menu, menu);
        edit = menu.getItem(0);
        done = menu.getItem(1);
        if (baseActivity.isEdit) {
            edit.setVisible(false);
            done.setVisible(true);
            profileCIV.setEnabled(true);
            nameET.setEnabled(true);
            emailET.setEnabled(true);

            lastNmLL.setVisibility(View.VISIBLE);
            searchBT.setVisibility(View.VISIBLE);
            selectImageIV.setVisibility(View.VISIBLE);
            phoneNumberET.setEnabled(true);
            countrySP.setVisibility(View.VISIBLE);
            postalET.setEnabled(true);
        } else {
            profileCIV.setEnabled(false);
            countrySP.setVisibility(View.GONE);
            lastNmLL.setVisibility(View.GONE);

            selectImageIV.setVisibility(View.GONE);
            searchBT.setVisibility(View.GONE);
            nameET.setEnabled(false);
            emailET.setEnabled(false);
            countrySP.setVisibility(View.GONE);
            phoneNumberET.setEnabled(false);
            postalET.setFocusableInTouchMode(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit_profile:
                editClick();
                break;
            case R.id.update_profile:
                updateClick();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateClick() {
        if (ProfileValidations()) {
            updateProfile();
            edit.setVisible(true);
            done.setVisible(false);
            baseActivity.isEdit = false;
            nameET.setEnabled(false);
            emailET.setEnabled(false);
            profileCIV.setEnabled(false);
            phoneNumberET.setEnabled(false);
            countrySP.setVisibility(View.GONE);
            firstNameTV.setText(getString(R.string.name));
            lastNmLL.setVisibility(View.GONE);
            nameET.setText(profileData.first_name.concat(" " + profileData.last_name));
            setEditTextMaxLength(phoneNumberET, 15);
            phoneNumberET.setText(profileData.country.telephone_code.concat(" " + profileData.contact_no));
            countrySP.setVisibility(View.GONE);
            selectImageIV.setVisibility(View.GONE);

        }
    }

    private boolean ProfileValidations() {
        if (nameET.getText().toString().trim().isEmpty())
            showToast(getString(R.string.please_enter_first_name));
        else if (lastNameET.getText().toString().trim().equals(""))
            showToast(getString(R.string.please_enter_last_name));
        else if (emailET.getText().toString().trim().equals(""))
            showToast(getString(R.string.please_enter_email));
        else if (!baseActivity.isValidMail(emailET.getText().toString()))
            showToast(getString(R.string.please_enter_valid_email_id));
        else if (phoneNumberET.getText().toString().trim().equals(""))
            showToast(getString(R.string.please_enter_phone_number));
        else
            return true;
        return false;
    }

    @SuppressLint("SetTextI18n")
    private void editClick() {
        baseActivity.isEdit = true;
        edit.setVisible(false);
        done.setVisible(true);
        profileCIV.setEnabled(true);
        nameET.setText(profileData.first_name);
        lastNameET.setText(profileData.last_name);
        firstNameTV.setText(getString(R.string.first_name));
        lastNmLL.setVisibility(View.VISIBLE);
        postalET.setFocusableInTouchMode(true);
        searchBT.setVisibility(View.VISIBLE);
        selectImageIV.setVisibility(View.VISIBLE);
        nameET.setEnabled(true);
        if (!isUpdated)
            emailET.setEnabled(true);
        countrySP.setVisibility(View.VISIBLE);
        phoneNumberET.setEnabled(true);
        setEditTextMaxLength(phoneNumberET, 11);
        phoneNumberET.setText(profileData.contact_no);
    }


    public void setEditTextMaxLength(EditText editText, int length) {
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(length);
        editText.setFilters(FilterArray);
    }


    private void getProfile() {
        baseActivity.syncManager.sendToServer("api/user/profile", null, this);
    }

    private ArrayList<CountryData> countryDatas = new ArrayList<>();
    private CustomSpinnerAdapter spinnerAdapter;

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject1) {
        super.onSyncSuccess(controller, action, status, jsonObject1);
        if (action.equals("profile")) {
            try {
                JSONObject jsonObject = jsonObject1.getJSONObject("detail");
                setData(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (controller.equals("user") && action.equals("country")) {
            if (status) {
                try {
                    JSONArray country = jsonObject1.getJSONArray("country");
                    parseCountryData(country);
                    spinnerAdapter = new CustomSpinnerAdapter(baseActivity, countryDatas, true);
                    countrySP.setAdapter(spinnerAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        } else if (controller.equals("user") && action.equals("passenger-update")) {
            if (status) {
                try {
                    JSONObject jsonObject = jsonObject1.getJSONObject("detail");
                    getProfile();
                    ProfileData profileData = baseActivity.parseProfileData(jsonObject);
                    baseActivity.saveProfileDataInPrefStore(profileData);
                    showToast(getString(R.string.profile_updated_successfully));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else
                showToast(jsonObject1.optString("error"));
        } else if (controller.equals("user") && action.equals("address-search")) {
            if (status) {
                JSONArray jsonArray = jsonObject1.optJSONArray("details");
                totalPages = jsonObject1.optInt("totalPage");
                if (totalPages >= pagecount) {
                    pagecount++;
                }
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.optJSONObject(i);
                    SearchSignUp searchSignUp = new SearchSignUp(Parcel.obtain());
                    searchSignUp.id = object.optInt("id");
                    searchSignUp.settlement = object.optString("settlement");
                    searchSignUp.municipality = object.optString("municipality");
                    searchSignUp.state = object.optString("state");
                    searchSignUp.zipcode = object.optInt("zipcode");
                    searchSignUps.add(searchSignUp);
                }
                if (postalCodeAdapter != null) {
                    if (!dialog.isShowing())
                        dialog.show();
                    postalCodeAdapter.notifyDataSetChanged();
                } else {
                    showPostalCodeDialog();
                }
            } else {
                if (pagecount > 0) {
                    showToast(getString(R.string.no_more_zipcodes));
                } else {
                    showToast(jsonObject1.optString("error"));
                }
            }

        }

    }

    private void parseCountryData(JSONArray country) throws JSONException {
        for (int i = 0; i < country.length(); i++) {
            JSONObject countryObjct = country.getJSONObject(i);
            CountryData countryData = new CountryData(Parcel.obtain());
            countryData.id = countryObjct.getInt("id");
            countryData.title = countryObjct.getString("title");
            countryData.telephone_code = countryObjct.getString("telephone_code");
            countryData.flag = countryObjct.getString("flag");
            if (countryObjct.has("car_type")) {
                JSONArray car_type = countryObjct.getJSONArray("car_type");
                if (car_type.length() != 0) {
                    int[] carTypes = new int[car_type.length()];
                    for (int j = 0; j < car_type.length(); j++) {
                        carTypes[j] = (int) car_type.get(j);
                        countryData.arr_car_type = carTypes;
                    }
                    countryDatas.add(countryData);
                }
            }

        }
    }

    private void setData(JSONObject jsonObject) throws JSONException {
        profileData = baseActivity.parseProfileData(jsonObject);
        baseActivity.saveProfileDataInPrefStore(profileData);
        try {
            baseActivity.picasso.load(profileData.image_file).error(R.mipmap.default_avatar).placeholder(R.mipmap.default_avatar).into(profileCIV);
        } catch (Exception e) {
            e.printStackTrace();
        }
        nameET.setText(profileData.first_name.concat(" " + profileData.last_name));
        if ((MainActivity) getActivity() != null)
            ((MainActivity) getActivity()).updateDrawer();
        lastNameET.setText(profileData.last_name);
        isUpdated = !profileData.email.isEmpty() || !profileData.email.equalsIgnoreCase("");
        emailET.setText(profileData.email);
        if (profileData.country != null) {
            phoneNumberET.setText(profileData.country.telephone_code.concat(" " + profileData.contact_no));

            for (int i = 0; i < countryDatas.size(); i++) {
                if (countryDatas.get(i).telephone_code.equals(profileData.country.telephone_code)) {
                    countrySP.setSelection(i);
                    break;
                }
            }
        }
        if (profileData.image_file != null && (!profileData.image_file.isEmpty())) {
            CircularImageView prof_drawer = ((MainActivity) baseActivity).profileCIV;
            baseActivity.picasso.load(profileData.image_file).placeholder(R.mipmap.default_avatar).into(profileCIV);
            baseActivity.picasso.load(profileData.image_file).placeholder(R.mipmap.default_avatar).into(prof_drawer);
        }
        if (profileData.settlement != null && !profileData.settlement.isEmpty()) {
            colonyET.setText(profileData.settlement);
            municipalityET.setText(profileData.municipality);
            stateET.setText(profileData.state);
            postalET.setText(profileData.zipcode);

        }

    }

    @Override
    public void permGranted(int resultCode) {
        ImageUtils.ImageSelect.Builder builder = new ImageUtils.ImageSelect.Builder(baseActivity, this, Const.USER_IMAGE);
        builder.start();
    }

    @Override
    public void permDenied(int resultCode) {

    }

    @Override
    public void onImageSelected(String imagePath, int resultCode) {
        Bitmap bitmap = ImageUtils.imageCompress(imagePath);
        profileCIV.setImageBitmap(bitmap);
        profileFile = ImageUtils.bitmapToFile(bitmap, baseActivity);
    }

    public void hitSearchApi() {
        RequestParams params = new RequestParams();
        params.put("Sepomex[zipcode]", postalET.getText().toString());
        baseActivity.syncManager.sendToServer(Const.API_SEARCH_ON_SIGNUP + "?page=" + pagecount, params, this);
    }

    private void showPostalCodeDialog() {
        if (searchSignUps != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(baseActivity);
            builder.setTitle("Select your address");

            LayoutInflater inflater = LayoutInflater.from(baseActivity);
            View content = inflater.inflate(R.layout.postaldialog, null);
            builder.setView(content);
            builder.setCancelable(false);
            builder.setPositiveButton("Cancel", new OnClicKKDialog(searchSignUps) {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    searchSignUps.clear();
                    dialog.dismiss();
                }
            });
            ListView postalLV = (ListView) content.findViewById(R.id.postalLV);

            postalCodeAdapter = new PostalCodeAdapter(baseActivity, searchSignUps, this);
            postalLV.setAdapter(postalCodeAdapter);
            postalLV.setOnItemClickListener(new OnClicKK(searchSignUps) {
                @SuppressLint("SetTextI18n")
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    pagecount = 0;
                    postalId = searchSignUps.get(position).id;
                    postalET.setText("" + searchSignUps.get(position).zipcode);
                    colonyET.setText("" + searchSignUps.get(position).settlement);
                    municipalityET.setText("" + searchSignUps.get(position).municipality);
                    stateET.setText("" + searchSignUps.get(position).state);
                    dialog.dismiss();
                }
            });
            dialog = builder.create();
            dialog.show();
        }
    }

    abstract class OnClicKKDialog implements DialogInterface.OnClickListener {
        ArrayList<SearchSignUp> searchSignUps;

        OnClicKKDialog(ArrayList<SearchSignUp> searchSignUps) {
            this.searchSignUps = searchSignUps;

        }
    }

    abstract class OnClicKK implements AdapterView.OnItemClickListener {
        ArrayList<SearchSignUp> searchSignUps;

        OnClicKK(ArrayList<SearchSignUp> searchSignUps) {
            this.searchSignUps = searchSignUps;

        }
    }

}

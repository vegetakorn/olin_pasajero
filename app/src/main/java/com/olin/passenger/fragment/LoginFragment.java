package com.olin.passenger.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcel;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.facebook.GraphResponse;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.iid.FirebaseInstanceId;
import com.olin.passenger.R;
import com.olin.passenger.activity.LoginSignUpActivity;
import com.olin.passenger.activity.MainActivity;
import com.olin.passenger.adapter.CustomSpinnerAdapter;
import com.olin.passenger.adapter.PostalCodeAdapter;
import com.olin.passenger.data.CountryData;
import com.olin.passenger.data.ProfileData;
import com.olin.passenger.data.RideDetails;
import com.olin.passenger.data.SearchSignUp;
import com.olin.passenger.socialLogin.FacebookLogin;
import com.olin.passenger.socialLogin.SocialLogin;
import com.olin.passenger.utils.Const;
import com.olin.passenger.utils.MyResponse;
import com.olin.passenger.utils.Util;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by TOXSL\ankush.walia on 12/1/17.
 */

public class LoginFragment extends BaseFragment implements SocialLogin.SocialLoginListener {
    private static String TAG = "LoginFragment";
    private View view;
    private EditText emailET, passwordET;
    private TextView forgotPasswordTV, emailLoginTV, signupTV;
    private TextView fbLoginTV;
    private SocialLogin socialLogin;
    private ArrayList<CountryData> countryDatas = new ArrayList<>();
    private String[] arrCountry;
    private Spinner countrySP;
    private CustomSpinnerAdapter spinnerAdapter;
    private int countryID;
    private RequestParams requestParams;
    private EditText postalET, colonyET, stateET, municipalityET;
    private Button searchBT;
    private int pagecount = 0;
    private ArrayList<SearchSignUp> searchSignUps = new ArrayList<>();
    private PostalCodeAdapter postalCodeAdapter;
    private AlertDialog dialog;
    private int postalId;
    private int totalPages;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login, null);
        ((LoginSignUpActivity) baseActivity).toolbar.setVisibility(View.GONE);
        baseActivity.syncManager.setLoginStatus(null);
        socialLogin = SocialLogin.getInstance();
        emailET = (EditText) view.findViewById(R.id.emailET);
        passwordET = (EditText) view.findViewById(R.id.passwordET);
        forgotPasswordTV = (TextView) view.findViewById(R.id.forgotPasswordTV);
        forgotPasswordTV.setOnClickListener(this);
        emailLoginTV = (TextView) view.findViewById(R.id.emailLoginTV);
        fbLoginTV = (TextView) view.findViewById(R.id.fbLoginTV);
        signupTV = (TextView) view.findViewById(R.id.signupTV);
        emailLoginTV.setOnClickListener(this);
        signupTV.setOnClickListener(this);

        fbLoginTV.setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.emailLoginTV:
                if (isValidate()) {
                    login();
                }
                break;
            case R.id.fbLoginTV:
                socialLogin.setListener(this);
                startActivity(new Intent(baseActivity, FacebookLogin.class));
                break;
            case R.id.forgotPasswordTV:
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container, new ForgotPasswordFragment()).addToBackStack(null).commit();
                break;
            case R.id.signupTV:
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container, new SignupFragment()).addToBackStack(null).commit();
                break;
            case R.id.searchBT:
                if (postalET.getText().length() < 3) {
                    baseActivity.showToast(getString(R.string.please_enter_atleast_three_number));
                } else {
                    pagecount = 0;
                    hitSearchApi();
                }
                break;
        }
    }

    public void hitSearchApi() {
        RequestParams params = new RequestParams();
        params.put("Sepomex[zipcode]", postalET.getText().toString());
        baseActivity.syncManager.sendToServer(Const.API_SEARCH_ON_SIGNUP + "?page=" + pagecount, params, this);
    }

    private void login() {
        RequestParams params = new RequestParams();
        params.put("LoginForm[username]", emailET.getText().toString());
        params.put("LoginForm[password]", passwordET.getText().toString());
        params.put("User[role_id]", 1);
        String token = FirebaseInstanceId.getInstance().getToken();
        if (token != null)
            params.put("LoginForm[device_token]", token);
        else

            params.put("LoginForm[device_token]", baseActivity.getUniqueDeviceId());
        params.put("LoginForm[device_type]", 1);
        baseActivity.syncManager.sendToServer(Const.API_USER_LOGIN, params, this);
    }

    private boolean isValidate() {
        if (emailET.getText().toString().trim().equals(""))
            showToast(getString(R.string.please_enter_email));
        else if (!baseActivity.isValidMail(emailET.getText().toString()))
            showToast(getString(R.string.please_enter_valid_email_id));
        else if (passwordET.getText().toString().trim().equals(""))
            showToast(getString(R.string.please_enter_password));
        else
            return true;
        return false;
    }


    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equalsIgnoreCase("user") && action.equalsIgnoreCase("login")) {
            if (status) {
                try {
                    Intent intent = new Intent(baseActivity, MainActivity.class);
                    JSONObject object = jsonObject.getJSONObject("user_detail");
                    ProfileData profileData = baseActivity.parseProfileData(object);
                    baseActivity.saveProfileDataInPrefStore(profileData);
                    baseActivity.store.setBoolean("isFb", false);
                    baseActivity.syncManager.setLoginStatus(jsonObject.getString("auth_code"));
                    if (object.has("last_ride")) {
                        RideDetails rideDetails = baseActivity.parseRideDetails(object.getJSONObject("last_ride"));
                        if (rideDetails != null) {
                            intent.putExtra("rideDetail", rideDetails);
                        }

                    }
                    baseActivity.startActivity(intent);
                    baseActivity.finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    if (jsonObject.has("error") && jsonObject.getString("error").equalsIgnoreCase(""))
                        showToast(jsonObject.getString("error"));
                    else {
                        try {
                            if (!jsonObject.getString("error").equalsIgnoreCase(""))
                                showToast(jsonObject.getString("error"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (controller.equalsIgnoreCase("user") && action.equalsIgnoreCase("facebook-login")) {
            if (status) {
                try {
                    Intent intent = new Intent(baseActivity, MainActivity.class);
                    JSONObject object = jsonObject.getJSONObject("detail");
                    ProfileData profileData = baseActivity.parseProfileData(object);
                    baseActivity.store.setBoolean("isFb", true);
                    baseActivity.saveProfileDataInPrefStore(profileData);
                    baseActivity.syncManager.setLoginStatus(jsonObject.getString("auth_code"));
                    if (object.has("sepomex") && object.get("sepomex") instanceof JSONObject) {
                        if (object.has("last_ride")) {
                            RideDetails rideDetails = baseActivity.parseRideDetails(object.getJSONObject("last_ride"));
                            if (rideDetails != null) {
                                intent.putExtra("rideDetail", rideDetails);
                            }

                        }
                        baseActivity.startActivity(intent);
                        baseActivity.finish();
                    } else {
                        openAddressDialog(requestParams);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                showToast(jsonObject.optString("error"));

                if(jsonObject.optString(MyResponse.RESPONSE_ERROR_NAME).equals(MyResponse.ERROR_EMAIL_ALREADY_REGISTERED)){
                    Intent intent = new Intent(baseActivity, LoginSignUpActivity.class);
                    baseActivity.startActivity(intent);
                    baseActivity.finish();
                }else{
                    if (!jsonObject.optBoolean("country"))
                    {
                        openCountrySelectionDialog(requestParams);
                    }
                }
            }
        } else if (controller.equals("user") && action.equals("country")) {
            if (status) {
                try {
                    countryDatas.clear();
                    JSONArray country = jsonObject.getJSONArray("country");
                    arrCountry = new String[country.length()];
                    for (int i = 0; i < country.length(); i++) {
                        JSONObject countryObjct = country.getJSONObject(i);
                        CountryData countryData = new CountryData(Parcel.obtain());
                        countryData.id = countryObjct.getInt("id");
                        countryData.title = countryObjct.getString("title");
                        countryData.telephone_code = countryObjct.getString("telephone_code");
                        countryData.flag = countryObjct.getString("flag");
                        countryData.country_code = countryObjct.getString("country_code");
                        countryData.currency_symbol = countryObjct.getString("currency_symbol");
                        if (countryObjct.has("car_type")) {
                            JSONArray car_type = countryObjct.getJSONArray("car_type");
                            if (car_type.length() != 0) {
                                int[] carTypes = new int[car_type.length()];
                                for (int j = 0; j < car_type.length(); j++) {
                                    carTypes[j] = (int) car_type.get(j);
                                    countryData.arr_car_type = carTypes;
                                }
                                countryDatas.add(countryData);
                                arrCountry[i] = countryData.title;
                            }
                        }


                    }
                    spinnerAdapter = new CustomSpinnerAdapter(baseActivity, countryDatas);
                    countrySP.setAdapter(spinnerAdapter);
                    if (countryDatas.size() != 0) {
                        if (countryID == 11) {
                            for (int i = 0; i < countryDatas.size(); i++) {
                                if (countryDatas.get(i).telephone_code.equals("+1")) {
                                    countrySP.setSelection(i);
                                    break;
                                }

                            }
                        } else {
                            for (int i = 0; i < countryDatas.size(); i++) {
                                if (countryDatas.get(i).id == countryID) {
                                    countrySP.setSelection(i);
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        } else if (controller.equals("user") && action.equals("address-search")) {
            if (status) {
                JSONArray jsonArray = jsonObject.optJSONArray("details");
                totalPages = jsonObject.optInt("totalPage");
                if (totalPages >= pagecount) {
                    pagecount++;
                }
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.optJSONObject(i);
                    SearchSignUp searchSignUp = new SearchSignUp(Parcel.obtain());
                    searchSignUp.id = object.optInt("id");
                    searchSignUp.settlement = object.optString("settlement");
                    searchSignUp.municipality = object.optString("municipality");
                    searchSignUp.state = object.optString("state");
                    searchSignUp.zipcode = object.optInt("zipcode");
                    searchSignUps.add(searchSignUp);
                }
                if (postalCodeAdapter != null) {
                    if (!dialog.isShowing())
                        dialog.show();
                    postalCodeAdapter.notifyDataSetChanged();
                } else {
                    showPostalCodeDialog();
                }
            } else {
                if (pagecount > 0) {
                    showToast(baseActivity.getString(R.string.no_more_zipcodes));
                } else {
                    showToast(jsonObject.optString("error"));
                }
            }

        } else if (controller.equals("user") && action.equals("passenger-update")) {
            if (status) {
                showToast(baseActivity.getString(R.string.address_added_successfully));
                Intent intent = new Intent(baseActivity, MainActivity.class);
                baseActivity.startActivity(intent);
                baseActivity.finish();

            } else
                showToast(jsonObject.optString("error"));
        }
    }

    private void showPostalCodeDialog() {
        if (searchSignUps != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(baseActivity);
            builder.setTitle(getString(R.string.select_your_address));

            LayoutInflater inflater = LayoutInflater.from(baseActivity);
            View content = inflater.inflate(R.layout.postaldialog, null);
            builder.setView(content);
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.cancel), new OnClicKKDialog(searchSignUps) {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    searchSignUps.clear();
                    dialog.dismiss();
                }
            });
            ListView postalLV = (ListView) content.findViewById(R.id.postalLV);

            postalCodeAdapter = new PostalCodeAdapter(baseActivity, searchSignUps, this);
            postalLV.setAdapter(postalCodeAdapter);
            postalLV.setOnItemClickListener(new OnClicKK(searchSignUps) {
                @SuppressLint("SetTextI18n")
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    pagecount = 0;
                    postalId = searchSignUps.get(position).id;
                    postalET.setText("" + searchSignUps.get(position).zipcode);
                    colonyET.setText("" + searchSignUps.get(position).settlement);
                    municipalityET.setText("" + searchSignUps.get(position).municipality);
                    stateET.setText("" + searchSignUps.get(position).state);
                    dialog.dismiss();
                }
            });
            dialog = builder.create();
            dialog.show();
        }
    }

    @Override
    public void onFacebookLoginDone(JSONObject object, GraphResponse response) {
        Util.log(TAG,"onFacebookLoginDone: object " + object + " response :" + response);
        if (response != null) {
            try {
                requestParams = setCommonParams(object);
                requestParams.put("User[contact_no]", "");
                requestParams.put("User[country_id]", "");
                baseActivity.syncManager.sendToServer(Const.API_FACEBOOK, requestParams, LoginFragment.this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public RequestParams setCommonParams(JSONObject object) {
        RequestParams params = new RequestParams();
        params.put("User[first_name]", object.optString("first_name"));
        params.put("User[last_name]", object.optString("last_name"));
        params.put("User[email]", object.optString("email").isEmpty() ? object.optString("id").concat("@facebook.com") : object.optString("email"));
        params.put("User[userId]", object.optString("id"));
        params.put("User[provider]", "Facebook");
        params.put("img_url", object.optJSONObject("picture").optJSONObject("data").optString("url"));
        String token = FirebaseInstanceId.getInstance().getToken();
        if (token != null)
            params.put("User[device_token]", token);
        else
            params.put("User[device_token]", baseActivity.getUniqueDeviceId());
        params.put("User[device_type]", "1");
        return params;
    }

    private void openCountrySelectionDialog(RequestParams params) {

        Dialog dialog = new Dialog(baseActivity, R.style.Theme_AppCompat_Dialog);
        dialog.setContentView(R.layout.dialog_country);
        dialog.setCancelable(false);


        Button continueBT = (Button) dialog.findViewById(R.id.continueBT);
        EditText phoneNumberET = (EditText) dialog.findViewById(R.id.phoneNumberET);
        countrySP = (Spinner) dialog.findViewById(R.id.countrySP);
        getCountryCode();
        countrySP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryData dataItem = (CountryData) parent.getItemAtPosition(position);
                if (!(((CountryData) countrySP.getSelectedItem()).id == 0)) {
                    countryID = dataItem.id;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        continueBT.setTag(R.string.params, params);
        continueBT.setTag(R.string.phone, phoneNumberET);
        continueBT.setTag(R.string.spinner, countrySP);
        continueBT.setTag(R.string.dialog, dialog);
        continueBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestParams params1 = (RequestParams) v.getTag(R.string.params);
                EditText phone = (EditText) v.getTag(R.string.phone);
                Spinner spinner = (Spinner) v.getTag(R.string.spinner);
                Dialog d = (Dialog) v.getTag(R.string.dialog);
                if (validations(phone, spinner)) {
                    params1.put("User[contact_no]", phone.getText().toString().replace(" ", ""));
                    params1.put("User[country_id]", countryID);
                    baseActivity.syncManager.sendToServer(Const.API_FACEBOOK, params1, LoginFragment.this);
                    d.dismiss();
                }
            }

            private boolean validations(EditText phoneET, Spinner spinner) {
                boolean isvalid = false;
                if (phoneET.getText().toString().trim().equals(""))
                    showToast(getString(R.string.please_enter_phone_number));
                else isvalid = true;
                return isvalid;
            }
        });

        if (dialog.isShowing()) {
            dialog.dismiss();
            dialog.show();
        } else
            dialog.show();

    }

    private void openAddressDialog(RequestParams params) {
        Dialog dialog = new Dialog(baseActivity, R.style.Theme_AppCompat_Dialog);
        dialog.setContentView(R.layout.dialog_address);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);


        Button continueBT = (Button) dialog.findViewById(R.id.continueBT);
        postalET = (EditText) dialog.findViewById(R.id.postalET);
        colonyET = (EditText) dialog.findViewById(R.id.colonyET);
        municipalityET = (EditText) dialog.findViewById(R.id.municipalityET);
        stateET = (EditText) dialog.findViewById(R.id.stateET);
        searchBT = (Button) dialog.findViewById(R.id.searchBT);
        searchBT.setOnClickListener(this);
        continueBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (postalET.getText().toString().isEmpty()) {
                    showToast("");
                } else {
                    hitAddressAddApi();
                }

            }
        });

        if (dialog.isShowing()) {
            dialog.dismiss();
            dialog.show();
        } else
            dialog.show();


    }

    private void hitAddressAddApi() {
        RequestParams params = new RequestParams();
        params.put("User[sepomex_id]", postalId);
        baseActivity.syncManager.sendToServer("api/user/passenger-update", params, this);
    }


    private void getCountryCode() {
        baseActivity.syncManager.sendToServer(Const.API_USER_COUNTRY, null, this);
    }


    @Override
    public void onGPlusLoginDone(GoogleSignInAccount currentPerson) {

    }

    abstract class OnClicKKDialog implements DialogInterface.OnClickListener {
        ArrayList<SearchSignUp> searchSignUps;

        OnClicKKDialog(ArrayList<SearchSignUp> searchSignUps) {
            this.searchSignUps = searchSignUps;

        }
    }

    abstract class OnClicKK implements AdapterView.OnItemClickListener {
        ArrayList<SearchSignUp> searchSignUps;

        OnClicKK(ArrayList<SearchSignUp> searchSignUps) {
            this.searchSignUps = searchSignUps;

        }
    }
}

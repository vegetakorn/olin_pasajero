package com.olin.passenger.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pkmmte.view.CircularImageView;
import com.olin.passenger.R;
import com.olin.passenger.data.RideDetails;
import com.olin.passenger.utils.Util;

/**
 * Created by TOXSL\jagmel.singh on 29/5/17.
 * Detalle del perfil del conductor
 */

public class DriverProfileFragment extends BaseFragment {
    private final String TAG = "DriverProfileFragment";

    private View view;
    private RideDetails rideDetails;
    private TextView modelTV;
    private TextView nameTV, vehicleMakeTV;
    private TextView phoneNumberTV,tvPickUpLocation,tvDestination;
    private CircularImageView profileCIV;
    private CircularImageView carCIV;
    private TextView vehicleRegTV;
    private TextView tvCompany;
    private ImageView ivPictureCar;

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_driver_profile, null);
        nameTV = (TextView) view.findViewById(R.id.nameTV);
        phoneNumberTV = (TextView) view.findViewById(R.id.phoneNumberTV);
        tvPickUpLocation = (TextView) view.findViewById(R.id.tvPickUpLocation);
        tvDestination = (TextView) view.findViewById(R.id.tvDestination);
        modelTV = (TextView) view.findViewById(R.id.modelTV);
        vehicleMakeTV = (TextView) view.findViewById(R.id.vehicleMakeTV);
        vehicleRegTV = (TextView) view.findViewById(R.id.vehicleRegTV);
        tvCompany = (TextView) view.findViewById(R.id.tvCompany);
        profileCIV = (CircularImageView) view.findViewById(R.id.profileCIV);
        ivPictureCar = view.findViewById(R.id.ivPictureCar);
        carCIV = (CircularImageView) view.findViewById(R.id.carCIV);
        if (getArguments() != null && getArguments().containsKey("rideDetails")) {
            rideDetails = getArguments().getParcelable("rideDetails");
            Util.log(TAG,rideDetails.driverData.image_file);
            Util.log(TAG,rideDetails.driverData.vehicle_image);
            if (rideDetails != null) {
                nameTV.setText(rideDetails.driverData.first_name + " " + rideDetails.driverData.last_name);
                modelTV.setText(rideDetails.vehicle.model);
                vehicleRegTV.setText(rideDetails.vehicle.registration_no);
                phoneNumberTV.setText(rideDetails.driverData.countryData.telephone_code+" "+rideDetails.driverData.contact_no);
                vehicleMakeTV.setText(rideDetails.vehicle.car_make);
                tvPickUpLocation.setText(rideDetails.location);
                tvDestination.setText(rideDetails.destination);
                tvCompany.setText(R.string.not_available);
                if(rideDetails.driverData != null && rideDetails.driverData.driver != null && rideDetails.driverData.driver.company != null){
                    tvCompany.setText(rideDetails.driverData.driver.company.name);
                }
                try {
                    baseActivity.picasso.load(rideDetails.driverData.image_file).error(R.mipmap.default_avatar).into(profileCIV);
                    baseActivity.picasso.load(rideDetails.driverData.vehicle_image).error(R.mipmap.default_avatar).into(carCIV);
                    baseActivity.picasso.load(rideDetails.driverData.vehicle_image).error(R.mipmap.ic_car_big).into(ivPictureCar);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return view;
    }
}

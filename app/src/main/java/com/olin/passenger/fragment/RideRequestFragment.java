package com.olin.passenger.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.olin.passenger.R;
import com.olin.passenger.activity.MainActivity;
import com.olin.passenger.data.JourneyData;
import com.olin.passenger.data.RideDetails;
import com.olin.passenger.utils.Const;
import com.olin.passenger.utils.GoogleApisHandle;

import org.json.JSONObject;


/**
 * Created by TOXSL\visha.sehgal on 17/1/17.
 */

public class RideRequestFragment extends BaseFragment implements OnMapReadyCallback, GoogleApisHandle.OnPolyLineReceived {
    private View view;
    private GoogleMap googlemap;
    private Marker pickupMarker;
    private Marker destinationMarker;
    private TextView pickUpLocationTV, dropOffTV;
    private Button cancelRideBT;
    JourneyData journeyData;
    private long times_in_millies = 60 * 1000;
    private int rideId;
    private CountDownTimer cTimer;
    private RideDetails rideDetails;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_ride_request_passenger, container, false);
            ((MainActivity) getActivity()).setActionBarTitle(baseActivity.getString(R.string.app_name), false);
            initUi();
        }
        return view;
    }

    private void initUi() {
        pickUpLocationTV = (TextView) view.findViewById(R.id.pickUpLocTV);
        dropOffTV = (TextView) view.findViewById(R.id.dropOffTV);
        pickUpLocationTV.setSelected(true);
        dropOffTV.setSelected(true);
        cancelRideBT = (Button) view.findViewById(R.id.cancelRideBT);
        cancelRideBT.setOnClickListener(this);
        if (getArguments() != null && getArguments().containsKey("journeyData")) {
            Bundle bundle = getArguments();
            journeyData = bundle.getParcelable("journeyData");
            if (journeyData != null) {
                rideId = journeyData.id;
                pickUpLocationTV.setText(journeyData.location);
                dropOffTV.setText(journeyData.destination);
                initilizeMap();
            }
        } else if (getArguments() != null && getArguments().containsKey("ride_id")) {
            rideId = getArguments().getInt("ride_id");

        }

    }

    @Override
    public void onResume() {
        getRideDetail(rideId);
        super.onResume();
    }

    public void startTimer() {
        cTimer = new CountDownTimer(times_in_millies, 1000) {
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                showToast(baseActivity.getString(R.string.no_driver_found));
                cancelTimer();
                cancelRide();

            }
        };
        cTimer.start();
    }

    void cancelTimer() {
        if (cTimer != null) cTimer.cancel();
    }

    private void getRideDetail(int ride_id) {
        baseActivity.syncManager.sendToServer(Const.API_RIDE_DETAIL + "?id=" + ride_id, null, this);
    }

    private void initilizeMap() {
        if (googlemap == null) {
            SupportMapFragment map = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
            map.getMapAsync(this);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancelRideBT:
                askToCancelRide();
                break;
        }
    }

    private void askToCancelRide() {


        AlertDialog.Builder builder = new AlertDialog.Builder(baseActivity);
        builder.setMessage(baseActivity.getString(R.string.do_you_want_to_cancel_this_ride));
        builder.setCancelable(false);
        builder.setPositiveButton(
                baseActivity.getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        cancelRide();
                        dialog.cancel();
                    }
                });

        builder.setNegativeButton(
                baseActivity.getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void cancelRide() {
        baseActivity.syncManager.sendToServer(Const.API_RIDE_CANCELED + rideId, null, this);
    }


    private void setPickupMarker(LatLng pickupLatLng) {
        if (pickupMarker == null)
            pickupMarker = googlemap.addMarker(new MarkerOptions()
                    .position(pickupLatLng).draggable(false)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_pin_green)));
        else {
            pickupMarker.setPosition(pickupLatLng);

        }
    }

    private void setDestinationMarker(LatLng dropoffLatLng, String time, double distance, String line) {
        if (destinationMarker == null) {
            MarkerOptions options = new MarkerOptions()
                    .position(dropoffLatLng).draggable(false)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_pin_red));
            if (time != null && !time.isEmpty()) {
                options.title(baseActivity.getString(R.string.estimate_time_distance));
                options.snippet("Time : " + time + " & " + "distance :" + line);
            }
            destinationMarker = googlemap.addMarker(options);
            destinationMarker.showInfoWindow();

        } else {
            if (time != null && !time.isEmpty()) {
                destinationMarker.setTitle(baseActivity.getString(R.string.estimate_time_distance));
                destinationMarker.setSnippet("Time : " + time + " & " + "distance :" + line);
            }
            destinationMarker.setPosition(dropoffLatLng);
            destinationMarker.showInfoWindow();

        }
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("ride") && action.equals("canceled")) {
            if (status) {
                cancelTimer();
                baseActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new HomeFragment())
                        .commitAllowingStateLoss();
                log(jsonObject.optString("detail"));
            } else {
                showToast(jsonObject.optString("error"));
            }
        } else if (controller.equals("ride") && action.equals("detail")) {
            if (status) {
                rideDetails = baseActivity.parseRideDetails(jsonObject.optJSONObject("detail"));
                pickUpLocationTV.setText(rideDetails.location);
                dropOffTV.setText(rideDetails.destination);
                gotoCurrentState(rideDetails);
            }

        } else {
            showToast(jsonObject.optString("error"));
        }

    }

    private void gotoCurrentState(RideDetails journeyDetail) {
        switch (rideDetails.state_id) {
            case Const.STATE_ACCEPTED:
                ((MainActivity) baseActivity).gotoStartRideFrag(journeyDetail.id);
                baseActivity.cancelNotification(Const.ALL_NOTI);
                break;
            case Const.STATE_DRIVER_ARRIVED:
                ((MainActivity) baseActivity).gotoStartRideFrag(journeyDetail.id);
                baseActivity.cancelNotification(Const.ALL_NOTI);
                break;
            case Const.STATE_STARTED:
                ((MainActivity) baseActivity).gotoStartRideFrag(journeyDetail.id);
                baseActivity.cancelNotification(Const.ALL_NOTI);
                break;
            case Const.STATE_COMPLETED:
                ((MainActivity) baseActivity).gotoPaymentFrag(journeyDetail.id);
                baseActivity.cancelNotification(Const.ALL_NOTI);
                break;
            case Const.STATE_CANCELLED:
                ((MainActivity) baseActivity).goToHomeFragment();
                break;
            case Const.STATE_PAID:
                ((MainActivity) baseActivity).goToHomeFragment();
                break;

        }
    }

    @Override
    public void onDestroy() {
        cancelTimer();
        super.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.getUiSettings().setTiltGesturesEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(false);
        this.googlemap = googleMap;
        googleApiHandle.setPolyLineReceivedListener(this);
        if (getArguments() != null && getArguments().containsKey("journeyData")) {
            googleApiHandle.getDirectionsUrl(new LatLng(journeyData.location_lat, journeyData.location_long), new LatLng(journeyData.destination_lat, journeyData.destination_long), googleMap);
        } else if (getArguments() != null && getArguments().containsKey("ride_id")) {
            googleApiHandle.getDirectionsUrl(new LatLng(rideDetails.location_lat, rideDetails.location_long), new LatLng(rideDetails.destination_lat, rideDetails.destination_long), googleMap);
        }


    }

    @Override
    public void onPolyLineReceived(LatLng origin, LatLng destination, GoogleMap routeMap, Polyline polyline, double distance, String time, String line) {
        setPickupMarker(origin);
        setDestinationMarker(destination, time, distance, line);
    }
}

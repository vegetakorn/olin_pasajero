package com.olin.passenger.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;
import com.olin.passenger.BuildConfig;
import com.olin.passenger.R;
import com.olin.passenger.activity.MainActivity;
import com.olin.passenger.data.RideDetails;
import com.olin.passenger.utils.Const;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by TOXSL\ankush.walia on 23/1/17.
 * Vista para calificar al conductor
 */
public class DriverRateFragment extends BaseFragment {
    private View view;
    private CircularImageView profileCIV;
    private TextView driverNameTV,vehicleRegTV;
    private EditText reviewET;
    private RatingBar ratingRB;
    private Button submitBT;
    private RideDetails rideDetails;

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_driver_review, null);
        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.rate__review), false);

        profileCIV = (CircularImageView) view.findViewById(R.id.profileCIV);
        driverNameTV = (TextView) view.findViewById(R.id.driverNameTV);
        vehicleRegTV = (TextView) view.findViewById(R.id.vehicleRegTV);
        reviewET = (EditText) view.findViewById(R.id.reviewET);
        ratingRB = (RatingBar) view.findViewById(R.id.ratingRB);
        submitBT = (Button) view.findViewById(R.id.submitBT);
        submitBT.setOnClickListener(this);
        if (getArguments() != null && getArguments().containsKey("rideDetails")) {
            rideDetails = getArguments().getParcelable("rideDetails");
            try {
                if (rideDetails != null) {
                    if (rideDetails.driverData.image_file != null && !rideDetails.driverData.image_file.equals("") && !rideDetails.driverData.image_file.isEmpty())
                        Picasso.with(baseActivity).load(rideDetails.driverData.image_file).placeholder(R.mipmap.default_avatar).into(profileCIV);
                    driverNameTV.setText(rideDetails.driverData.first_name + " " + rideDetails.driverData.last_name);
                    //vehicleRegTV.setText(rideDetails.vehicle.registration_no);
                    if(rideDetails.driverData.driver != null && rideDetails.driverData.driver.company != null){
                        vehicleRegTV.setText(rideDetails.driverData.driver.company+"");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (BuildConfig.DEBUG) {
            ratingRB.setRating(5);
            reviewET.setText(getString(R.string.great_journey));
        }
        return view;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.submitBT:
                if (validations())
                    submitRatingandReview();
                break;
        }
    }

    private boolean validations() {
        if (ratingRB.getRating() == 0)
            showToast(getString(R.string.pleas_rate_the_ride));
        else if (reviewET.getText().toString().isEmpty())
            showToast(getString(R.string.please_provide_some_feedback));
        else return true;
        return false;
    }


    private void submitRatingandReview() {
        RequestParams params = new RequestParams();
        params.put("Review[rate]", ratingRB.getRating());
        params.put("Review[comment]", reviewET.getText().toString().trim());
        baseActivity.syncManager.sendToServer(Const.API_REVIEW_ADD + "?id=" + rideDetails.id, params, this);
    }

    private void showLongToast(String msg) {
        Toast.makeText(baseActivity, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("ride") && action.equals("paid")) {
            if (status) {
                try {
                    showToast(jsonObject.getString("message"));
                    baseActivity.getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.container, new HomeFragment())
                            .commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    showToast(jsonObject.getString("error"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (controller.equals("review") && action.equals("add")) {
            if (status) {
                showLongToast(baseActivity.getString(R.string.thanks_for_providing_your_feedback));
                baseActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new HomeFragment())
                        .commit();
            } else {
                showToast(jsonObject.optString("error"));
                baseActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new HomeFragment())
                        .commit();
            }
        }
    }
}

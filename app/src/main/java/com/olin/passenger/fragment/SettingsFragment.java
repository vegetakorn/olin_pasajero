package com.olin.passenger.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.olin.passenger.R;
import com.olin.passenger.activity.MainActivity;
import com.olin.passenger.utils.Const;
import com.olin.passenger.utils.PrefStore;

import org.json.JSONObject;


/**
 * Created by TOXSL\chirag.tyagi on 17/3/17.
 */

public class SettingsFragment extends BaseFragment {
    private View view;
    private TextView changeTV, logoutTV;
    private LinearLayout langOptLL;
    private RadioButton spanishRB, englishRB;
    private RadioGroup langRG;
    private TextView termsAndPrivacyTV;
    private TextView aboutUsTV;
    private PrefStore store;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings, container, false);
        ((MainActivity) baseActivity).setActionBarTitle(getString(R.string.settings), true);
        initUI();
        return view;
    }

    private void initUI() {
        changeTV = (TextView) view.findViewById(R.id.changeTV);
        termsAndPrivacyTV = (TextView) view.findViewById(R.id.termsAndPrivacyTV);
        aboutUsTV = (TextView) view.findViewById(R.id.aboutUsTV);
        langOptLL = (LinearLayout) view.findViewById(R.id.langOptLL);
        spanishRB = (RadioButton) view.findViewById(R.id.spanishRB);
        englishRB = (RadioButton) view.findViewById(R.id.englishRB);
        langRG = (RadioGroup) view.findViewById(R.id.langRG);
        store = new PrefStore(baseActivity);
        setRadioButton();
        langRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                Intent intent = new Intent(baseActivity, MainActivity.class);
                switch (checkedId) {
                    case R.id.englishRB:
                        store.saveString("language", "English");
                        baseActivity.checkLanguage();
                        startActivity(intent);
                        baseActivity.finish();
                        break;
                    case R.id.spanishRB:
                        store.saveString("language", "Spanish");
                        baseActivity.checkLanguage();
                        startActivity(intent);
                        baseActivity.finish();
                        break;

                }

            }
        });


        changeTV.setOnClickListener(this);
        aboutUsTV.setOnClickListener(this);
        termsAndPrivacyTV.setOnClickListener(this);

    }

    private void setRadioButton() {
        if (store.getString("language") != null && !store.getString("language").equalsIgnoreCase("")) {
            if (store.getString("language").equals("English")) {
                englishRB.setChecked(true);
            } else if (store.getString("language").equals("Spanish")) {
                spanishRB.setChecked(true);
            }

        } else {
            spanishRB.setChecked(true);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.changeTV:
                gotoChangePassword();
                break;
            case R.id.termsAndPrivacyTV:
                showTerms();
                break;
            case R.id.aboutUsTV:
                showAboutUs();
                break;

        }

    }

    private void showAboutUs() {
        baseActivity.syncManager.sendToServer(Const.API_PAGE_GET + "?type_id=" + Const.ABOUT_PAGE, null, this);
    }

    private void showTerms() {
        baseActivity.syncManager.sendToServer(Const.API_PAGE_GET + "?type_id=" + Const.TC_CUSTOMER_PAGE, null, this);
    }

    public void showTermAndAboutDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(baseActivity, R.style.DialogTheme)
                .setTitle(title)
                .setMessage(Html.fromHtml(message))
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();

    }


    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("page") && action.equals("get")) {
            if (status) {
                showTermAndAboutDialog(jsonObject.optJSONObject("list").optString("title"), jsonObject.optJSONObject("list").optString("description"));
            } else {
                if (jsonObject.has("error")) {
                    showToast(jsonObject.optString("error"));
                }
            }
        }
    }

    private void gotoChangePassword() {
        Fragment fragment = new ChangePasswordFragment();
        baseActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }


}

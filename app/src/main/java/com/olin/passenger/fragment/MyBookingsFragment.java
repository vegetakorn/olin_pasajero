package com.olin.passenger.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.olin.passenger.R;
import com.olin.passenger.activity.MainActivity;
import com.olin.passenger.adapter.BookingsAdapter;
import com.olin.passenger.data.RideDetails;
import com.olin.passenger.utils.Const;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by TOXSL\chirag.tyagi on 17/3/17.
 */

public class MyBookingsFragment extends BaseFragment {
    private View view;
    private TextView emptyTV, logoutTV;
    private LinearLayout langOptLL;
    private ListView reservationsLV;
    private BookingsAdapter adapter;
    ArrayList<RideDetails> rides = new ArrayList<>();
    private int pageCount = 1;
    private int currentPage = 0;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_bookings, container, false);
        ((MainActivity) baseActivity).setActionBarTitle(getString(R.string.my_bookings), true);
        initUI();
        hitRideHistoryApi();
        return view;
    }

    public void hitRideHistoryApi() {
        if (currentPage + 1 <= pageCount)
            baseActivity.syncManager.sendToServer(Const.API_RIDE_USER_RIDE_HISTORY + "?page=" + currentPage, null, this);
    }

    private void initUI() {
        emptyTV = (TextView) view.findViewById(R.id.emptyTV);
        reservationsLV = (ListView) view.findViewById(R.id.reservationsLV);
        reservationsLV.setEmptyView(emptyTV);
        adapter = new BookingsAdapter(baseActivity, rides, this);
        reservationsLV.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

        }

    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equalsIgnoreCase("ride") && action.equalsIgnoreCase("user-ride-history")) {
            if (status) {
                currentPage++;
                if (jsonObject.has("totalPage")) {
                    pageCount = jsonObject.optInt("totalPage");
                }
                JSONArray list = jsonObject.optJSONArray("list");

                for (int i = 0; i < list.length(); i++) {
                    RideDetails rideDetail = baseActivity.parseRideDetails(list.optJSONObject(i));

                    rides.add(rideDetail);
                }
                adapter.notifyDataSetChanged();
            } else {
                showToast(jsonObject.optString("error"));
            }
        } else if (controller.equalsIgnoreCase("complain") && action.equalsIgnoreCase("add")) {
            showToast(getString(R.string.complain_submitted));
        }
    }

    public void sendComplain(int id, String complain) {
        RequestParams params = new RequestParams();
        params.add("Complain[comment]", complain);
        baseActivity.syncManager.sendToServer(Const.API_COMPLAIN_ADD + "?ride_id=" + id, params, this);
    }
}

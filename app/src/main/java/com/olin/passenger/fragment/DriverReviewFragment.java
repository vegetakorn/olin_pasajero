package com.olin.passenger.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Parcel;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.pkmmte.view.CircularImageView;
import com.olin.passenger.R;
import com.olin.passenger.activity.BaseActivity;
import com.olin.passenger.adapter.DriverReviewAdapter;
import com.olin.passenger.data.DriverReviewData;
import com.olin.passenger.data.RideDetails;
import com.olin.passenger.utils.Const;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by TOXSL\jagmel.singh on 29/5/17.
 * Calificaciones del conductor
 */

public class DriverReviewFragment extends BaseFragment {
    private View view;
    private CircularImageView profileIV;
    private TextView driverNameTV;
    private RatingBar ratingRB;
    private ListView reviewsLV;
    private RideDetails rideDetails;
    int driverID;
    private ArrayList<DriverReviewData> reviewDatas = new ArrayList<>();
    private DriverReviewAdapter adapter;
    private int currentPage = 0;
    private int pageCount = 1;

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_reviews, null);
        profileIV = (CircularImageView) view.findViewById(R.id.profileIV);
        driverNameTV = (TextView) view.findViewById(R.id.driverNameTV);
        ratingRB = (RatingBar) view.findViewById(R.id.ratingRB);
        reviewsLV = (ListView) view.findViewById(R.id.reviewsLV);
        if (getArguments() != null && getArguments().containsKey("rideDetails")) {
            rideDetails = getArguments().getParcelable("rideDetails");
            assert rideDetails != null;
            driverNameTV.setText(rideDetails.driverData.first_name + " " + rideDetails.driverData.last_name);
            ratingRB.setRating(BaseActivity.round((float) rideDetails.driverData.driver_rating, 1));
            try {
                baseActivity.picasso.load(rideDetails.driverData.image_file).placeholder(R.mipmap.default_avatar).error(R.mipmap.default_avatar).into(profileIV);
            } catch (Exception e) {
                e.printStackTrace();
            }
            driverID = rideDetails.driverData.id;
            getReviews();
            adapter = new DriverReviewAdapter(baseActivity, reviewDatas, this);
            reviewsLV.setAdapter(adapter);
        }

        return view;
    }



    public void getReviews() {
        if (currentPage < pageCount)
            baseActivity.syncManager.sendToServer(Const.API_REVIEW_GET + "?driver_id=" + driverID + "&page=" + currentPage, null, this);
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equalsIgnoreCase("review") && action.equalsIgnoreCase("get")) {
            if (status) {
                pageCount = jsonObject.optInt("pageCount");
                currentPage++;
                JSONArray list = jsonObject.optJSONArray("list");
                for (int i = 0; i < list.length(); i++) {
                    DriverReviewData reviewData = new DriverReviewData(Parcel.obtain());
                    JSONObject item = list.optJSONObject(i);
                    reviewData.rate = (float) item.optDouble("rate");
                    reviewData.comment = item.optString("comment");
                    reviewData.created_on = item.optString("created_on");
                    if (item.has("createdBy")) {
                        JSONObject passenger = item.optJSONObject("createdBy");
                        reviewData.first_name = passenger.optString("first_name");
                        reviewData.last_name = passenger.optString("last_name");
                        reviewData.image_file = passenger.optString("image_file");
                    }
                    reviewDatas.add(reviewData);
                    adapter.notifyDataSetChanged();
                }

            }
        }
    }
}

package com.olin.passenger.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcel;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.olin.passenger.R;
import com.olin.passenger.activity.MainActivity;
import com.olin.passenger.adapter.CouponAdapter;
import com.olin.passenger.data.CouponsDetails;
import com.olin.passenger.utils.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by TOXSL\ankan.tiwari on 27/9/17.
 */

public class CouponCodeFragment extends BaseFragment {

    ListView couponslistLV;
    ArrayList<CouponsDetails> coupons = new ArrayList<>();
    private boolean is_applied;
    private View view;
    private int pageCount = 1;
    private int currentPage = 0;
    private CouponAdapter adapter;
    private JSONArray list;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_couponcode, container, false);
        ((MainActivity) baseActivity).setActionBarTitle(getString(R.string.coupons), true);
        initUI();
        return view;
    }

    public void hitCouponApi() {

        baseActivity.syncManager.sendToServer(Const.API_COUPONS_LIST + "?page=" + currentPage, null, this);
    }

    private void initUI() {
        couponslistLV = (ListView) view.findViewById(R.id.couponcodeLV);
        hitCouponApi();
    }


    public void hitApplyCouponApi(int coupon_id, TextView appliedTV) {
        if (is_applied) {
            AlertDialog.Builder builder = new AlertDialog.Builder(baseActivity);
            builder.setMessage(baseActivity.getString(R.string.do_you_want_to_apply_this_coupon))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.yes), new OnClick(coupon_id, appliedTV) {
                        public void onClick(DialogInterface dialog, int id) {
                            appliedTV.setText(getString(R.string.coupon_applied));
                            dialog.dismiss();
                            baseActivity.syncManager.sendToServer(Const.API_APPLY_COUPON + "?id=" + coupon_id, null, CouponCodeFragment.this);
                        }
                    })
                    .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            builder.create().show();
        }
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equalsIgnoreCase("coupon") && action.equalsIgnoreCase("index")) {
            coupons.clear();
            if (jsonObject.has("list")) {
                list = jsonObject.optJSONArray("list");
                if (status) {
                    is_applied = true;
                    currentPage++;
                    if (jsonObject.has("totalPage")) {
                        pageCount = jsonObject.optInt("totalPage");
                    }
                    if (list != null || list.length() > 0) {
                        fetchingCoupan(list);
                    } else {
                        showToast(baseActivity.getString(R.string.no_coupans_available));
                    }

                } else {
                    is_applied = false;

                    if (list != null || list.length() > 0) {
                        fetchingCoupan(list);
                    } else {
                        showToast(baseActivity.getString(R.string.no_coupans_available));
                    }
                    setAdapter();
                }
            } else {
                showToast(baseActivity.getString(R.string.no_coupans_available));
            }
        } else if (controller.equalsIgnoreCase("coupon") && action.equalsIgnoreCase("apply")) {

            if (status) {
                coupons.clear();
                showToast(baseActivity.getString(R.string.your_coupon_is_successfully_applied));
                CouponsDetails coupondetails = new CouponsDetails(Parcel.obtain());
                try {
                    JSONObject object = jsonObject.getJSONObject("detail");
                    is_applied = false;
                    coupondetails.id = object.getInt("id");
                    coupondetails.Coupon_code = object.getString("coupon_code");
                    coupondetails.Description = object.getString("description");
                    coupondetails.discount = object.getInt("discount");
                    coupondetails.startime = object.getString("start_date");
                    coupondetails.endtime = object.getString("end_date");
                    coupons.add(coupondetails);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                setAdapter();
            } else {
                try {
                    if (jsonObject.has("error"))
                    showToast(jsonObject.getString("error"));
                    else {
                        showToast(baseActivity.getString(R.string.your_coupon_is_already_applied));
                    }
                    setAdapter();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    private void fetchingCoupan(JSONArray list) {
        try {

            for (int i = 0; i < list.length(); i++) {
                JSONObject detail = list.getJSONObject(i);
                CouponsDetails coupondetails = new CouponsDetails(Parcel.obtain());
                if (detail != null) {
                    coupondetails.id = detail.optInt("id");
                    if (detail.has("pageCount")) {
                        coupondetails.pageCount = detail.optInt("pageCount");
                    }
                    coupondetails.id = detail.optInt("id");
                    coupondetails.coupon_id = detail.optInt("coupon_id");
                    coupondetails.Coupon_code = detail.optString("coupon_code");
                    coupondetails.Description = detail.optString("description");
                    coupondetails.discount = detail.optInt("discount");
                    coupondetails.startime = detail.optString("start_date");
                    coupondetails.endtime = detail.optString("end_date");
                }

                coupons.add(coupondetails);
            }
            setAdapter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        if (adapter == null) {
            adapter = new CouponAdapter(baseActivity, coupons, this, is_applied);
            couponslistLV.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }

    }


    abstract class OnClick implements DialogInterface.OnClickListener {
        int coupon_id;
        TextView appliedTV;

        OnClick(int coupon_id, TextView appliedTV) {
            this.coupon_id = coupon_id;
            this.appliedTV = appliedTV;
        }

    }
}
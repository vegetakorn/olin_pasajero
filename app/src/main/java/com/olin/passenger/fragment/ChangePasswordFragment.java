package com.olin.passenger.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;
import com.olin.passenger.R;
import com.olin.passenger.activity.MainActivity;
import com.olin.passenger.utils.Const;
import com.olin.passenger.utils.Util;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONObject;

/**
 * Created by TOXSL\chirag.tyagi on 17/3/17.
 */

public class ChangePasswordFragment extends BaseFragment {
    private String TAG = "ChangePasswordFragment";
    private View view;
    private TextInputEditText currentpwdET,newpwdET,confirmpwdET;
    private Button doneBT;
    private String currentpwd, newpwd, conpwd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_change_password, container, false);
        ((MainActivity) baseActivity).setActionBarTitle(baseActivity.getString(R.string.change_password), true);
        initUI();
        setAccess(baseActivity.store.getBoolean("isFb", false));
        return view;
    }

    private void setAccess(boolean isFb) {
        currentpwdET.setEnabled(!isFb);
        newpwdET.setEnabled(!isFb);
        confirmpwdET.setEnabled(!isFb);

        if (isFb) {
            showToast(baseActivity.getString(R.string.cannot_change_password));
        }
    }

    private void initUI() {
        currentpwdET = (TextInputEditText) view.findViewById(R.id.currentpwdET);
        newpwdET = (TextInputEditText) view.findViewById(R.id.newpwdET);
        confirmpwdET = (TextInputEditText) view.findViewById(R.id.confirmpwdET);

        doneBT = (Button) view.findViewById(R.id.doneBT);
        doneBT.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.doneBT:
                if (baseActivity.store.getBoolean("isFb", false)) {
                    showToast(baseActivity.getString(R.string.cannot_change_password));
                } else
                    validate();
                break;
        }
    }

    private void validate() {
        currentpwd = currentpwdET.getText().toString().trim();
        newpwd = newpwdET.getText().toString().trim();
        conpwd = confirmpwdET.getText().toString().trim();
        Util.log(TAG,newpwd);
        if (currentpwd.isEmpty()) {
            showToast(baseActivity.getString(R.string.please_enter_current_password));
        } else if (newpwd.isEmpty()) {
            showToast(baseActivity.getString(R.string.please_enter_new_password));
        } else if (newpwd.length() < 8) {
            showToast(baseActivity.getString(R.string.Please_enter_password_f_at_least_8_letters));
        } else if (!baseActivity.isValidPassword(newpwd)) {
            showToast(baseActivity.getString(R.string.password_should_contain_atleast_1_special_character));
        } else if (conpwd.isEmpty()) {
            showToast(baseActivity.getString(R.string.Please_enter_confirm_password));
        } else if (!conpwd.equals(newpwd)) {
            showToast(baseActivity.getString(R.string.Confirm_password_dont_match_with_password));
        } else {
            hitChangepwdApi();
        }
    }

    private void hitChangepwdApi() {
        RequestParams params = new RequestParams();
        params.put("User[oldPassword]", currentpwd);
        params.put("User[confirm_password]", newpwd);
        params.put("User[newPassword]", conpwd);
        baseActivity.syncManager.sendToServer(Const.API_USER_CHANGE_PASSWORD, params, this);
        baseActivity.startProgressDialog();
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("user") && action.equals("change-password")) {
            baseActivity.stopProgressDialog();
            if (status) {
                showToast(baseActivity.getString(R.string.password_changed_succesfully));
                gotoHomeFragment();
            } else {
                if (jsonObject.has("error")) {
                    showToast(jsonObject.optString("error"));
                } else {
                    showToast(baseActivity.getString(R.string.error));
                }
            }
        }
    }
}

package com.olin.passenger.fragment;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.olin.passenger.R;
import com.olin.passenger.activity.MainActivity;
import com.olin.passenger.data.PayPalData;
import com.olin.passenger.data.RideDetails;
import com.olin.passenger.data.WalletData;
import com.olin.passenger.utils.Const;
import com.olin.passenger.utils.PaypalPaymentActivity;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

/**
 * Created by TOXSL\ankush.walia on 23/1/17.
 */

public class PaymentFragment extends BaseFragment implements PaypalPaymentActivity.PaypalPaymentListener {
    private View view;
    private CardView cashCV;
    private int rideId;
    private RideDetails rideDetails;
    private TextView amountTV;
    private TextView walletBalanceTV;
    private CardView payPalCV;
    private CardView walletCV;
    private String balance;
    private String leftamount;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_payment, null);
        ((MainActivity) getActivity()).setActionBarTitle(baseActivity.getString(R.string.payment), false);
        PaypalPaymentActivity.setPaypalPaymentListener(PaymentFragment.this);
        initUi();
        if (getArguments() != null && getArguments().containsKey("ride_id")) {
            rideId = getArguments().getInt("ride_id");

        }
        getWalletDetails(baseActivity.getProfileDataFromPrefStore().id);
        return view;
    }

    public void getWalletDetails(int id) {
        baseActivity.syncManager.sendToServer(Const.API_USER_WALLET_GET + "?id=" + id, null, this);
    }

    private void initUi() {
        cashCV = (CardView) view.findViewById(R.id.cashCV);
        payPalCV = (CardView) view.findViewById(R.id.payPalCV);
        walletCV = (CardView) view.findViewById(R.id.walletCV);
        amountTV = (TextView) view.findViewById(R.id.amountTV);
        walletBalanceTV = (TextView) view.findViewById(R.id.walletBalanceTV);
        cashCV.setOnClickListener(this);
        walletCV.setOnClickListener(this);
        payPalCV.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        getRideDetail(rideId);
        super.onResume();
    }

    private void getRideDetail(int ride_id) {
        baseActivity.syncManager.sendToServer(Const.API_RIDE_DETAIL + "?id=" + ride_id, null, this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.cashCV:
                cashPayment();
                break;
            case R.id.payPalCV:
                paymentThroughPayPal();
                break;
            case R.id.walletCV:
                payByWallet();
                break;
        }
    }


    private void payByWallet() {
        if (rideDetails.amount != null) {
            try {
                RequestParams params = new RequestParams();
                params.add("Ride[amount]", rideDetails.amount);
                baseActivity.syncManager.sendToServer(Const.API_RIDE_WALLET_PAY + "?id=" + rideDetails.id, params, this);

            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }
    }


    private void paymentThroughPayPal() {
        if (!rideDetails.amount.isEmpty()) {
            Intent intent = new Intent(baseActivity,
                    PaypalPaymentActivity.class);
            if (rideDetails.remaining_balance < 0)
                intent.putExtra("totalFare", Double.parseDouble(balance));
            else if (leftamount != null)
                intent.putExtra("totalFare", Double.parseDouble(leftamount));
            else
                intent.putExtra("totalFare", Double.parseDouble(rideDetails.amount));
            intent.putExtra("id", rideDetails.id);
            startActivity(intent);
        }

    }


    private void cashPayment() {
        if (rideDetails.amount != null) {
            RequestParams params = new RequestParams();
            params.add("amount", rideDetails.amount);
            baseActivity.syncManager.sendToServer(Const.API_RIDE_CASH_PAY + "?id=" + rideId, params, this);
        } else {
            showToast(baseActivity.getString(R.string.please_try_again));
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("ride") && action.equals("detail")) {
            if (status) {
                rideDetails = baseActivity.parseRideDetails(jsonObject.optJSONObject("detail"));
                rideId = rideDetails.id;
                if (rideDetails.remaining_balance < 0) {
                    balance = String.valueOf(rideDetails.remaining_balance);
                    balance = balance.substring(1);
                    BigDecimal totalamount = new BigDecimal(balance);
                    totalamount = totalamount.setScale(2, BigDecimal.ROUND_HALF_UP);
                    amountTV.setText(baseActivity.getString(R.string.appx_price) + totalamount);
                } else {
                    if (rideDetails.amount != null && !rideDetails.amount.isEmpty() && !rideDetails.amount.equals("")) {
                        BigDecimal totalamount = new BigDecimal(rideDetails.amount);
                        totalamount = totalamount.setScale(2, BigDecimal.ROUND_HALF_UP);
                        amountTV.setText(baseActivity.getString(R.string.appx_price) + totalamount);
                    }
                }
            } else {
                showToast(jsonObject.optString("error"));
            }
        } else if (controller.equals("ride") && action.equals("cash-pay")) {
            if (status) {
                showToast(baseActivity.getString(R.string.payment_sucessfull));
                goToRateFragment();
                ((MainActivity) getActivity()).cancelNotification(Const.ALL_NOTI);
            } else {
                try {
                    showToast(jsonObject.getString("error"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (controller.equals("ride") && action.equals("wallet-pay")) {
            if (status) {
                if (jsonObject.has("left_amount")) {
                    BigDecimal totalamount = new BigDecimal(jsonObject.optString("left_amount"));
                    totalamount = totalamount.setScale(2, BigDecimal.ROUND_HALF_UP);
                    amountTV.setText("" + totalamount);
                    leftamount = jsonObject.optString("left_amount");
                    showDialog(jsonObject.optString("left_amount"));
                } else {
                    showToast(baseActivity.getString(R.string.payment_done));
                    ((MainActivity) getActivity()).cancelNotification(Const.ALL_NOTI);
                    goToRateFragment();
                }
                getWalletDetails(baseActivity.getProfileDataFromPrefStore().id);
                ((MainActivity) getActivity()).updateDrawer();
            } else {
                try {
                    showToast(jsonObject.getString("error"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (controller.equals("user-wallet") && action.equals("get")) {
            if (status) {
                WalletData walletData = baseActivity.parseWalletData(jsonObject);
                walletBalanceTV.setText(("$ " + walletData.amount));
                if (walletData.amount.equalsIgnoreCase("0")) {
                    showToast(baseActivity.getString(R.string.you_donot_have_money_in_our_wallet));
                    walletCV.setEnabled(false);
                } else {
                    walletCV.setEnabled(true);
                }
            } else {
                try {
                    showToast(jsonObject.getString("error"));
                    walletCV.setEnabled(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        if (controller.equals("ride") && action.equals("paypal-pay")) {
            if (status) {
                showToast(baseActivity.getString(R.string.payment_done));
                ((MainActivity) getActivity()).cancelNotification(Const.ALL_NOTI);
                goToRateFragment();
            } else {
                try {
                    showToast(jsonObject.getString("error"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void showDialog(String left_amount) {
        AlertDialog.Builder builder = new AlertDialog.Builder(baseActivity);
        builder.setTitle(baseActivity.getString(R.string.attention_))
                .setCancelable(false)
                .setMessage(baseActivity.getString(R.string.you_have) + " " + left_amount + " " + baseActivity.getString(R.string.left_to_pay_to_Driver))
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        walletCV.setEnabled(false);
                    }
                });
        builder.create().show();
    }

    private void goToRateFragment() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("rideDetails", rideDetails);
        Fragment fragment = new DriverRateFragment();
        fragment.setArguments(bundle);
        baseActivity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }


    @Override
    public void paymentDone(PayPalData payPalData) {
        RequestParams params = new RequestParams();
        params.put("transaction[transaction_id]", payPalData.id);
        params.put("transaction[amount]", payPalData.amount);
        baseActivity.syncManager.sendToServer(Const.API_RIDE_PAYPAL_PAY + "?id=" + rideId, params, this);
    }
}

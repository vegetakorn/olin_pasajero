package com.olin.passenger.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.olin.passenger.R;
import com.olin.passenger.activity.MainActivity;
import com.olin.passenger.data.PayPalData;
import com.olin.passenger.data.RideDetails;
import com.olin.passenger.data.WalletData;
import com.olin.passenger.utils.Const;
import com.olin.passenger.utils.PaypalPaymentActivity;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by TOXSL\ankush.walia on 23/1/17.
 */

public class WalletFragment extends BaseFragment implements PaypalPaymentActivity.PaypalPaymentListener {
    private static final double FIFTY = 50;
    private static final double TWOHND = 200;
    private static final double FIVEHND = 500;
    private View view;
    private int rideId;
    private RideDetails rideDetails;
    private EditText amountET;
    private TextView walletBalanceTV;
    private TextView twohndTV, fiftyTV, fivehndTV;
    private Button addMoneyBT;
    private TextView currentAmountTV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_wallet, null);
        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.wallet_), true);
        initUi();
        PaypalPaymentActivity.setPaypalPaymentListener(WalletFragment.this);
        return view;
    }

    private void initUi() {
        amountET = (EditText) view.findViewById(R.id.amountET);
        twohndTV = (TextView) view.findViewById(R.id.twohndTV);
        fivehndTV = (TextView) view.findViewById(R.id.fivehndTV);
        addMoneyBT = (Button) view.findViewById(R.id.addMoneyBT);
        fiftyTV = (TextView) view.findViewById(R.id.fiftyTV);
        currentAmountTV = (TextView) view.findViewById(R.id.currentAmountTV);
        amountET.setOnClickListener(this);
        twohndTV.setOnClickListener(this);
        fivehndTV.setOnClickListener(this);
        fiftyTV.setOnClickListener(this);
        addMoneyBT.setOnClickListener(this);
        getWalletDetails(baseActivity.getProfileDataFromPrefStore().id);
    }


    public void getWalletDetails(int id) {
        baseActivity.syncManager.sendToServer(Const.API_USER_WALLET_GET + "?id=" + id, null, this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.addMoneyBT:
                validateAndAddAmount();
                break;
            case R.id.fiftyTV:
                addAmountToWallet(FIFTY);
                break;
            case R.id.twohndTV:
                addAmountToWallet(TWOHND);
                break;
            case R.id.fivehndTV:
                addAmountToWallet(FIVEHND);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        PaypalPaymentActivity.setPaypalPaymentListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void validateAndAddAmount() {
        if (amountET.getText().toString().isEmpty()) {
            showToast(getString(R.string.please_enter_amonut_to_add_in_wallet));
        } else if (amountET.getText().toString().equals("0")) {
            showToast(getString(R.string.Please_enter_valid_amount));
        } else {
            addAmountToWallet(Double.parseDouble(amountET.getText().toString()));
        }
    }


    private void addAmountToWallet(double amount) {
        Intent intent = new Intent(baseActivity,
                PaypalPaymentActivity.class);
        intent.putExtra("totalFare", amount);
        startActivity(intent);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("user-wallet") && action.equals("get")) {
            if (status) {
                WalletData walletData = baseActivity.parseWalletData(jsonObject);
                setButtonsEnabled(true);
                currentAmountTV.setText(getString(R.string.appx_price) + walletData.amount);
                fiftyTV.setText(baseActivity.getString(R.string._50));
                twohndTV.setText(baseActivity.getString(R.string._200));
                fivehndTV.setText(baseActivity.getString(R.string._500));
                if ((MainActivity) getActivity() != null) {
                    ((MainActivity) getActivity()).updateDrawer();
                    ((MainActivity) getActivity()).updateWallet(getString(R.string.appx_price), walletData.amount);
                }

            } else {
                try {
                    showToast(jsonObject.getString("error"));
                    setButtonsEnabled(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (controller.equals("user-wallet") && action.equals("add")) {
            if (status) {
                getWalletDetails(baseActivity.getProfileDataFromPrefStore().id);
                amountET.setText("");
            } else {
                try {
                    showToast(jsonObject.getString("error"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setButtonsEnabled(boolean enabled) {
        addMoneyBT.setEnabled(enabled);
        fiftyTV.setEnabled(enabled);
        twohndTV.setEnabled(enabled);
        fivehndTV.setEnabled(enabled);
    }


    @Override
    public void paymentDone(PayPalData payPalData) {

        String addWallet = Const.API_USER_WALLET_ADD;
        RequestParams params = new RequestParams();
        params.put("UserWallet[transaction-id]", payPalData.id);
        params.put("UserWallet[transaction-amount]", payPalData.amount);
        baseActivity.syncManager.sendToServer(addWallet, params, this);

        getWalletDetails(baseActivity.getProfileDataFromPrefStore().id);

    }
}

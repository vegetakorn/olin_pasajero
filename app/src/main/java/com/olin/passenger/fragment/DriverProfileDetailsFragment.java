package com.olin.passenger.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.olin.passenger.R;
import com.olin.passenger.activity.MainActivity;
import com.olin.passenger.adapter.ReviewPagerAdapter;
import com.olin.passenger.data.RideDetails;

/**
 * Created by TOXSL\jagmel.singh on 29/5/17.
 */

public class DriverProfileDetailsFragment extends BaseFragment {

    private View view;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ReviewPagerAdapter adapter;
    private RideDetails rideDetails;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_driver_profile_details, null);
        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.details), true);
        viewPager = (ViewPager) view.findViewById(R.id.viewpagerVP);
        tabLayout = (TabLayout) view.findViewById(R.id.tabsTL);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setUpTab();
        return view;
    }

    private void setUpTab() {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            RelativeLayout relativeLayout = (RelativeLayout)
                    LayoutInflater.from(baseActivity).inflate(R.layout.tab_layout, tabLayout, false);
            TextView tabTextView = (TextView) relativeLayout.findViewById(R.id.tab_title);
            View dividerV = relativeLayout.findViewById(R.id.dividerV);
            assert tab != null;
            tabTextView.setText(tab.getText());
            tab.setCustomView(relativeLayout);
            if (i == 0) {
                tab.select();
            }
            if (i == tabLayout.getTabCount() - 1) {
                dividerV.setVisibility(View.GONE);
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ReviewPagerAdapter(getChildFragmentManager());

        Fragment profile = new DriverProfileFragment();
        Fragment review = new DriverReviewFragment();
        if (getArguments() != null) {
            profile.setArguments(getArguments());
            review.setArguments(getArguments());
        }
        adapter.addFragment(profile, getString(R.string.details));
        adapter.addFragment(review, getString(R.string.reviews));
        viewPager.setAdapter(adapter);
    }

}

package com.olin.passenger.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.olin.passenger.R;
import com.olin.passenger.activity.LoginSignUpActivity;
import com.olin.passenger.utils.Const;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by TOXSL\ankush.walia on 23/1/17.
 */

public class ForgotPasswordFragment extends BaseFragment {
    private View view;
    private EditText emailET;
    private Button resetBT;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_forgot_password, null);
        ((LoginSignUpActivity) baseActivity).toolbar_titleTV.setText(getString(R.string.forgot_password));
        emailET = (EditText) view.findViewById(R.id.emailET);
        resetBT = (Button) view.findViewById(R.id.resetBT);
        resetBT.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.resetBT:
                checkValidation();
                break;
        }
    }

    private void checkValidation() {
        String email = emailET.getText().toString().trim();
        if (email.isEmpty()) {
            showToast(getString(R.string.please_enter_your_emil));
        } else if (!baseActivity.isValidMail(email)) {
            showToast(getString(R.string.please_enter_valid_email));
        } else {
            sendToServer(email);
        }
    }

    private void sendToServer(String email) {
        RequestParams params = new RequestParams();
        params.put("User[email]", email);
        baseActivity.syncManager.sendToServer("api/user/recover" + "?role_id=" + Const.ROLE_PASSENGER, params, this);
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("user") && action.equals("recover")) {
            if (status) {
                try {
                    showToast(jsonObject.getString("success"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                baseActivity.getSupportFragmentManager().popBackStack();
            } else {
                try {
                    showToast(jsonObject.getString("error"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

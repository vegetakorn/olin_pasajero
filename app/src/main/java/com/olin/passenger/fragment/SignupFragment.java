package com.olin.passenger.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcel;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.pkmmte.view.CircularImageView;
import com.olin.passenger.R;
import com.olin.passenger.activity.BaseActivity;
import com.olin.passenger.activity.LoginSignUpActivity;
import com.olin.passenger.activity.MainActivity;
import com.olin.passenger.adapter.CustomSpinnerAdapter;
import com.olin.passenger.adapter.PostalCodeAdapter;
import com.olin.passenger.data.CountryData;
import com.olin.passenger.data.ProfileData;
import com.olin.passenger.data.SearchSignUp;
import com.olin.passenger.utils.Const;
import com.olin.passenger.utils.ImageUtils;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by TOXSL\ankush.walia on 23/1/17.
 */

public class SignupFragment extends BaseFragment implements BaseActivity.PermCallback, ImageUtils.ImageSelectCallback {


    private View view;
    private CircularImageView profileCIV;
    private AppCompatEditText emailET, passwordET, confirmPasswordET, firstNameET, lastNameET;
    private AppCompatEditText phoneNumberET;
    private Button signupBT;
    private int countryID;
    private File profileFile;
    private ArrayList<CountryData> countryDatas = new ArrayList<>();
    private ArrayList<SearchSignUp> searchSignUps = new ArrayList<>();
    private String[] arrCountry;
    private Spinner countrySP;
    private CustomSpinnerAdapter spinnerAdapter;
    private PostalCodeAdapter postalCodeAdapter;
    private TextView tx;
    private int totalPages;
    private int pagecount = 0;
    private AlertDialog dialog;
    private int postalId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_signup, null);
        ((LoginSignUpActivity) baseActivity).toolbar.setVisibility(View.VISIBLE);
        ((LoginSignUpActivity) baseActivity).setToolbarTitle(baseActivity.getString(R.string.sign_up));
        profileCIV = (CircularImageView) view.findViewById(R.id.profileCIV);
        firstNameET = (AppCompatEditText) view.findViewById(R.id.firstNameET);
        lastNameET = (AppCompatEditText) view.findViewById(R.id.lastNameET);
        countrySP = (Spinner) view.findViewById(R.id.countrySP);
        emailET = (AppCompatEditText) view.findViewById(R.id.emailET);
        passwordET = (AppCompatEditText) view.findViewById(R.id.passwordET);
        confirmPasswordET = (AppCompatEditText) view.findViewById(R.id.confirmPasswordET);
        phoneNumberET = (AppCompatEditText) view.findViewById(R.id.phoneNumberET);
        signupBT = (Button) view.findViewById(R.id.signupBT);
        signupBT.setOnClickListener(this);

        getCountryCode();

        countrySP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryData dataItem = (CountryData) parent.getItemAtPosition(position);
                if (!(((CountryData) countrySP.getSelectedItem()).id == 0)) {
                    countryID = dataItem.id;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        return view;
    }

    private void getCountryCode() {
        baseActivity.syncManager.sendToServer(Const.API_USER_COUNTRY, null, this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.signupBT:
                if (isValidate())
                    signup();
                break;
            case R.id.selectImageIV:
                if (baseActivity.checkPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Const.USER_IMAGE, this)) {
                    ImageUtils.ImageSelect.Builder builder = new ImageUtils.ImageSelect.Builder(baseActivity, this, Const.USER_IMAGE);
                    builder.start();
                }
                break;
        }
    }

    private void signup() {
        RequestParams params = new RequestParams();
        params.put("User[first_name]", firstNameET.getText().toString().trim());
        params.put("User[last_name]", lastNameET.getText().toString().trim());
        params.put("User[email]", emailET.getText().toString());
        params.put("User[password]", passwordET.getText().toString());
        params.put("User[contact_no]", phoneNumberET.getText().toString().replace(" ", ""));
        params.put("User[country_id]", countryID);//mexico countryID
        params.put("User[sepomex_id]", postalId);
        params.put("User[device_type]", 1);
        params.put("User[device_token]", baseActivity.getUniqueDeviceId());
        baseActivity.syncManager.sendToServer(Const.API_USER_PASSENGER_SIGNUP, params, this);
    }

    private boolean isValidate() {
        if (firstNameET.getText().toString().trim().equals(""))
            showToast(getString(R.string.please_enter_first_name));
        else if (lastNameET.getText().toString().trim().equals(""))
            showToast(getString(R.string.please_enter_last_name));
        else if (emailET.getText().toString().trim().equals(""))
            showToast(getString(R.string.please_enter_email));
        else if (!baseActivity.isValidMail(emailET.getText().toString()))
            showToast(getString(R.string.please_enter_valid_email_id));
        else if (passwordET.getText().toString().trim().equals(""))
            showToast(getString(R.string.please_enter_password));
        else if (!baseActivity.isValidPassword(passwordET.getText().toString()))
            showToast(getString(R.string.please_enter_valid_password));
        else if (confirmPasswordET.getText().toString().trim().equals(""))
            showToast(getString(R.string.pleease_enter_confirm));
        else if (!baseActivity.isValidPassword(confirmPasswordET.getText().toString()))
            showToast(getString(R.string.password_not_match));
        else if (!passwordET.getText().toString().trim().equals(confirmPasswordET.getText().toString().trim()))
            showToast(getString(R.string.mismatch_password));
        else if (phoneNumberET.getText().toString().trim().equals(""))
            showToast(getString(R.string.please_enter_phone_number));
        else
            return true;
        return false;
    }


    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equalsIgnoreCase("user") && action.equals("passenger-signup")) {
            if (status) {
                login();
            } else {
                showToast(getString(R.string.email_exist));
            }
        } else if (controller.equalsIgnoreCase("user") && action.equals("login")) {
            if (status) {
                showToast(getString(R.string.signup_successfully));
                try {
                    baseActivity.syncManager.setLoginStatus(jsonObject.getString("auth_code"));
                    JSONObject object = jsonObject.getJSONObject("user_detail");
                    baseActivity.store.setBoolean("isFb", false);
                    ProfileData profileData = baseActivity.parseProfileData(object);
                    baseActivity.saveProfileDataInPrefStore(profileData);
                    baseActivity.startActivity(new Intent(baseActivity, MainActivity.class));
                    baseActivity.finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (controller.equals("user") && action.equals("country")) {
            if (status) {
                try {
                    countryDatas.clear();
                    JSONArray country = jsonObject.getJSONArray("country");
                    arrCountry = new String[country.length()];
                    for (int i = 0; i < country.length(); i++) {
                        JSONObject countryObjct = country.getJSONObject(i);
                        CountryData countryData = new CountryData(Parcel.obtain());
                        countryData.id = countryObjct.getInt("id");
                        countryData.title = countryObjct.getString("title");
                        countryData.telephone_code = countryObjct.getString("telephone_code");
                        countryData.flag = countryObjct.getString("flag");
                        countryData.country_code = countryObjct.getString("country_code");
                        countryData.currency_symbol = countryObjct.getString("currency_symbol");
                        if (countryObjct.has("car_type")) {
                            JSONArray car_type = countryObjct.getJSONArray("car_type");
                            if (car_type.length() != 0) {
                                int[] carTypes = new int[car_type.length()];
                                for (int j = 0; j < car_type.length(); j++) {
                                    carTypes[j] = (int) car_type.get(j);
                                    countryData.arr_car_type = carTypes;
                                }
                                countryDatas.add(countryData);
                                arrCountry[i] = countryData.title;
                            }
                        }


                    }
                    spinnerAdapter = new CustomSpinnerAdapter(baseActivity, countryDatas);
                    countrySP.setAdapter(spinnerAdapter);
                    if (countryDatas.size() != 0) {
                        if (countryID == 11) {
                            for (int i = 0; i < countryDatas.size(); i++) {
                                if (countryDatas.get(i).telephone_code.equals("+1")) {
                                    countrySP.setSelection(i);
                                    break;
                                }

                            }
                        } else {
                            for (int i = 0; i < countryDatas.size(); i++) {
                                if (countryDatas.get(i).id == countryID) {
                                    countrySP.setSelection(i);
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        } else if (controller.equals("user") && action.equals("address-search")) {
            if (status) {
                JSONArray jsonArray = jsonObject.optJSONArray("details");
                totalPages = jsonObject.optInt("totalPage");
                if (totalPages >= pagecount) {
                    pagecount++;
                }
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.optJSONObject(i);
                    SearchSignUp searchSignUp = new SearchSignUp(Parcel.obtain());
                    searchSignUp.id = object.optInt("id");
                    searchSignUp.settlement = object.optString("settlement");
                    searchSignUp.municipality = object.optString("municipality");
                    searchSignUp.state = object.optString("state");
                    searchSignUp.zipcode = object.optInt("zipcode");
                    searchSignUps.add(searchSignUp);
                }
                if (postalCodeAdapter != null) {
                    if (!dialog.isShowing())
                        dialog.show();
                    postalCodeAdapter.notifyDataSetChanged();
                } else {
                    showPostalCodeDialog();
                }
            } else {
                if (pagecount > 0) {
                    showToast(getString(R.string.no_more_zipcodes));
                } else {
                    showToast(jsonObject.optString("error"));
                }
            }

        }
    }

    private void showPostalCodeDialog() {
        if (searchSignUps != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(baseActivity);
            builder.setTitle(getString(R.string.select_your_address));

            LayoutInflater inflater = LayoutInflater.from(baseActivity);
            View content = inflater.inflate(R.layout.postaldialog, null);
            builder.setView(content);
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.cancel), new OnClicKKDialog(searchSignUps) {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    searchSignUps.clear();
                    dialog.dismiss();
                }
            });
            ListView postalLV = (ListView) content.findViewById(R.id.postalLV);

            postalCodeAdapter = new PostalCodeAdapter(baseActivity, searchSignUps, this);
            postalLV.setAdapter(postalCodeAdapter);
            postalLV.setOnItemClickListener(new OnClicKK(searchSignUps) {
                @SuppressLint("SetTextI18n")
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    pagecount = 0;
                    postalId = searchSignUps.get(position).id;
                    dialog.dismiss();
                }
            });
            dialog = builder.create();
            dialog.show();
        }
    }

    abstract class OnClicKK implements AdapterView.OnItemClickListener {
        ArrayList<SearchSignUp> searchSignUps;

        OnClicKK(ArrayList<SearchSignUp> searchSignUps) {
            this.searchSignUps = searchSignUps;

        }
    }

    abstract class OnClicKKDialog implements DialogInterface.OnClickListener {
        ArrayList<SearchSignUp> searchSignUps;

        OnClicKKDialog(ArrayList<SearchSignUp> searchSignUps) {
            this.searchSignUps = searchSignUps;

        }
    }

    private void login() {
        RequestParams params = new RequestParams();
        params.put("LoginForm[username]", emailET.getText().toString());
        params.put("LoginForm[password]", passwordET.getText().toString());
        params.put("User[role_id]", 1);
        String token = FirebaseInstanceId.getInstance().getToken();
        if (token != null)
            params.put("LoginForm[device_token]", token);
        else
            params.put("LoginForm[device_token]", baseActivity.getUniqueDeviceId());
        params.put("LoginForm[device_type]", "1");
        baseActivity.syncManager.sendToServer(Const.API_USER_LOGIN, params, this);
    }

    @Override
    public void permGranted(int resultCode) {
        ImageUtils.ImageSelect.Builder builder = new ImageUtils.ImageSelect.Builder(baseActivity, this, Const.USER_IMAGE);
        builder.start();
    }

    @Override
    public void permDenied(int resultCode) {

    }

    @Override
    public void onImageSelected(String imagePath, int resultCode) {
        Bitmap bitmap = ImageUtils.imageCompress(imagePath);
        profileCIV.setImageBitmap(bitmap);
        profileFile = ImageUtils.bitmapToFile(bitmap, baseActivity);
    }


    private abstract class MyClickListener implements DialogInterface.OnClickListener {
        String[] mStrings;
        TextView textView;


        @Override
        public void onClick(DialogInterface dialog, int which) {
        }

        MyClickListener(TextView textView, String[] strings) {
            this.textView = textView;
            this.mStrings = strings;

        }
    }
}

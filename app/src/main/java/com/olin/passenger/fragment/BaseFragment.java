package com.olin.passenger.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.olin.passenger.R;
import com.olin.passenger.activity.BaseActivity;
import com.olin.passenger.service.LocationUpdateService;
import com.olin.passenger.utils.GoogleApisHandle;
import com.toxsl.volley.Request;
import com.toxsl.volley.VolleyError;
import com.toxsl.volley.toolbox.SyncEventListner;

import org.json.JSONObject;

/**
 * Created by TOXSL\neeraj.narwal on 2/2/16.
 */
public class BaseFragment extends Fragment implements AdapterView.OnItemClickListener,
        View.OnClickListener, SyncEventListner, AdapterView.OnItemSelectedListener,
        CompoundButton.OnCheckedChangeListener {

    public BaseActivity baseActivity;
    public static GoogleApisHandle googleApiHandle;
    public static LocationUpdateService locationUpdateService;
    public Animation startAnim;
    public Animation exitAnim;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseActivity = (BaseActivity) getActivity();
        googleApiHandle = BaseActivity.googleApiHandle;
        locationUpdateService = BaseActivity.locationUpdateService;
        startAnim = AnimationUtils.loadAnimation(getContext(), R.anim.show_b_t);
        exitAnim = AnimationUtils.loadAnimation(getContext(), R.anim.popup_hide);
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
        baseActivity.checkPlayServices();
        baseActivity.hideSoftKeyboard();
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                baseActivity.getSupportFragmentManager().popBackStack();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    }

    public void showToast(String msg) {
        baseActivity.showToast(msg);
    }

    @Override
    public void onSyncStart() {
        baseActivity.onSyncStart();
    }

    @Override
    public void onSyncFinish() {
        baseActivity.onSyncFinish();
    }

    @Override
    public void onSyncFailure(VolleyError error, Request mRequest) {
        baseActivity.onSyncFailure(error, mRequest);
    }

    public void log(String s) {
        baseActivity.log(s);
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

    public void startAnimation(final LinearLayout view, final boolean isStart) {

        Animation animation;
        if (isStart) {
            animation = startAnim;
        } else {
            animation = exitAnim;
        }
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (isStart)
                    view.setVisibility(View.VISIBLE);
                else
                    view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.clearAnimation();
        if (isStart) {
            view.startAnimation(startAnim);
        } else {
            view.startAnimation(exitAnim);
        }
    }
    public void startAnimationRL(final RelativeLayout view, final boolean isStart) {

        Animation animation;
        if (isStart) {
            animation = startAnim;
        } else {
            animation = exitAnim;
        }
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (isStart)
                    view.setVisibility(View.VISIBLE);
                else
                    view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.clearAnimation();
        if (isStart) {
            view.startAnimation(startAnim);
        } else {
            view.startAnimation(exitAnim);
        }
    }
    public void gotoHomeFragment() {
        baseActivity.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeFragment()).commit();
    }
}

package com.olin.passenger.fragment;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pkmmte.view.CircularImageView;
import com.olin.passenger.R;
import com.olin.passenger.activity.BaseActivity;
import com.olin.passenger.activity.MainActivity;
import com.olin.passenger.activity.PickLocationActivity;
import com.olin.passenger.data.RideDetails;
import com.olin.passenger.utils.Const;
import com.olin.passenger.utils.GoogleApisHandle;
import com.olin.passenger.utils.Util;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import static android.app.Activity.RESULT_CANCELED;

/**
 * Created by TOXSL\visha.sehgal on 17/1/17.
 */

public class RideStartFragment extends BaseFragment implements OnMapReadyCallback, GoogleApisHandle.OnPolyLineReceived {
    private static String TAG = "RideStartFragment";
    private View view;
    private GoogleMap googlemap;
    private Marker pickupMarker;
    private Marker destinationMarker;
    private TextView pickUpLocationTV;
    private TextView carNameTV;
    private TextView tvDriverCompany;
    private TextView carNumerTV;
    private TextView nameTV;
    private RatingBar rbDriver;
    private double lng;
    private CircularImageView profileIV;
    private int rideId;
    Runnable task = new Runnable() {
        @Override
        public void run() {
            if (rideId != 0)
                getRideDetail(rideId);
        }
    };
    private RideDetails rideDetails;
    private TextView updateRideTV;
    private LinearLayout driverDetailsLL;
    private TextView shareTV;
    private Timer timer;
    private Handler handler;
    private String dropOffAddress;
    private LatLng dropoffPlace;
    Location currentLocation;
    private Bundle bundle;
    private double lat;
    private double droplat;
    private double droplng;
    private FloatingActionButton callTV,messagesTV,cancelRideTV;
    private HomeFragment homeFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_start_ride, container, false);
            ((MainActivity) getActivity()).setActionBarTitle(baseActivity.getString(R.string.app_name), true);
            initUi();
        } else {
            ((MainActivity) getActivity()).setActionBarTitle(baseActivity.getString(R.string.app_name), true);
        }
        Util.log(TAG,"onCreateView se inicializo la vista");
        return view;
    }

    private void initUi() {
        handler = new Handler();
        pickUpLocationTV = (TextView) view.findViewById(R.id.pickUpLocTV);
        pickUpLocationTV.setSelected(true);
        carNameTV = (TextView) view.findViewById(R.id.carNameTV);
        tvDriverCompany = (TextView) view.findViewById(R.id.tvDriverCompany);
        messagesTV = (FloatingActionButton) view.findViewById(R.id.messagesTV);
        callTV = (FloatingActionButton) view.findViewById(R.id.callTV);
        shareTV = (TextView) view.findViewById(R.id.shareTV);
        carNumerTV = (TextView) view.findViewById(R.id.carNumerTV);
        cancelRideTV = (FloatingActionButton) view.findViewById(R.id.cancelRideTV);
        updateRideTV = (TextView) view.findViewById(R.id.updateRideTV);
        driverDetailsLL = (LinearLayout) view.findViewById(R.id.driverDetailsLL);
        nameTV = (TextView) view.findViewById(R.id.nameTV);
        rbDriver = (RatingBar) view.findViewById(R.id.rbDriver);
        profileIV = (CircularImageView) view.findViewById(R.id.profileIV);
        //FloatingActionButton callFAB = (FloatingActionButton) view.findViewById(R.id.callFAB);
        pickUpLocationTV.setSelected(true);
        if (getArguments() != null && getArguments().containsKey("ride_id")) {
            rideId = getArguments().getInt("ride_id");
        }
        cancelRideTV.setOnClickListener(this);
        driverDetailsLL.setOnClickListener(this);
        callTV.setOnClickListener(this);
        shareTV.setOnClickListener(this);
        messagesTV.setOnClickListener(this);
        updateRideTV.setOnClickListener(this);
        //callFAB.setOnClickListener(this);
        initilizeMap();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void getRideDetail(int ride_id) {
        baseActivity.syncManager.sendToServer(Const.API_RIDE_DETAIL + "?id=" + ride_id, null, this);
    }

    private void initilizeMap() {
        if (googlemap == null) {
            SupportMapFragment startmap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.startmap));
            startmap.getMapAsync(this);
        }
    }

    public void startTimer() {
        stopTimer();
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(task);
            }
        };
        timer.schedule(timerTask, 10000, 10000);
    }

    public void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
            if (handler != null)
                handler.removeCallbacks(task);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getRideDetail(rideId);

        if (googlemap != null) {
            SupportMapFragment startmap = ((SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.startmap));
            startmap.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopTimer();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancelRideTV:
                askToCancelRide();
                break;
            case R.id.updateRideTV:
                askToUpdateRide();
                break;
            case R.id.messagesTV:
                message(rideDetails.driverData.countryData.telephone_code.concat(rideDetails.driverData.contact_no));
                break;
            case R.id.callTV:
                call(rideDetails.driverData.countryData.telephone_code.concat(rideDetails.driverData.contact_no));
                break;
            case R.id.shareTV:
                shareLocation(rideDetails.location_lat, rideDetails.location_long);
                break;
            case R.id.driverDetailsLL:
                if (rideDetails != null)
                    goToDriverDetailsFrag(rideDetails);
                break;
            /*case R.id.callFAB:
                baseActivity.callIntent("911");
                break;
                */
        }
    }

    private void searchLocation(int requestCode) {
        if (requestCode != 0) {
            Intent i = new Intent(getActivity(), PickLocationActivity.class);
            if (requestCode == Const.DROPOFF)
                i.putExtra("title", "Dropoff Location");
            getActivity().startActivityForResult(i, requestCode);
        }
    }

    private void shareLocation(Double lat, Double longt) {
        String uri = "https://maps.google.com/maps/place/" + lat + "," + longt;
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/.html");
        String ShareSub = getResources().getString(R.string.here_is_my_location);
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, ShareSub);
        sharingIntent.putExtra(Intent.EXTRA_TITLE, ShareSub);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, ShareSub + '\n' + '\n' + uri);
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_via)));
    }

    private void askToCancelRide() {
        AlertDialog.Builder builder = new AlertDialog.Builder(baseActivity);
        builder.setMessage(baseActivity.getString(R.string.do_you_want_to_cancel_this_ride));
        builder.setCancelable(false);

        builder.setPositiveButton(
                getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        cancelRide();
                        dialog.cancel();
                    }
                });

        builder.setNegativeButton(
                getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void askToUpdateRide() {
        AlertDialog.Builder builder = new AlertDialog.Builder(baseActivity);
        if (rideDetails.coupon_applied) {
            builder.setMessage(baseActivity.getString(R.string.no_longer_valid));
        } else {
            builder.setMessage(baseActivity.getString(R.string.do_you_want_to_update_this_ride));
        }
        builder.setCancelable(false);

        builder.setPositiveButton(
                getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        searchLocation(Const.DROPOFF);
                        dialog.cancel();
                    }
                });

        builder.setNegativeButton(
                getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void goToDriverDetailsFrag(RideDetails rideDetails) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("rideDetails", rideDetails);
        Fragment fragment = new DriverProfileDetailsFragment();
        fragment.setArguments(bundle);
        baseActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void cancelRide() {
        baseActivity.syncManager.sendToServer(Const.API_RIDE_CANCELED + rideId, null, this);
    }


    @Override
    public void onSyncStart() {
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("ride") && action.equals("detail")) {
            if (status) {
                rideDetails = baseActivity.parseRideDetails(jsonObject.optJSONObject("detail"));
                drawPolyLine();
                assert rideDetails != null;
                if (rideDetails.state_id == Const.STATE_STARTED) {
                    cancelRideTV.setVisibility(View.GONE);
                } else if (rideDetails.state_id == Const.STATE_COMPLETED) {
                    gotoPaymentFrag(rideDetails.id);
                }
                rideId = rideDetails.id;
                updateUiData();

            } else {
                showToast(jsonObject.optString("error"));
            }
        } else if (controller.equals("ride") && action.equals("update")) {
            if (status) {
                rideDetails = baseActivity.parseRideDetails(jsonObject.optJSONObject("detail"));
                drawPolyLine();
                assert rideDetails != null;
                if (rideDetails.state_id == Const.STATE_STARTED) {
                    cancelRideTV.setVisibility(View.GONE);
                } else if (rideDetails.state_id == Const.STATE_COMPLETED) {
                    gotoPaymentFrag(rideDetails.id);
                }
                rideId = rideDetails.id;
                updateUiData();

            } else {
                showToast(jsonObject.optString("error"));
            }
        } else if (controller.equals("ride") && action.equals("canceled")) {
            if (status) {
                baseActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new HomeFragment())
                        .commitAllowingStateLoss();
                log(jsonObject.optString("detail"));
            } else {
                showToast(jsonObject.optString("error"));
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void updateUiData() {
        pickUpLocationTV.setText(rideDetails.location);
        nameTV.setText(rideDetails.driverData.first_name + " " + rideDetails.driverData.last_name);
        rbDriver.setRating(BaseActivity.round((float) rideDetails.driverData.driver_rating, 1));
        carNumerTV.setText(rideDetails.vehicle.registration_no);
        carNameTV.setText(rideDetails.vehicle.car_make + " " + rideDetails.vehicle.model);
        /*
        if(rideDetails.driverData.driver != null && rideDetails.driverData.driver.company.name != ""){
            tvDriverCompany.setVisibility(View.VISIBLE);
            tvDriverCompany.setText(rideDetails.driverData.driver.company.name);
        }
        */
        if (rideDetails.driverData != null) {
            try {
                //baseActivity.picasso.load(rideDetails.driverData.image_file).placeholder(R.mipmap.default_avatar).into(profileIV);
                baseActivity.picasso.load(rideDetails.driverData.vehicle_image).placeholder(R.mipmap.ic_car_big).into(profileIV);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void call(String number) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + number));
            startActivity(callIntent);
        } catch (ActivityNotFoundException e) {
            baseActivity.showToast(String.valueOf(e));
        }
    }


    private void message(String number) {
        Intent message = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + number));
        message.putExtra("sms_body", baseActivity.getString(R.string.type_message_here));

        try {
            startActivity(message);
        } catch (Exception e) {
            showToast(baseActivity.getString(R.string.no_app_found));
        }
    }

    private void setPickupMarker(LatLng pickupLatLng) {
        if (pickupMarker == null)
            pickupMarker = googlemap.addMarker(new MarkerOptions()
                    .position(pickupLatLng).draggable(false)
                    .icon(BitmapDescriptorFactory.fromResource(rideDetails == null ? setCarType(0) : setCarType(rideDetails.vehicle.id))));

        else {
            pickupMarker.setPosition(pickupLatLng);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == Const.DROPOFF) {
                if (data != null) {
                    dropOffAddress = data.getStringExtra("Location Address");
                    droplat = data.getDoubleExtra("lat", droplat);
                    droplng = data.getDoubleExtra("lng", droplng);

                    bundle = new Bundle();
                    bundle = data.getBundleExtra("Latlng");
                    if (bundle != null) {
                        lat = bundle.getDouble("lat");
                        lng = bundle.getDouble("longt");

                    }

                    if (dropOffAddress != null && !dropOffAddress.isEmpty()) {
                        if (bundle != null)
                            dropoffPlace = new LatLng(lat, lng);
                        else if (droplat != 0.0)
                            dropoffPlace = new LatLng(droplat, droplng);
                        else
                            dropoffPlace = googleApiHandle.getLatLngFromAddress(dropOffAddress);
                        if (dropoffPlace != null) {
                            rideUpdate();
                        } else {
                            showToast(getString(R.string.address_is_currently_unavailable));
                        }
                    }
                }
            }

        }

    }

    private void rideUpdate() {

        if (rideDetails != null && rideDetails.driverData != null && rideDetails.driverData.latitude != null) {
            RequestParams params = new RequestParams();
            params.put("Ride[updated_lat]", rideDetails.driverData.latitude);
            params.put("Ride[updated_long]", rideDetails.driverData.longitude);
            params.put("Ride[destination]", dropOffAddress);
            params.put("Ride[destination_lat]", dropoffPlace.latitude);
            params.put("Ride[destination_long]", dropoffPlace.longitude);

            baseActivity.syncManager.sendToServer(Const.API_RIDE_UPDATE + "?id=" + rideId, params, this);
        }

    }

    private void setDestinationMarker(LatLng dropoffLatLng, String time, double distance, String line) {
        if (destinationMarker == null) {
            MarkerOptions options = new MarkerOptions()
                    .position(dropoffLatLng).draggable(false)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_pin_red));
            if (time != null && !time.isEmpty()) {
                options.title(baseActivity.getString(R.string.estimate_time_distance));
                options.snippet("Time : " + time + " & " + "distance :" + line);
            }
            destinationMarker = googlemap.addMarker(options);
            destinationMarker.showInfoWindow();

        } else {
            if (time != null && !time.isEmpty()) {
                destinationMarker.setTitle(baseActivity.getString(R.string.estimate_time_distance));
                destinationMarker.setSnippet("Time : " + time + " & " + "distance :" + line);
            }
            destinationMarker.setPosition(dropoffLatLng);
            destinationMarker.showInfoWindow();

        }
    }

    private int setCarType(int type) {
        switch (type) {
            case 0:
                return R.mipmap.ic_car;
            case 1:
                return R.mipmap.ic_suv;
            case 2:
                return R.mipmap.ic_minivan;
        }
        return R.mipmap.ic_car;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setTiltGesturesEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(false);
        this.googlemap = googleMap;
        googleApiHandle.setPolyLineReceivedListener(this);
        drawPolyLine();
        if(homeFragment != null){
            homeFragment.stopTimer();
        }
        startTimer();
    }

    private void drawPolyLine() {
        if (rideDetails != null && rideDetails.driverData != null && googlemap != null) {
            if (rideDetails.state_id == Const.STATE_ACCEPTED) {
                if (rideDetails.driverData.latitude != null)
                    googleApiHandle.getDirectionsUrl(new LatLng(rideDetails.driverData.latitude, rideDetails.driverData.longitude), new LatLng(rideDetails.location_lat, rideDetails.location_long), googlemap);
            } else {
                if (rideDetails.driverData.latitude != null)
                    googleApiHandle.getDirectionsUrl(new LatLng(rideDetails.driverData.latitude, rideDetails.driverData.longitude), new LatLng(rideDetails.destination_lat, rideDetails.destination_long), googlemap);

            }
        }
    }

    @Override
    public void onPolyLineReceived(LatLng origin, LatLng destination, GoogleMap routeMap, Polyline polyline, double distance, String time, String line) {
        setPickupMarker(origin);
        setDestinationMarker(destination, time, distance, line);
    }

    private void gotoPaymentFrag(int id) {
        Fragment fragment = new PaymentFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("ride_id", id);
        fragment.setArguments(bundle);
        baseActivity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commitAllowingStateLoss();
    }

    public void setHomeFragment(HomeFragment homeFragment) {
        this.homeFragment = homeFragment;
    }
}

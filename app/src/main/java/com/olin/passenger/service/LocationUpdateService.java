package com.olin.passenger.service;

import android.Manifest;
import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.location.Location;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.olin.passenger.BuildConfig;
import com.olin.passenger.R;
import com.olin.passenger.utils.Const;
import com.olin.passenger.utils.PrefStore;
import com.olin.passenger.utils.Util;
import com.toxsl.volley.Request;
import com.toxsl.volley.VolleyError;
import com.toxsl.volley.toolbox.SyncEventListner;
import com.toxsl.volley.toolbox.SyncManager;

import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;


public class LocationUpdateService extends Service implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, SyncEventListner, SensorEventListener {
    private static final String TAG = "LocationUpdateService";
    private static final long INTERVAL = 1000 * 30; // 30 seconds
    private static LocationUpdateService instance;
    private PrefStore store;

    private SyncManager syncManager;
    private static Context mContext;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private String mLastUpdateTime;
    private boolean notifyVisible;
    private OnLocationReceived mLocationReceived;

    private LocationCallback locationCallback;
    private FusedLocationProviderClient fusedLocationClient;


    public static LocationUpdateService getInstance(Context mAct) {
        mContext = mAct;


        if (instance == null) {
            instance = new LocationUpdateService();
        }
        return instance;
    }

    public LocationUpdateService() {
    }

    public static void startService(Context context) {
        Intent callIntent = new Intent(context, LocationUpdateService.class);
        context.startService(callIntent);
    }

    public static void stopService(Context context) {
        Intent callIntent = new Intent(context, LocationUpdateService.class);
        context.stopService(callIntent);
    }

    protected static void log(String string) {
        if (BuildConfig.DEBUG)
            Log.e(TAG, string);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    public void setLocationReceivedListner(OnLocationReceived mLocationReceived) {
        this.mLocationReceived = mLocationReceived;
    }

    public interface OnLocationReceived {

        public void onLocationReceived(Location location);

        public void onConntected(Bundle bundle);

    }

    protected void createLocationRequest() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(INTERVAL);
        //mLocationRequest.setMaxWaitTime(60 * 60 * 1000);
        //mLocationRequest.setSmallestDisplacement(10.0f);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mContext == null)
            mContext = this;

        store = new PrefStore(this);
        syncManager = SyncManager.getInstance(this, BuildConfig.DEBUG);
        syncManager.setBaseUrl(Const.SERVER_REMOTE_URL, getString(R.string.app_name), store.getString("language", "en"));
        init();
        notifyVisible = false;
        return super.onStartCommand(intent, flags, startId);
    }

    private void init() {
        createLocationRequest();
        buildGoogleApiClient();
        mGoogleApiClient.connect();
    }

    @Override
    public void onDestroy() {
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        if(getFusedLocationClient() != null && locationCallback != null){
            getFusedLocationClient().removeLocationUpdates(locationCallback);
            fusedLocationClient = null;
            locationCallback = null;
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());
        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartServicePendingIntent);
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onConnected(Bundle bundle) {
        log("onConnected - isConnected ...............: " + mGoogleApiClient.isConnected());
        try {
            startLocationUpdates();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            generateNotification(1);
            stopService(mContext);
            return;
        }

        locationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Util.log(TAG,"onLocationResult "+locationResult);
                if(locationResult == null){
                    return;
                }
                onLocationChanged(locationResult.getLastLocation());
            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                super.onLocationAvailability(locationAvailability);
            }
        };
        getFusedLocationClient().requestLocationUpdates(mLocationRequest,locationCallback, Looper.getMainLooper());
        log("Location update started ..............: ");
    }

    private boolean isGPSEnabled() {
        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        Boolean enable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (enable) {
            notifyVisible = false;
        }
        return enable;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        log("Connection failed: " + connectionResult.toString());
    }

    public void onLocationChanged(Location location) {
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        log("Firing onLocationChanged................    latitude=  " + location.getLatitude() + "    longitude=   " + location.getLongitude() + "  " + mLastUpdateTime);
        handleNotifications();
        if (mLocationReceived != null)
            mLocationReceived.onLocationReceived(location);
    }

    public void handleNotifications() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            generateNotification(1);
        } else if (!isGPSEnabled()) {
            generateNotification(2);
        }
    }

    private void generateNotification(int i) {
        if (!notifyVisible) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String NOTIFICATION_CHANNEL_ID = "tsp_01";

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications",NotificationManager.IMPORTANCE_HIGH);
                // Configure the notification channel.
                notificationChannel.setDescription("Ubicacion");
                notificationManager.createNotificationChannel(notificationChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext,NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.mipmap.notification_1)
                    .setAutoCancel(true);
            Intent mainIntent;
            String message;
            if (i == 1) {
                mBuilder.setContentTitle(mContext.getString(R.string.app_name));
                message = mContext.getString(R.string.please_relaunch) + mContext.getString(R.string.app_name);
//                mainIntent = new Intent(mContext, SplashActivity.class);
//                mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            } else {
                mBuilder.setContentTitle(mContext.getString(R.string.app_name) + mContext.getString(R.string.gps));
                message = mContext.getString(R.string.check_gps);
                mainIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            }
            mBuilder.setContentText(message);
            mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
            Uri NotiSound = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder.setSound(NotiSound);
            long[] vibrate = {600, 100, 100, 700};
            mBuilder.setVibrate(vibrate);
//            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 12, mainIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager = (NotificationManager) mContext
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(i, mBuilder.build());
            notifyVisible = true;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onSyncStart() {

    }

    @Override
    public void onSyncFinish() {

    }

    @Override
    public void onSyncFailure(VolleyError error, Request mRequest) {

    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        log("Service onSyncSuccess--------" + action + "====" + status);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }


    public FusedLocationProviderClient getFusedLocationClient(){
        if(fusedLocationClient == null){
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext);
        }
        return fusedLocationClient;
    }

}
